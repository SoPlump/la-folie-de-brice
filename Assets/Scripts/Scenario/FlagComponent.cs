﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum ConditionType
{
    FLAG,
    TRIGGER_ENTER,
}

public class FlagComponent : MonoBehaviour
{
    public ConditionType condition;
    public UnityEvent onTrigger;

    private void Awake()
    {
        if (onTrigger == null)
            onTrigger = new UnityEvent();
    }

    private void Start()
    {
        if (condition == ConditionType.FLAG)
        {
            //StartCoroutine(TestTriggerAfter3Sec());
        }
    }

    public void Trigger()
    {
        onTrigger.Invoke();
    }

    IEnumerator TestTriggerAfter3Sec()
    {
        yield return new WaitForSeconds(3.0f);
        Trigger();
        yield return new WaitForSeconds(10.0f);
        Trigger();
        yield return new WaitForSeconds(10.0f);
        Trigger();
        yield return new WaitForSeconds(10.0f);
        Trigger();
        yield return new WaitForSeconds(10.0f);
        Trigger();
        yield return new WaitForSeconds(10.0f);
        Trigger();
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.CompareTag("Player")) && (condition == ConditionType.TRIGGER_ENTER))
        {
            Trigger();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if ((other.CompareTag("Player")) && (condition == ConditionType.TRIGGER_ENTER))
        {
            Trigger();
        }
    }
}
