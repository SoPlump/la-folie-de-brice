﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public abstract class Scenario : MonoBehaviour
{
    protected Scenario currentPart;
    public IEnumerator currentEntryPoint;
    public virtual IEnumerator EntryPoint()
    {
        Debug.LogFormat("Launched {0}", GetType());
        yield return null;
    }

    public IEnumerator CallSubScenario(Scenario subScenario)
    {
        yield return StartCoroutine(subScenario.EntryPoint());
    }

    public IEnumerator WaitForHelpQuote(string dialRef, float secondsToWait)
    {
        yield return new WaitForSeconds(secondsToWait);

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue(dialRef, QueuingMode.QUEUE_IF_QUEUE_EMPTY));

        StartCoroutine(WaitForHelpQuote(dialRef, secondsToWait + 5.0f));
    }
}
