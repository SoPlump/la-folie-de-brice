﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Act1 : Scenario
{
    public void Awake()
    {
    }

    public override IEnumerator EntryPoint()
    {
        StopAllCoroutines();
        switch (ScenarioManager.Instance.currentShortcut)
        {
            case ScenarioManager.Shortcuts.ACT1_PART0:
                currentEntryPoint = Part0_EntryPoint();
                break;
            case ScenarioManager.Shortcuts.ACT1_TAVERN:
                currentEntryPoint = Tavern_EntryPoint();
                break;
            case ScenarioManager.Shortcuts.ACT1_PART1:
                currentEntryPoint = Part1_EntryPoint();
                break;
        }

        yield return StartCoroutine(currentEntryPoint);
    }

    IEnumerator Part0_EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act1/Act1_Part0_IndustrialDistrict"));

        // Auto save
        GameMode.Instance.SaveGame();

        GameManager.Instance.enableInput = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        // Only audio for this
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act1Intro", QueuingMode.FLUSH));

        GameManager.Instance.enableInput = true;

        // Once the scenario has finished, change scenario shortcut
        ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.ACT1_TAVERN;

        yield return null;
    }
    IEnumerator Tavern_EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act1/Act1_Tavern"));

        GameManager.Instance.enableInput = false;

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act1TavernIntro", QueuingMode.FLUSH));

        GameManager.Instance.enableInput = true;

        yield return new WaitForSeconds(1.0f);

        // Create scenario adapted to the level
        if (currentPart)
        {
            Destroy(currentPart);
        }
        currentPart = gameObject.AddComponent<Scenario_Act1_Tavern>();
        yield return StartCoroutine(currentPart.EntryPoint());

        // Once the scenario has finished, change scenario shortcut
        ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.ACT1_PART1;
        Destroy(currentPart);

        yield return null;
    }
    IEnumerator Part1_EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act1/Act1_Part1"));

        // Create scenario adapted to the level
        if (currentPart)
        {
            Destroy(currentPart);
        }
        currentPart = gameObject.AddComponent<Scenario_Act1_Part1>();
        yield return StartCoroutine(currentPart.EntryPoint());

        // Once the scenario has finished, change scenario shortcut
        ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.ACT2_PART0;
        Destroy(currentPart);

        yield return null;
    }
}

/* For documentation purpose */


//Debug.Log("start waiting for trigger");
//yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.testTrigger.GetComponent<FlagComponent>().onTrigger));
//Debug.Log("stop waiting for trigger");

//Debug.Log("start intro Diana");
//GameManager.Instance.interactionAllowed = false;
//yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("intro1Diana", QueuingMode.FLUSH));
//yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("intro2Diana", QueuingMode.FLUSH));
//GameManager.Instance.interactionAllowed = true;

//Debug.Log("start waiting for diana dialogue");
//yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.dianaTrigger.GetComponent<DialogueInteraction>().onDialogueEnd));
//Debug.Log("stop waiting for diana dialogue");

//Debug.Log("start intro Jean");
//GameManager.Instance.interactionAllowed = false;
//yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("intro0Jean", QueuingMode.FLUSH));
//GameManager.Instance.interactionAllowed = true;

//Debug.Log("start waiting for flag");
//yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.testFlag.GetComponent<FlagComponent>().onTrigger));
//Debug.Log("stop waiting for flag");

//Debug.Log("start waiting for trigger");
//yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.testTrigger.GetComponent<FlagComponent>().onTrigger));
//Debug.Log("stop waiting for trigger");

//Debug.Log("start intro Diana");
//GameManager.Instance.interactionAllowed = false;
//yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("intro1Diana", QueuingMode.FLUSH));
//yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("intro2Diana", QueuingMode.FLUSH));
//GameManager.Instance.interactionAllowed = true;

//Debug.Log("start waiting for diana dialogue");
//yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.dianaTrigger.GetComponent<DialogueInteraction>().onDialogueEnd));
//Debug.Log("stop waiting for diana dialogue");

//Debug.Log("start intro Jean");
//GameManager.Instance.interactionAllowed = false;
//yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("intro0Jean", QueuingMode.FLUSH));
//GameManager.Instance.interactionAllowed = true;

//Debug.Log("start waiting for flag");
//yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.testFlag.GetComponent<FlagComponent>().onTrigger));
//Debug.Log("stop waiting for flag");