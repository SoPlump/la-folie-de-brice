﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Act1_Part1 : Scenario
{
    refs_Act1_Part1 refs;

    public void Awake()
    {
        refs = FindObjectOfType<refs_Act1_Part1>();
    }

    public override IEnumerator EntryPoint()
    {
        // Start level music
        AudioManager.Instance.PlayMusic(AudioManager.Music.FIGHT);

        // Inform refs that they're scenario entity for now
        refs.bossDoor.AddComponent<ScenarioInteractable>();
        refs.bossDoor.GetComponent<ScenarioInteractable>().canInteract = false;

        // Auto save
        GameMode.Instance.SaveGame();

        // Start fight
        FightManager.Instance.StartFight(1, 0, refs.eldritchsSpawn);

        // Start tuto
        GameManager.Instance.StartTutorial();

        // Wait until Brice win against first eldritchs
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(FightManager.Instance.endOfFight.onTrigger));

        // Allow access to what's next 
        refs.peopleBlockingTheWay.SetActive(false);

        // Stop the music
        StartCoroutine(AudioManager.Instance.FadeOutMusic(5.0f));

        // Wait to trigger tension music
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.industrialTrigger.GetComponent<FlagComponent>().onTrigger));

        refs.boss.SetActive(true);
        EnemyManager.Instance.StartBossFight();

        AudioManager.Instance.PlayMusic(AudioManager.Music.BOSS);

        // Wait to trigger boss apparition
        //yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.bossApparitionTrigger.GetComponent<FlagComponent>().onTrigger));

        // Wait until Brice win against first boss
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.boss.GetComponent<FlagComponent>().onTrigger));
        //yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.bossDeadTrigger.GetComponent<FlagComponent>().onTrigger));

        // Auto save
        GameMode.Instance.SaveGame();

        StartCoroutine(AudioManager.Instance.FadeOutMusic(10.0f));

        GameManager.Instance.enableInput = false;

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act1AfterBoss", QueuingMode.FLUSH));

        GameManager.Instance.enableInput = true;

        // Inform refs that they're not anymore scenario entity and can go back to their normal behaviour
        Destroy(refs.bossDoor.GetComponent<ScenarioInteractable>());

        // Wait Brice to open the door
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.bossDoor.GetComponentInChildren<TeleportDoor>().flagSwitchScene.onTrigger));
    }
}
