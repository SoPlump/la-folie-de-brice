﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenario_Act1_Tavern : Scenario
{
    refs_Act1_Tavern refs;

    public void Awake()
    {
        refs = FindObjectOfType<refs_Act1_Tavern>();
    }

    public override IEnumerator EntryPoint()
    {
        // Inform refs that they're scenario entity for now
        refs.nextDoor.AddComponent<ScenarioInteractable>();
        refs.nextDoor.GetComponent<ScenarioInteractable>().canInteract = false;

        refs.barman.AddComponent<ScenarioInteractable>();
        refs.barman.GetComponent<ScenarioInteractable>().canInteract = true;

        // Auto save
        GameMode.Instance.SaveGame();

        // Wait until Brice talk to the Barman
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.barman.GetComponent<DialogueInteraction>().onInteraction));

        GameManager.Instance.enableInput = false;

        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act1Tavern", QueuingMode.FLUSH));

        GameManager.Instance.enableInput = true;

        // Inform refs that they're not anymore scenario entity and can go back to their normal behaviour
        Destroy(refs.barman.GetComponent<ScenarioInteractable>());
        Destroy(refs.nextDoor.GetComponent<ScenarioInteractable>());

        // Wait until Brice leave the tavern
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.nextDoor.GetComponent<TeleportDoor>().flagSwitchScene.onTrigger));
    }
}
