﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Act2_Part1 : Scenario
{
    refs_Act2_Part1 refs;

    public void Awake()
    {
        refs = FindObjectOfType<refs_Act2_Part1>();
    }

    public override IEnumerator EntryPoint()
    {
        // Inform refs that they're scenario entity for now
        refs.bossDoor.AddComponent<ScenarioInteractable>();
        refs.bossDoor.GetComponent<ScenarioInteractable>().canInteract = false;

        // Auto save
        GameMode.Instance.SaveGame();

        // Wait to trigger tension music
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.tensionTrigger.GetComponent<FlagComponent>().onTrigger));

        // Start level music
        AudioManager.Instance.PlayMusic(AudioManager.Music.BOSS);

        EnemyManager.Instance.StartBossFight();

        // Wait to trigger boss apparition
        //yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.bossApparitionTrigger.GetComponent<FlagComponent>().onTrigger));

        // Wait until Brice win against first boss
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.boss.GetComponent<FlagComponent>().onTrigger));
        //yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.bossDeadTrigger.GetComponent<FlagComponent>().onTrigger));

        // Auto save
        GameMode.Instance.SaveGame();

        StartCoroutine(AudioManager.Instance.FadeOutMusic(10.0f));

        // Inform refs that they're not anymore scenario entity and can go back to their normal behaviour
        Destroy(refs.bossDoor.GetComponent<ScenarioInteractable>());

        // Wait Brice to open the door
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.bossDoor.GetComponentInChildren<TeleportDoor>().flagSwitchScene.onTrigger));
    }
}
