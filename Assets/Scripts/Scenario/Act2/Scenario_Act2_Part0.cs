﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Act2_Part0 : Scenario
{
    refs_Act2_Part0 refs;

    public void Awake()
    {
        refs = FindObjectOfType<refs_Act2_Part0>();
    }

    public override IEnumerator EntryPoint()
    {
        // Start level music
        AudioManager.Instance.PlayMusic(AudioManager.Music.FIGHT_ACT2);

        // Auto save
        GameMode.Instance.SaveGame();

        // Wait for Brice to win the fight
        FightManager.Instance.StartFight(1, 0, refs.eldritchsSpawn);
        FightManager.Instance.StartFight(1, 1, refs.eldritchsSpawn1);
        FightManager.Instance.StartFight(1, 0, refs.eldritchsSpawn2);
        FightManager.Instance.StartFight(1, 0, refs.eldritchsSpawn3);

        // Block tavern door
        refs.tavernDoor.AddComponent<ScenarioInteractable>();
        refs.tavernDoor.GetComponent<ScenarioInteractable>().canInteract = false;

        // Start help coroutine
        IEnumerator helpCor = WaitForHelpQuote("act2Help", 60.0f);
        StartCoroutine(helpCor);

        // Start fight
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(FightManager.Instance.endOfFight.onTrigger));
        StopCoroutine(helpCor);

        // Start level music
        StartCoroutine(AudioManager.Instance.FadeOutMusic(5.0f));

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act2Next", QueuingMode.FLUSH));

        helpCor = WaitForHelpQuote("act2Next", 60.0f);
        StartCoroutine(helpCor);

        // Auto save
        GameMode.Instance.SaveGame();

        // Start level music
        AudioManager.Instance.PlayMusic(AudioManager.Music.SPOOKY);

        // Do not block door
        Destroy(refs.tavernDoor.GetComponent<ScenarioInteractable>());

        // Wait for Brice to enter king castle
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.tavernDoor.GetComponentInChildren<TeleportDoor>().flagSwitchScene.onTrigger));
    }
}
