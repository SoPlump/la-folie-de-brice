﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Act2 : Scenario
{
    public void Awake()
    {
    }

    public override IEnumerator EntryPoint()
    {
        StopAllCoroutines();
        switch (ScenarioManager.Instance.currentShortcut)
        {
            case ScenarioManager.Shortcuts.ACT2_PART0:
                currentEntryPoint = Part0_EntryPoint();
                break;
            case ScenarioManager.Shortcuts.ACT2_TAVERN:
                currentEntryPoint = Tavern_EntryPoint();
                break;
            case ScenarioManager.Shortcuts.ACT2_PART1:
                currentEntryPoint = Part1_EntryPoint();
                break;
        }

        yield return StartCoroutine(currentEntryPoint);
    }

    IEnumerator Part0_EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act2/Act2_Part0"));

        GameManager.Instance.enableInput = false;

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act2Intro", QueuingMode.FLUSH));

        GameManager.Instance.enableInput = true;

        yield return new WaitForSeconds(1.0f);

        // Create scenario adapted to the level
        if (currentPart)
        {
            Destroy(currentPart);
        }
        currentPart = gameObject.AddComponent<Scenario_Act2_Part0>();
        yield return StartCoroutine(currentPart.EntryPoint());

        // Once the scenario has finished, change scenario shortcut
        ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.ACT2_TAVERN;

        yield return null;
    }
    IEnumerator Tavern_EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act2/Act2_Tavern"));

        // play music
        AudioManager.Instance.PlayMusic(AudioManager.Music.TAVERN);

        GameManager.Instance.enableInput = false;

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act2Tavern", QueuingMode.FLUSH));

        GameManager.Instance.enableInput = true;

        // Once the scenario has finished, change scenario shortcut
        ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.ACT2_PART1;
        Destroy(currentPart);

        // Auto save
        GameMode.Instance.SaveGame();

        yield return null;
    }
    IEnumerator Part1_EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act2/Act2_Part1"));

        // Create scenario adapted to the level
        if (currentPart)
        {
            Destroy(currentPart);
        }
        currentPart = gameObject.AddComponent<Scenario_Act2_Part1>();
        yield return StartCoroutine(currentPart.EntryPoint());

        // Once the scenario has finished, change scenario shortcut
        ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.ACT3_PART0; // TODO : à changer quand on fera le scénario acte 2
        Destroy(currentPart);

        yield return null;
    }
}
