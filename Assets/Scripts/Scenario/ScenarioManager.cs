﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ScenarioManager : MonoBehaviour
{
    public enum Shortcuts
    {
        DEBUG_SCENARIO,
        ACT1_PART0,
        ACT1_TAVERN,
        ACT1_PART1,
        ACT2_PART0,
        ACT2_TAVERN,
        ACT2_PART1,
        ACT3_PART0,
        ACT3_TEAROOM,
        ACT3_INSANEPART,
        ACT4,
    }

    enum Acts
    {
        DEBUG,
        ACT1,
        ACT2,
        ACT3,
        ACT4,
    }

    //SINGLETON
    public static ScenarioManager Instance;

    // SCENARIOs

    private Scenario currentScenario;
    public Shortcuts currentShortcut = Shortcuts.ACT1_PART0;
    Acts currentAct = Acts.ACT1;

    // Used when debugging
    public bool scenarioAllowed = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            //StartCoroutine(Instance.LaunchScenario());
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LaunchScenario());
    }

    public void RestartScenario()
    {
        StopAllCoroutines();

        StartCoroutine(Instance.LaunchScenario());
    }

    public IEnumerator LaunchScenario()
    {
        bool startScenario = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        if (GameMode.Instance)
        {
            if (GameMode.Instance.requestedGameLoad)
            {
                while (!GameMode.Instance.gameHasLoaded)
                {
                    yield return null;
                }

                startScenario = true;
            }
            else
            {
                startScenario = true;
            }
        }
        else if (scenarioAllowed)
        {
            startScenario = true;
        }

        if (startScenario)
        {
            StartCoroutine(UpdateScenarioOnLevelSwitch());
        }
    }

    public IEnumerator UpdateScenarioOnLevelSwitch()
    {
        DialogueManager.Instance.LoadScenarioDialoguesFromLevel(currentShortcut);
        Debug.Log(currentShortcut);
        ChooseScenarioFromShortcut();

        IEnumerator currentAct = currentScenario.EntryPoint();
        StartCoroutine(currentAct);

        yield return StartCoroutine(WaitForSceneSwitchFromCurrentScene(SceneManager.GetActiveScene().buildIndex));

        //StartCoroutine(AudioManager.Instance.FadeOutMusic());
        StopCoroutine(currentScenario.currentEntryPoint);
        StopCoroutine(currentAct);
        GameManager.Instance.SpawnPlayer(SceneManager.GetActiveScene().buildIndex);

        yield return new WaitForSeconds(0.5f);

        StartCoroutine(UpdateScenarioOnLevelSwitch());
    }

    public void ChooseScenarioFromShortcut()
    {
        switch (currentShortcut)
        {
            case Shortcuts.DEBUG_SCENARIO:
                currentAct = Acts.DEBUG;
                currentScenario = gameObject.AddComponent<Scenario_Test>();
                break;
            case Shortcuts.ACT1_PART0:
            case Shortcuts.ACT1_TAVERN:
            case Shortcuts.ACT1_PART1:
                currentAct = Acts.ACT1;
                break;
            case Shortcuts.ACT2_PART0:
            case Shortcuts.ACT2_TAVERN:
            case Shortcuts.ACT2_PART1:
                currentAct = Acts.ACT2;
                break;
            case Shortcuts.ACT3_PART0:
            case Shortcuts.ACT3_TEAROOM:
            case Shortcuts.ACT3_INSANEPART:
                currentAct = Acts.ACT3;
                break;
            case Shortcuts.ACT4:
                currentAct = Acts.ACT4;
                break;
        }

        if (currentScenario)
        {
            if ((currentAct == Acts.ACT1)&&(currentScenario.GetType() != typeof(Scenario_Act1)))
            {
                currentScenario = gameObject.AddComponent<Scenario_Act1>();
            }
            if ((currentAct == Acts.ACT2) && (currentScenario.GetType() != typeof(Scenario_Act2)))
            {
                currentScenario = gameObject.AddComponent<Scenario_Act2>();
            }
            if ((currentAct == Acts.ACT3) && (currentScenario.GetType() != typeof(Scenario_Act3)))
            {
                currentScenario = gameObject.AddComponent<Scenario_Act3>();
            }
            if ((currentAct == Acts.ACT4) && (currentScenario.GetType() != typeof(Scenario_Act4)))
            {
                currentScenario = gameObject.AddComponent<Scenario_Act4>();
            }
        }
        else
        {
            switch (currentAct)
            {
                case Acts.ACT1:
                    currentScenario = gameObject.AddComponent<Scenario_Act1>();
                    break;
                case Acts.ACT2:
                    currentScenario = gameObject.AddComponent<Scenario_Act2>();
                    break;
                case Acts.ACT3:
                    currentScenario = gameObject.AddComponent<Scenario_Act3>();
                    break;
                case Acts.ACT4:
                    currentScenario = gameObject.AddComponent<Scenario_Act4>();
                    break;

            }

        }
    }

    public IEnumerator WaitUntilEvent(UnityEvent unityEvent)
    {
        var trigger = false;
        Action action = () => trigger = true;

        unityEvent.AddListener(action.Invoke);

        yield return new WaitUntil(() => trigger);

        unityEvent.RemoveListener(action.Invoke);
    }

    public IEnumerator WaitForSceneLoad(string scenePath)
    {
        int idScene = SceneUtility.GetBuildIndexByScenePath(scenePath);
        while (SceneManager.GetActiveScene().buildIndex != idScene)
        {
            yield return null;
        }
    }

    public IEnumerator WaitForSceneSwitchFromCurrentScene(int idCurrentScene)
    {
        while (SceneManager.GetActiveScene().buildIndex == idCurrentScene)
        {
            yield return null;
        }
    }

    public IEnumerator WaitForQTE()
    {
        GameManager.Instance.enableInput = true;
        GameManager.Instance.StartQTE(true);

        var qteFlag = GameManager.Instance.QTEManager.AddComponent<FlagComponent>();

        yield return WaitUntilEvent(qteFlag.onTrigger);
    }
}
