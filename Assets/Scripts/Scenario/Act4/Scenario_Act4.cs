﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenario_Act4 : Scenario
{
    refs_Act4 refs;

    public void Awake()
    {
        refs = FindObjectOfType<refs_Act4>();
    }

    public override IEnumerator EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act4/Act4_final"));

        // Auto Save
        GameMode.Instance.SaveGame();

        // Play music
        AudioManager.Instance.PlayMusic(AudioManager.Music.JEALOUS_VOICE);

        GameManager.Instance.enableInput = false;

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act4Intro", QueuingMode.FLUSH));

        GameManager.Instance.enableInput = true;

        // Wait to trigger boss monologue 
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.bossMonologueTrigger.GetComponent<FlagComponent>().onTrigger));

        GameManager.Instance.enableInput = false;

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act4Betrayal", QueuingMode.FLUSH));

        // Spawn eldritchs : TODO
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act4SpawnEldritchs", QueuingMode.FLUSH));
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act4Stuck", QueuingMode.FLUSH));
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act4Disclosure", QueuingMode.FLUSH));

        // StartCoroutine(AudioManager.Instance.FadeOutMusic(1.0f));

        // Wait for Brice to do a AoE spell to get rid of Eldritchs // not done yet
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act4Action", QueuingMode.FLUSH));

        refs.blockingWall.SetActive(true);

        GameManager.Instance.enableInput = true;

        AudioManager.Instance.PlayMusic(AudioManager.Music.BOSS);

        EnemyManager.Instance.StartBossFight();

        // Wait until Brice win against the king
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.boss.GetComponent<FlagComponent>().onTrigger));

        StartCoroutine(AudioManager.Instance.FadeOutMusic(10.0f));

        AudioManager.Instance.PlayMusic(AudioManager.Music.END, 10.0f);

        SceneManager.LoadScene("Levels/L_Outro");
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
