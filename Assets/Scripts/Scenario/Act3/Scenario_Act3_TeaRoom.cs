﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenario_Act3_TeaRoom : Scenario
{
    refs_Act3_TeaRoom refs;

    public void Awake()
    {
        refs = FindObjectOfType<refs_Act3_TeaRoom>();
    }

    public override IEnumerator EntryPoint()
    {
        // Auto save
        GameMode.Instance.SaveGame();

        // Launch cinematique where Brice is not welcomed
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3Hatred", QueuingMode.FLUSH));
        StartCoroutine(DialogueManager.Instance.PlayDialogue("act3Insults", QueuingMode.FLUSH));
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3BeforeQTE", QueuingMode.QUEUING)); //TODO : tester

        // Start QTE
        StartCoroutine(DialogueManager.Instance.PlayDialogue("creepyVoices", QueuingMode.FLUSH));
        yield return StartCoroutine(ScenarioManager.Instance.WaitForQTE());

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3AfterQTE", QueuingMode.FLUSH));

        SceneManager.LoadScene("Levels/Act3/Act3_InsanePart");
    }
}
