﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Act3_Part0 : Scenario
{
    refs_Act3_Part0 refs;

    public void Awake()
    {
        refs = FindObjectOfType<refs_Act3_Part0>();
    }

    public override IEnumerator EntryPoint()
    {

        // Notify Guards are on this scene
        EnemyManager.Instance.FindGuards();

        // Start level music
        AudioManager.Instance.PlayMusic(AudioManager.Music.FIGHT);

        // Auto save
        GameMode.Instance.SaveGame();

        // Wait until Brice win against first eldritchs
        FightManager.Instance.StartFight(1, 0, refs.eldritchsSpawn);
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(FightManager.Instance.endOfFight.onTrigger));

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3GuardBefore", QueuingMode.FLUSH));

        // Start level music
        AudioManager.Instance.PlayMusic(AudioManager.Music.SPOOKY);

        // Inform guard that they're scenario entity for now
        if (!refs.guard.TryGetComponent<ScenarioInteractable>(out var doorSi))
        {
            refs.guard.AddComponent<ScenarioInteractable>();
            refs.guard.GetComponent<ScenarioInteractable>().canInteract = true;
        }

        // Wait until interaction with guards
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.guard.GetComponent<DialogueInteraction>().onInteraction));

        // Auto save
        GameMode.Instance.SaveGame();

        // Guards let you pass because you're sane enough
        // or guards fight you because you're too insane
        if (Player.Instance.PlayerMentalHealth.IsSane())
        {
            yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3GuardPeace", QueuingMode.FLUSH));
        }
        else
        {
            // Start level music
            AudioManager.Instance.PlayMusic(AudioManager.Music.FIGHT);

            // Audio
            yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3GuardFight", QueuingMode.FLUSH));
            yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3GuardFight2", QueuingMode.QUEUING)); // TODO : pas sûre que ça fonctionne ça

            // Start fight
            FightManager.Instance.StartGuardFight();

            // Wait until Brice win against guards
            yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(FightManager.Instance.endOfFight.onTrigger));

            // Audio
            yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3GuardFightAfter", QueuingMode.FLUSH));

            // Start level music
            AudioManager.Instance.PlayMusic(AudioManager.Music.SPOOKY);

            // Auto save
            GameMode.Instance.SaveGame();
        }

        // Inform refs that they're not anymore scenario entity and can go back to their normal behaviour
        //Destroy(refs.guard.GetComponent<ScenarioInteractable>());

        // Let access the rest of the level
        refs.guardWall.GetComponent<MeshRenderer>().enabled = false;
        refs.guardWall.GetComponent<BoxCollider>().enabled = false;

        //Destroy(refs.guard.GetComponent<ScenarioInteractable>());

        // Wait for Brice to open the door
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.teaRoomDoor.GetComponentInChildren<TeleportDoor>().flagSwitchScene.onTrigger));
    }
}
