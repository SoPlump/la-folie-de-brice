﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Act3 : Scenario
{
    public override IEnumerator EntryPoint()
    {
        StopAllCoroutines();

        switch (ScenarioManager.Instance.currentShortcut)
        {
            case ScenarioManager.Shortcuts.ACT3_PART0:
                currentEntryPoint = Part0_EntryPoint();
                break;
            case ScenarioManager.Shortcuts.ACT3_TEAROOM:
                currentEntryPoint = TeaRoom_EntryPoint();
                break;
            case ScenarioManager.Shortcuts.ACT3_INSANEPART:
                currentEntryPoint = InsanePart_EntryPoint();
                break;
        }

        yield return StartCoroutine(currentEntryPoint);
    }

    IEnumerator Part0_EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act3/Act3_Part0"));

        // Auto save
        GameMode.Instance.SaveGame();

        // Create scenario adapted to the level

        if (currentPart)
        {
            Destroy(currentPart);
        }

        currentPart = gameObject.AddComponent<Scenario_Act3_Part0>();
        yield return StartCoroutine(currentPart.EntryPoint());

        // Once the scenario has finished, change scenario shortcut
        ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.ACT3_TEAROOM;
        Destroy(currentPart);

        // Auto save
        GameMode.Instance.SaveGame();

        yield return null;
    }
    IEnumerator TeaRoom_EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act3/Act3_TeaRoom"));

        // Start level music
        AudioManager.Instance.PlayMusic(AudioManager.Music.TEA_ROOM);

        // Audio
        GameManager.Instance.enableInput = false;
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3TeaRoomIntro", QueuingMode.FLUSH));

        // Create scenario adapted to the level

        if (currentPart)
        {
            Destroy(currentPart);
        }

        currentPart = gameObject.AddComponent<Scenario_Act3_TeaRoom>();
        yield return StartCoroutine(currentPart.EntryPoint());

        // Once the scenario has finished, change scenario shortcut
        ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.ACT3_INSANEPART;
        Destroy(currentPart);

        yield return null;
    }
    IEnumerator InsanePart_EntryPoint()
    {
        // Wait to enter concerned level
        yield return StartCoroutine(ScenarioManager.Instance.WaitForSceneLoad("Levels/Act3/Act3_InsanePart"));

        // Create scenario adapted to the level

        if (currentPart)
        {
            Destroy(currentPart);
        }

        currentPart = gameObject.AddComponent<Scenario_Act3_InsanePart>();
        yield return StartCoroutine(currentPart.EntryPoint());

        // Once the scenario has finished, change scenario shortcut
        ScenarioManager.Instance.currentShortcut = ScenarioManager.Shortcuts.ACT4;
        Destroy(currentPart);

        yield return null;
    }
}
