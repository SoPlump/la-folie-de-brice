﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Act3_InsanePart : Scenario
{
    refs_Act3_InsanePart refs;

    public void Awake()
    {
        refs = FindObjectOfType<refs_Act3_InsanePart>();
    }

    public override IEnumerator EntryPoint()
    {
        // Play cinematique
        // Auto save
        GameMode.Instance.SaveGame();

        // Getting Brice Insane
        Player.Instance.PlayerMentalHealth.GetInsane();

        // Start level music
        AudioManager.Instance.PlayMusic(AudioManager.Music.FIGHT);

        // Wait for Brice to win the fight
        FightManager.Instance.StartFight(1, 0, refs.eldritchsSpawn);
        FightManager.Instance.StartFight(1, 1, refs.eldritchsSpawn1);
        FightManager.Instance.StartFight(1, 0, refs.eldritchsSpawn2);

        refs.teaRoomDoor.AddComponent<ScenarioInteractable>();
        refs.teaRoomDoor.GetComponent<ScenarioInteractable>().canInteract = false;

        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(FightManager.Instance.endOfFight.onTrigger));

        // Start level music
        AudioManager.Instance.PlayMusic(AudioManager.Music.JEALOUS_VOICE);

        GameManager.Instance.enableInput = false;

        // Audio
        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3AfterFight", QueuingMode.FLUSH));

        yield return StartCoroutine(DialogueManager.Instance.PlayDialogue("act3Help", QueuingMode.FLUSH));

        GameManager.Instance.enableInput = true;

        Destroy(refs.teaRoomDoor.GetComponent<ScenarioInteractable>());

        // Auto save
        GameMode.Instance.SaveGame();

        // Let access the rest of the level
        refs.kingWall.GetComponent<MeshRenderer>().enabled = false;
        refs.kingWall.GetComponent<BoxCollider>().enabled = false;

        // Wait for Brice to enter king castle
        yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(refs.kingDoor.GetComponentInChildren<TeleportDoor>().flagSwitchScene.onTrigger));

        yield return null;
    }
}
