﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario_Test : Scenario
{
    refs_Test refs;

    public void Awake()
    {
        refs = FindObjectOfType<refs_Test>();
    }

    public override IEnumerator EntryPoint()
    {
        //yield return StartCoroutine(ScenarioManager.Instance.WaitForQTE());

        Debug.LogError(GameManager.Instance.QTEisSuccess);
        yield return null;
    }

}
