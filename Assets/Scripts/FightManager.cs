﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightManager : MonoBehaviour
{
    //SINGLETON
    public static FightManager Instance;

    [System.NonSerialized]
    public int killsNeeded;

    [System.NonSerialized]
    public FlagComponent endOfFight;

    private bool fightingGuard = false;

    // Start is called before the first frame update
    void Start()
    {
        endOfFight = gameObject.AddComponent<FlagComponent>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(this.gameObject);

            //Rest of your Awake code

        }
        else
        {
            Destroy(this);
        }
    }

    public void StartFight(int numberToKill, int numberOfRanged, GameObject spawnZone)
    {
        Vector3 centre = spawnZone.transform.position;
        float distX = spawnZone.GetComponent<Renderer>().bounds.size.x;
        float distZ = spawnZone.GetComponent<Renderer>().bounds.size.z;
        for (int i = 0; i < numberOfRanged; ++i)
        {
            EnemyManager.Instance.SpawnRangedEldritch(spawnZone);
        }
        for(int i = numberOfRanged; i < numberToKill; ++i)
        {
            EnemyManager.Instance.SpawnMeleeEldritch(spawnZone);
        }
    }

    public void StartGuardFight()
    {
        killsNeeded += EnemyManager.Instance.CountGuards();
        EnemyManager.Instance.StartGuardFight();
        fightingGuard = true;
    }

    public void GuardKilled()
    {
        if (--killsNeeded == 0)
        {
            Player.Instance.PlayerMentalHealth.existInsanity();
            killsNeeded = 0;
            EnemyManager.Instance.DestroyGuards();
            endOfFight.Trigger();
        }
    }


    public void EldritchKilled()
    {
        if (--killsNeeded == 0)
        {
            killsNeeded = 0;
            Player.Instance.PlayerMentalHealth.existInsanity();
            if(fightingGuard)
            {
                EnemyManager.Instance.DestroyGuards();
                fightingGuard = false;
            }
            endOfFight.Trigger();
        }
    }

    public void EldritchSpawned()
    {
        killsNeeded++;
    }

    public void ForceEndOfFight()
    {
        endOfFight.Trigger();
        killsNeeded = 0;

    }


}
