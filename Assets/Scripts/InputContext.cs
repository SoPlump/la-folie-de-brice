﻿using UnityEngine;
using UnityEngine.InputSystem;

public class InputContext : MonoBehaviour
{

    private PlayerActions playerActions;

    private Vector2 _look;
    private Vector2 _move;
    private bool isDrinkingButtonHold = false;

    // Start is called before the first frame update
    void Start()
    {
        playerActions = GetComponentInParent<PlayerActions>();

        GameManager.Instance.InitializePlayerInfo(GetComponentInParent<PlayerInput>());
        InputSystem.onDeviceChange += test;
    }
    void test(InputDevice inputDevice, InputDeviceChange inputDeviceChange)
    {
        Debug.Log(inputDevice.name + " " + inputDeviceChange.ToString());
    }
    // Update is called once per frame
    void Update()
    {

        if (GameManager.Instance.enableInput)
        {
            playerActions.Move(_move);

            if (isDrinkingButtonHold)
            {
                Player.Instance.PlayerMentalHealth.UsePotion(Time.deltaTime);
            }
        }
        else
        {
            playerActions.Move(new Vector2());
        }

        playerActions.Look(_look);
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        _move = context.ReadValue<Vector2>();
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        _look = context.ReadValue<Vector2>();
    }

    public void OnPause(InputAction.CallbackContext context)
    {
        GameManager.Instance.Pause();
    }

    public void OnQTE(InputAction.CallbackContext context)
    {
        if (context.action.controls[0].name == "x")
            GameManager.Instance.StartQTE(true);
        else
            GameManager.Instance.StartQTE(false);
    }

    public void OnQTE_Action_LeftQ(InputAction.CallbackContext context)
    {
        GameManager.Instance.OnQTEAction(QTE.Keys.Q);
    }
    public void OnQTE_Action_RightD(InputAction.CallbackContext context)
    {
        GameManager.Instance.OnQTEAction(QTE.Keys.D);
    }
    public void OnQTE_Action_TopZ(InputAction.CallbackContext context)
    {
        GameManager.Instance.OnQTEAction(QTE.Keys.Z);
    }
    public void OnQTE_Action_BottomS(InputAction.CallbackContext context)
    {
        GameManager.Instance.OnQTEAction(QTE.Keys.S);
    }
    public void OnQTE_Action_TriangleR(InputAction.CallbackContext context)
    {
        GameManager.Instance.OnQTEAction(QTE.Keys.R);
    }
    public void OnQTE_Action_CrossSpace(InputAction.CallbackContext context)
    {
        GameManager.Instance.OnQTEAction(QTE.Keys.SPACE);
    }
    public void OnQTE_Action_SquareA(InputAction.CallbackContext context)
    {
        GameManager.Instance.OnQTEAction(QTE.Keys.A);
    }
    public void OnQTE_Action_CircleE(InputAction.CallbackContext context)
    {
        GameManager.Instance.OnQTEAction(QTE.Keys.E);
    }

    public void OnPotionDrink(InputAction.CallbackContext context)
    {
        if (context.performed)
            isDrinkingButtonHold = true;
        if (context.canceled)
            isDrinkingButtonHold = false;
    }

    public void OnAttack(InputAction.CallbackContext context)
    {
        if (isDrinkingButtonHold) return;
        GameManager.Instance.QuickAttack();
    }

    public void OnInteraction(InputAction.CallbackContext context)
    {
        if (isDrinkingButtonHold) return;

        playerActions.Interaction();
    }

    public void OnParry(InputAction.CallbackContext context)
    {
        if (isDrinkingButtonHold) return;
        if (context.started)
        {
            GameManager.Instance.ActivateParry();
        }
        else if (context.canceled)
        {
            GameManager.Instance.StopParry();
        }
    }

    public void OnRoll(InputAction.CallbackContext context)
    {
        if (isDrinkingButtonHold) return;

        GameManager.Instance.TriggerRoll();
    }

    public void OnHeavyAttack(InputAction.CallbackContext context)
    {
        if (isDrinkingButtonHold) return;

        GameManager.Instance.HeavyAttack();
    }

    public void OnSpell01(InputAction.CallbackContext context)
    {
        if (isDrinkingButtonHold) return;

        GameManager.Instance.Spell01();
    }

    public void OnSpawnMelee(InputAction.CallbackContext context)
    {
        
    }

    public void OnSpawnRange(InputAction.CallbackContext context)
    {

    }

    public void OnSpellAoE(InputAction.CallbackContext context)
    {
        if (isDrinkingButtonHold) return;

        GameManager.Instance.TriggerAoeSpell();
    }

    public void OnExecute(InputAction.CallbackContext context)
    {
        if (isDrinkingButtonHold) return;

        GameManager.Instance.TriggerExecute();
    }
}
