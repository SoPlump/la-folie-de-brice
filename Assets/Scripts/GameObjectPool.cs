﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameObjectPool : MonoBehaviour
{



    [SerializeField]
    private GameObject[] pool;

    private List<GameObject> instantiatedObjects = new List<GameObject>();

    public GameObject GetObject(string objectType)
    {
        foreach (GameObject obj in instantiatedObjects)
        {
            if (obj.name == objectType && !obj.activeInHierarchy)
            {
                obj.SetActive(true);
                return obj;
            }
        }

        // Instantiate a new object
        for (int i = 0; i < pool.Length; i++)
        {
            if (pool[i].name == objectType)
            {
                GameObject newInstance = GameObject.Instantiate(pool[i]);
                instantiatedObjects.Add(newInstance);
                newInstance.name = objectType;
                return newInstance;
            }
        }

        return null;
    }

    private void Start()
    {
        StartCoroutine(WaitForLevelLoading());
    }

    public void ReleaseObject(GameObject objectToRelease)
    {
        objectToRelease.SetActive(false);
    }

    public void ClearPool()
    {
        instantiatedObjects = new List<GameObject>();
    }

    private IEnumerator WaitForLevelLoading()
    {
        int actualScene = SceneManager.GetActiveScene().buildIndex;

        yield return StartCoroutine(GameMode.Instance.WaitForSceneSwitchFromActualScene(actualScene));
        ClearPool();
        StartCoroutine(WaitForLevelLoading());
    }
}
