﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public void TakeDamage(float damage)
    {
        //Debug.Log("Brice took " + damage + " damage.");
        if(!Player.Instance.isInvincible)
        {
            Player.Instance.OnHitReceived(damage);
        }
    }
}
