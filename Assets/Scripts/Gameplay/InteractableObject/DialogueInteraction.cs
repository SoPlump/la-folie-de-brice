﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

public class DialogueInteraction : InteractableComponent
{
    [HideInInspector] Animator anim;
    FlagComponent flag;
    public DialogueManager.QuestData[] data;
    PeopleBehaviourManager behaviour;

    private int nbQuestSpoken = 0;
    private int nbSentencesToSay = 0;
    private int nbSentencesFinished = 0;
    private bool HasLaunchedInteraction = false;
    bool hasInteracted = false;

    public UnityEvent onDialogueEnd;

    private void Awake()
    {
        if (onDialogueEnd == null)
            onDialogueEnd = new UnityEvent();

        anim = gameObject.GetComponent<Animator>();
        behaviour = gameObject.GetComponent<PeopleBehaviourManager>();
    }

    private void Start()
    {
        flag = gameObject.AddComponent<FlagComponent>();
        flag.condition = ConditionType.FLAG;
        flag.onTrigger.AddListener(DialogueHasFinished);

        StartCoroutine(LateStart());
    }

    private IEnumerator LateStart()
    {
        yield return new WaitForSeconds(1.0f);

        DialogueManager.Instance.CreateQuestDialogues(data, name);
    }

    public override void Interact()
    {
        if (gameObject.TryGetComponent<ScenarioInteractable>(out var scenarioInteractable))
        {
            if (scenarioInteractable.canInteract)
            {
                behaviour.StartTalking();
                if (gameObject.TryGetComponent<PlayableDirector>(out var playable))
                {
                    playable.Play();
                }
                //anim.SetBool("isTalking", true);
                onInteraction.Invoke();
            }
            else
            {
                ClassicInteract();
            }
        }
        else
        {
            ClassicInteract();
        }
    }

    private void ClassicInteract()
    {
        if ((GameManager.Instance.interactionAllowed) && (!hasInteracted))
        {
            behaviour.StartTalking();
            HasLaunchedInteraction = true;
            hasInteracted = true;

            if (nbQuestSpoken == data.Length)
                nbQuestSpoken = 0;

            if (data[nbQuestSpoken].questName.Contains("Healing"))
            {
                if (TryGetComponent<HealingBehaviour>(out var healingComponent))
                {
                    healingComponent.HealBrice();
                }
                else
                {
                    Debug.LogError("No healing behaviour component to heal");
                }
            }
            else
            {
                Player.Instance.PlayerMentalHealth.LoseSanity(5.0f);
            }

            nbSentencesToSay = data[nbQuestSpoken].nbSentences;

            StartCoroutine(DialogueManager.Instance.PlayDialogue(data[nbQuestSpoken].questName + "0" + name, QueuingMode.FLUSH));

            for (int i = 1; i < data[nbQuestSpoken].nbSentences; ++i)
            {
                StartCoroutine(DialogueManager.Instance.PlayDialogue(data[nbQuestSpoken].questName + i + name, QueuingMode.QUEUING));
            }

            ++nbQuestSpoken;
        }
    }

    public void FreeInsult()
    {
        if ((GameManager.Instance.interactionAllowed) && (!hasInteracted))
        {
            Player.Instance.PlayerMentalHealth.LoseSanity(5.0f);
            behaviour.StartTalking(false);
            HasLaunchedInteraction = true;
            hasInteracted = true;

            if (nbQuestSpoken == data.Length)
                nbQuestSpoken = 0;

            nbSentencesToSay = data[nbQuestSpoken].nbSentences;

            StartCoroutine(DialogueManager.Instance.PlayDialogue(data[nbQuestSpoken].questName + "0" + name, QueuingMode.FLUSH));

            for (int i = 1; i < data[nbQuestSpoken].nbSentences; ++i)
            {
                StartCoroutine(DialogueManager.Instance.PlayDialogue(data[nbQuestSpoken].questName + i + name, QueuingMode.QUEUING));
            }

            ++nbQuestSpoken;
        }
    }

    public void DialogueHasFinished()
    {
        if (HasLaunchedInteraction)
        {
            ++nbSentencesFinished;
            if (nbSentencesFinished == nbSentencesToSay)
            {
                behaviour.StopTalking();
                hasInteracted = HasLaunchedInteraction = false;
                nbSentencesFinished = 0;

                onDialogueEnd.Invoke();
            }
            else
            {
                //behaviour.StopTalking();
            }
        }
        else
        {
            behaviour.StopTalking();
        }
    }

}
