﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioInteractable : MonoBehaviour
{
    public bool canInteract = false;
    void Awake()
    {
        enabled = false;
    }
}
