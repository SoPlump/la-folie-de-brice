﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class InteractableComponent : MonoBehaviour
{
    public UnityEvent onInteraction;
    private void Awake()
    {
        if (onInteraction == null)
            onInteraction = new UnityEvent();
    }

    // Start is called before the first frame update
    void Start()
    {
        enabled = false;
    }

    public abstract void Interact();
}
