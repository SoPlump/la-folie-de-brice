﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenableDoor : InteractableComponent
{
    bool isDoorOpen = false;
    bool isOpening = false;

    Animator anim;

    // Start is called before the first frame update
    virtual public void Start()
    {
        anim = GetComponent<Animator>();
    }

    public override void Interact()
    {
        if (!isOpening)
        {
            isOpening = true;
            if (!isDoorOpen)
            {
                anim.Play("OpenDoor");
            }
            else
            {
                anim.Play("CloseDoor");
            }
        }
    }

    public void Open()
    {
        if ((isOpening)&&(!isDoorOpen))
        {
            isOpening = false;
            isDoorOpen = true;
        }
    }

    public void Close()
    {
        if ((isOpening) && (isDoorOpen))
        {
            isOpening = false;
            isDoorOpen = false;
        }
    }
}
