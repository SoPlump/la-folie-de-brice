﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FountainInteraction : InteractableComponent
{
    public override void Interact()
    {
        AudioManager.Instance.PlayHealingSound();

        Player.Instance.PlayerMentalHealth.FullyHealSanity();

        //Debug.Log(Player.Instance.PlayerMentalHealth.Sanity);
    }
}
