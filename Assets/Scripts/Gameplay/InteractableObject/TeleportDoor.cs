﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportDoor : OpenableDoor
{
    public string NextScenePath = "Levels/";

    [System.NonSerialized]
    public FlagComponent flagSwitchScene;
    private AudioClip lockedDoor;
    private AudioClip unlockedDoor;
    private AudioSource audioSource;

    override public void Start()
    {
        base.Start();
        flagSwitchScene = gameObject.AddComponent<FlagComponent>();
        flagSwitchScene.condition = ConditionType.FLAG;

        lockedDoor = (AudioClip)Resources.Load("Sounds/lockDoor");
        unlockedDoor = (AudioClip)Resources.Load("Sounds/unlockDoor");
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.dopplerLevel = 0.0f;
    }

    public override void Interact()
    {
        if (gameObject.TryGetComponent<ScenarioInteractable>(out var scenarioInteractable))
        {
            if (scenarioInteractable.canInteract)
            {
                ClassicInteract();
            }
            else
            {
                audioSource.clip = lockedDoor;
                audioSource.Play();
            }
        }
        else
        {
            ClassicInteract();
        }
    }

    public void ClassicInteract()
    {
        audioSource.clip = unlockedDoor;
        audioSource.Play();

        base.Interact();

        GameManager.Instance.enableInput = false;
        int currentScene = SceneManager.GetActiveScene().buildIndex;

        if (GameManager.Instance.lastPositionInMap.ContainsKey(currentScene))
        {
            GameManager.Instance.lastPositionInMap.Remove(currentScene);
        }
        GameManager.Instance.lastPositionInMap.Add(currentScene, Player.Instance.gameObject.transform.position);

        StartCoroutine(SwitchScene());
    }

    IEnumerator SwitchScene()
    {
        yield return new WaitForSeconds(1.0f);

        flagSwitchScene.Trigger();
        GameManager.Instance.enableInput = true;
        SceneManager.LoadScene(NextScenePath);
    }
}
