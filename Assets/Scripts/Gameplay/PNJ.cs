﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PNJ : MonoBehaviour
{

    //Maximum amount of time between two insults
    [SerializeField]
    float maxTime = 5.0f;

    //Minimum amount of time between two insults
    [SerializeField]
    float minTime = 2.0f;

    // time for the next insult in the [minTime, maxTime] interval
    float nextInsultTime;

    // current insult cooldown
    float currentTime = 0.0f;

    // is the NPC ready to insult or not 
    bool insultInCooldown = false;

    bool isPlayerAround = false;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (insultInCooldown)
        {
            UpdateInsultCooldown();
        } else
        {
            if (isPlayerAround)
            //if (isPlayerAround && ( Player.Instance.PlayerMentalHealth.IsBorderline() 
            //    || Player.Instance.PlayerMentalHealth.IsInsane())|| Player.Instance.PlayerMentalHealth.IsMedium())
            {
                InsultPlayer();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("I detect the player");
            PlayerDetected();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("I lost the player");
            PlayerLost();
        }
    }

    void PlayerDetected()
    {
        isPlayerAround = true;
        nextInsultTime = Random.Range(minTime, maxTime);
        insultInCooldown = true; // On fait ça pour que le PNJ insulte pas instantanément le joueur
    }

    void PlayerLost()
    {
        isPlayerAround = false;
    }

    void InsultPlayer()
    {

        Player.Instance.PlayerMentalHealth.LoseSanity(5.0f);
        Debug.Log("Insulting at " + Time.time);
        insultInCooldown = true;
        nextInsultTime = Random.Range(minTime, maxTime);
        currentTime = 0.0f;
        gameObject.GetComponent<DialogueInteraction>().FreeInsult();
    }

    void UpdateInsultCooldown()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= nextInsultTime)
        {
            insultInCooldown = false;
        }
    }
}
