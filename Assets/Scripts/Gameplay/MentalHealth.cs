﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[System.Serializable]
public class MentalHealth 
{

    //DEFINITIONS 


    SaneState sane = new SaneState();
    MediumState medium = new MediumState();
    BorderlineState borderline = new BorderlineState();
    InsaneState insane = new InsaneState();

    MentalHealthState currentState = new MentalHealthState();


    //CONSTANTS  


    //If sanity is above this value , the character is considered Sane
    [SerializeField]
    private float saneThreshHold = 75.0f;

    //If sanity is under this value , but above insaneThreshHold, the character is considered Borderline
    //If sanity is above this value , but under saneThreshHold, the character is considered Medium
    [SerializeField]
    public float mediumBorderlineThreshHold = 50.0f;

    //If sanity is under this value , the character is considered Insane
    [SerializeField]
    public float insaneThreshHold = 25.0f;

    //Maximum amount of Sanity 
    [SerializeField]
    public float maxSanity = 100.0f;

    //Maximum size of the sanity potion
    [SerializeField]
    private float maxSanityPotionSize = 100.0f;

    //Amount of sanity restored per point of sanityPotion consumed 
    [SerializeField]
    private float sanityPotionHealCoefficient = 7.5f;

    //Amount of sanity restored when killing an Eldritch 
    [SerializeField]
    private float sanityPotionRefill = 5;

    //ATTRIBUTES 


    //private HealthBar sanityBar;

    public bool sanityLocked = false;

    private float sanity; // From 0 to maxSanity

    public float Sanity
    {
        get => sanity;
        set
        {
            if (!sanityLocked)
            {
                var previous_sanity = sanity;
                sanity = Mathf.Clamp(value, 0, maxSanity); // restrain sanity value to [0,maxSanity]
                if (sanity != previous_sanity)
                    OnSanityUpdate(sanity, previous_sanity - sanity);
            }
        }
    }

    public float GetSanityPercentage()
    {
        return ((Sanity / maxSanity));
    }

    public float GetPotionSanityPercentage()
    {
        return ((SanityPotion / maxSanityPotionSize));
    }

    public float SanityPotion
    {
        get => sanityPotion;
        set
        {
            sanityPotion = Mathf.Clamp(value, 0, maxSanityPotionSize); // restrain sanity value to [0,maxSanityPotionSize]
            OnSanityPotionUpdate(sanityPotion);          
        }
    }

    private float sanityPotion; // From 0 to maxSanityPotionSize


    //METHODS  


    // Constructor
    public MentalHealth()
    {
        sanity = maxSanity; ;
        sanityPotion = 100.0f;
        currentState = GetStateFromSanity(sanity);
    }

    public void Initialize()
    {
        sanityLocked = false;
        Sanity = maxSanity; ;
        sanityPotion = 100.0f;
        currentState = GetStateFromSanity(sanity);
        currentState.Enter();
        GameManager.Instance.sanity.Init(insaneThreshHold, maxSanity);


        //GameManager.Instance.SwitchValueToDisplay();
        //OnSanityUpdate(sanity);
    }

    // Use your sanity potion to restore sanity based on how long the 
    // player keeps the key down

    public void UsePotion(float deltaTime /*POSITIF*/)
    {
        //If sanity potion is empty or if Sanity is full or if sanity is locked, leave
        if (SanityPotion == 0.0f || Sanity == maxSanity || sanityLocked) return;

        //You can not consume more potion than you have
        float amount = Mathf.Clamp(deltaTime * sanityPotionHealCoefficient, 0, Mathf.Min(SanityPotion, maxSanity - Sanity));
        Sanity += amount;
        SanityPotion -= amount;
    }

    public void RefillPotion()
    {
        SanityPotion += sanityPotionRefill;
            
    }

    public void LoseSanity(float value)
    {
        Sanity -= value;
    }

    public void HealSanity(float value)
    {
        Assert.IsTrue(value >= 0.0f);
        Sanity += value;

    }

    public void FullyHealSanity()
    {
        Sanity = maxSanity;
    }

    public void InitializeSanityPotion(float initialAmont = 0.0f)
    {
        SanityPotion = initialAmont;
    }

    //EVENTS


    // This code is called whenever the sanity value is changed
    // Actions that should be triggered by such a modification should
    // be put in this method and NOT in the setter
    void OnSanityUpdate(float value, float delta)
    {
        if (delta > 0)
        {
            GameManager.Instance.sanity.TakeDamage(delta);
        }
        else
        {
            GameManager.Instance.sanity.Heal(-1.0f * delta);

        }

        //GameManager.Instance.UpdateValueToDisplay();
        MentalHealthState targetState = GetStateFromSanity(sanity);

        if (currentState != targetState)
        {
            currentState.Leave();
            targetState.Enter();
            currentState = targetState;
            GameManager.Instance.ChangeMentalState(currentState);
        }



    }

    // This code is called whenever the sanity potion value is changed
    // Actions that should be triggered by such a modification should
    // be put in this method and NOT in the setter
    void OnSanityPotionUpdate(float value = 0)
    {
        GameManager.Instance.UpdateSanityPotionDisplay();
    }


    private MentalHealthState GetStateFromSanity(float value)
    {
        //Debug.Log("[GetStateFromSanity] sanity is " + value);
        if (sanity > saneThreshHold) return sane;
        if (sanity > mediumBorderlineThreshHold) return medium;
        if (sanity > insaneThreshHold) return borderline;
        return insane;
    }

    public bool IsSane()
    {
        return currentState == sane;
    }

    public bool IsMedium()
    {
        return currentState == medium;
    }

    public bool IsBorderline()
    {
        return currentState == borderline;
    }

    public bool IsInsane()
    {
        return currentState == insane;
    }
    
    public void existInsanity()
    {
        if (IsInsane())
        {
            sanityLocked = false;
            currentState.Leave();
            currentState = borderline;
            currentState.Enter();
            GameManager.Instance.ChangeMentalState(currentState);

        } else
        {
            Debug.Log("Tried to exit insane state while not being insane");
        }
    }

    public void GetInsane()
    {
        Sanity = insaneThreshHold - 1.0f;
    }

    public void UpgradePotionSize()
    {
        var oldRatio = SanityPotion / maxSanityPotionSize;
        maxSanityPotionSize = 150.0f;
        SanityPotion = oldRatio * maxSanityPotionSize;
    }
}
