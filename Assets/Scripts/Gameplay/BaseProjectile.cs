﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
    public float damage;

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            this.gameObject.transform.position = new Vector3(100, -50, 100);
            this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            GameManager.Instance.Pool.ReleaseObject(this.gameObject);
            PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(damage);
        }
        else if (other.CompareTag("ParryHitBox"))
        {
            this.gameObject.transform.position = new Vector3(100, -50, 100);
            this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            GameManager.Instance.Pool.ReleaseObject(this.gameObject);
        }
        else if(other.CompareTag("Building"))
        {
            this.gameObject.transform.position = new Vector3(100, -50, 100);
            this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            GameManager.Instance.Pool.ReleaseObject(this.gameObject);
        }
    }
}
