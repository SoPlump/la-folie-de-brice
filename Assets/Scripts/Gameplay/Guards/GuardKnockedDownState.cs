﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardKnockedDownState : GuardState
{

    private float stunDuration;

    public bool hasNoHp;

    public void Initialize(float distance, float speed)
    {
        base.Initialize(distance, speed);
    }

    public override void LaunchAttack()
    {

    }

    public override void Move(Vector3 playerPosition)
    {
        guard.GetComponent<Rigidbody>().velocity = Vector3.zero;
        guard.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }

    public override void OnEnter()
    {
        if (stunDuration == 6.0f)
            hasNoHp = true;
        StartCoroutine(ButIGetUpAgain(stunDuration));
    }

    public override void Rotate(Vector3 playerPosition)
    {

    }

    public void SetDuration(float newDuration)
    {
        stunDuration = newDuration;
    }

    IEnumerator ButIGetUpAgain(float duration)
    {
        animator.SetBool("walking", false);
        animator.SetBool("stunned", true);
        yield return new WaitForSeconds(duration);
        if(duration != 6.0f)
        {
            if(!hasNoHp)
            {
                animator.SetBool("stunned", false);
                guard.GetComponent<GuardBehaviour>().StopBeingKO();
            }
        }
        else
        {
            animator.SetBool("stunned", false);
            hasNoHp = false;
            guard.GetComponent<GuardBehaviour>().StopBeingKOFromZeroHealth();
        }
    }
}
