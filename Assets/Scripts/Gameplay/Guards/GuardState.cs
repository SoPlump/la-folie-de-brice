﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GuardState : MonoBehaviour
{
    protected float distanceToPlayer; // Distance (between the player and the boss) that the boss will try to keep
    protected float movementSpeed;

    [System.NonSerialized]
    public GameObject guard;

    [System.NonSerialized]
    public Animator animator;

    protected Baton baton;


    public virtual void Initialize(float distance, float speed)
    {
        distanceToPlayer = distance;
        movementSpeed = speed;
        guard = this.gameObject;
        animator = gameObject.GetComponent<Animator>();

        baton = gameObject.GetComponentInChildren<Baton>();
    }

    public abstract void LaunchAttack();

    public abstract void Move(Vector3 playerPosition);

    public abstract void Rotate(Vector3 playerPosition);

    public abstract void OnEnter();
}
