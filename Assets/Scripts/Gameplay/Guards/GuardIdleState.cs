﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// If idle, the guard moves a bit but not too much, and he parries if Brice is getting close
public class GuardIdleState : GuardState
{

    private int hitsBeforeCounter;
    private float guardDuration;
    private float guardCooldown;
    private bool isParrying;
    private int hitsRecieved;

    [SerializeField]
    private float guardTimer;
    [SerializeField]
    private float guardDurationTimer;

    private float hitAnimationTime;
    private float counterAnimationTime;

    private float counterDamage;

    private bool countering;

    private float distanceToGuard;

    public void Initialize(float distance, float speed, float guardDuration, float guardCooldown, 
        int maxHits, float hitTime, float counterTime, float counterDamage, float distanceToGuard)
    {
        base.Initialize(distance, speed);
        hitsBeforeCounter = maxHits;
        this.guardDuration = guardDuration;
        this.guardCooldown = guardCooldown;

        hitAnimationTime = hitTime;
        counterAnimationTime = counterTime;
        this.counterDamage = counterDamage;

        this.distanceToGuard = distanceToGuard;
    }

    public override void LaunchAttack()
    {
        if (countering)
            return;

        //Debug.Log("d = " + Vector3.Distance(transform.position, Player.Instance.transform.position));
        if(!isParrying && guardTimer >= guardCooldown 
            && Vector3.Distance(transform.position, Player.Instance.transform.position) <= distanceToGuard)
        {
            animator.SetBool("walking", false);
            isParrying = true;

            guard.GetComponent<GuardBehaviour>().SwitchHitboxes();
            // Set hitboxes (switch between parry and not)

            animator.SetBool("isParrying", true);
            // Set parry animation
        }
        else if(isParrying)
        {

            //Check if he recieved enough hits
            if(hitsRecieved > hitsBeforeCounter)
            {
                // Launch counterattack and stop parrying
                StartCoroutine(LaunchCounterattack());
                //StopParry();
            }
            else
            {
                if (guardDurationTimer >= guardDuration || Vector3.Distance(transform.position, Player.Instance.transform.position) > distanceToGuard)
                {
                    StopParry();
                }
            }
        }
    }

    public override void Move(Vector3 playerPosition)
    {

        if (!isParrying && Vector3.Distance(transform.position, Player.Instance.transform.position) > distanceToPlayer)
        {
            animator.SetBool("walking", true);
            Vector3 moveVector = guard.transform.forward;
            moveVector.y = 0.0f;
            guard.GetComponent<Rigidbody>().AddForce(moveVector * movementSpeed);
        }
        else
        {
            guard.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            guard.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            if (isParrying && Vector3.Distance(transform.position, Player.Instance.transform.position) < distanceToPlayer * 0.8f)
            {
                animator.SetBool("walking", false);
            }

        }
    }

    private void StopParry()
    {
        animator.SetBool("isParrying", false);
        isParrying = false;
        guardDurationTimer = 0.0f;
        guardTimer = 0.0f;
        hitsRecieved = 0;
        guard.GetComponent<GuardBehaviour>().SwitchHitboxes();
    }

    public override void OnEnter()
    {
        isParrying = false;
        guardTimer = guardCooldown;
        guardDurationTimer = 0.0f;
        hitsRecieved = 0;
        countering = false;
        baton.gameObject.GetComponent<CapsuleCollider>().enabled = false;
    }

    public override void Rotate(Vector3 playerPosition)
    {
        transform.LookAt(new Vector3(playerPosition.x, /*player.position.y + 1.0f*/ transform.position.y, playerPosition.z));
    }

    public void HitWhileParrying()
    {
        if(hitsRecieved < hitsBeforeCounter)
        {
            StartCoroutine(HitInGuard());
        }
        hitsRecieved++;
    }

    public void GuardBroke()
    {
        StartCoroutine(GuardBreak());
    }

    private void Update()
    {
        if(isParrying)
        {
            guardDurationTimer += Time.deltaTime;
        }
        else
        {
            guardTimer += Time.deltaTime;
        }
    }

    IEnumerator GuardBreak()
    {
        animator.SetBool("guardBroken", true);
        //StopParry();
        guard.GetComponent<GuardBehaviour>().GetKnockedDown(3.5f);
        yield return new WaitForSeconds(1.0f);
        animator.SetBool("guardBroken", false);
    }
    

    IEnumerator HitInGuard()
    {
        //Trigger the 'hit while parrying' animation
        animator.SetBool("damagedInGuardB", true);

        yield return new WaitForSeconds(hitAnimationTime/2.0f);
        //Trigger the 'hit while parrying' animation
        animator.SetBool("damagedInGuardB", false);

    }

    IEnumerator LaunchCounterattack()
    {
        countering = true;
        // Start the counterattack Animation
        animator.SetBool("counterTrigger", true);
        baton.SetDamage(counterDamage);

        // Enable hitbox of baton at some point
        yield return new WaitForSeconds(counterAnimationTime/4.0f);
        baton.gameObject.GetComponent<CapsuleCollider>().enabled = true;
        animator.SetBool("counterTrigger", false);

        yield return new WaitForSeconds(3*counterAnimationTime / 4.0f);
        // Disable it
        baton.gameObject.GetComponent<CapsuleCollider>().enabled = false;
        StopParry();
        countering = false;
    }
}
