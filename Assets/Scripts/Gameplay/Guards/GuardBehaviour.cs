﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardBehaviour : EnemyBehaviour
{
    private GuardState actualState;

    private GuardAggroState aggroState;
    private GuardIdleState idleState;
    private GuardKnockedDownState koState;
    private GuardVanquishedState vanquishedState;

    private GameObject parryBox;
    private GameObject guardBox;

    public float guardDuration;
    public float guardCooldown;
    public float counterDamage;

    public float chargeSpeed;
    public float chargeDamage;

    public float distanceToPlayerInIdle;
    public float walkingSpeed;
    public float distanceToParryInIdle;

    private bool isVanquished;

    [SerializeField]
    private float attackTiming;
    [SerializeField]
    private float switchTime;

    private bool isAggressive;


    // Start is called before the first frame update
    void Start()
    {
        player = Player.Instance.transform;

        //Get animations duration
        AnimationClip[] clips = gameObject.GetComponent<Animator>().runtimeAnimatorController.animationClips;

        float hitTime = 0.0f;
        float counterTime = 0.0f;
        float attackTime = 0.0f;
        foreach (AnimationClip clip in clips)
        {
            switch (clip.name)
            {
                case "Armed-Block-R-GetHit1":
                    hitTime = clip.length;
                    break;
                case "Sword-Attack-R7":
                    counterTime = clip.length;
                    break;
                case "Sword-Attack-R3":
                    attackTime = clip.length;
                    break;

            }
        }

        // Init states
        idleState = this.gameObject.GetComponent<GuardIdleState>();
        idleState.Initialize(distanceToPlayerInIdle, walkingSpeed, guardDuration, guardCooldown, 1, hitTime, counterTime, counterDamage, distanceToParryInIdle);
        aggroState = this.gameObject.GetComponent<GuardAggroState>();
        aggroState.Initialize(0.0f, chargeSpeed, attackTime, chargeDamage);
        koState = this.gameObject.GetComponent<GuardKnockedDownState>();
        koState.Initialize(0.0f, 0.0f);
        vanquishedState = gameObject.GetComponent<GuardVanquishedState>();
        vanquishedState.Initialize(0.0f, 0.0f);
        //Init hitboxes
        guardBox = gameObject.transform.Find("GuardHitBox").gameObject;
        parryBox = gameObject.transform.Find("ParryHitBox").gameObject;

        ChangeParryBox(false);
        GetToIdleState();

        isAggressive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isAggressive)
        {
            // Switch between attacks here !
            if (actualState == idleState)
            {
                attackTiming += Time.deltaTime;
            }

            if (attackTiming > switchTime)
            {
                attackTiming = 0.0f;
                Charge();
            }
        }
    }

    public bool IsAggro()
    {
        return isAggressive;
    }

    public void BecomeAggro()
    {
        isAggressive = true;
    }

    public override void FireAttack()
    {
        if (isAggressive)
        {
            actualState.LaunchAttack();
        }
    }

    public override void Move()
    {
        if (isAggressive)
        {
            actualState.Move(player.transform.position);
        }
    }

    public override void Release()
    {
        
    }

    public override void RotateToFacePlayer()
    {
        actualState.Rotate(player.transform.position);
    }

    public void GetKnockedDown(float duration) // But he gets up again
    {
        if (isVanquished)
        {
            return;
        }
        actualState = koState;
        koState.SetDuration(duration);
        koState.OnEnter();
        ChangeGuardBox(true);
        ChangeParryBox(false);
    }

    public void StopBeingKO()
    {
        if (isVanquished)
        {
            return;
        }
        GetToIdleState();
    }

    public void StopBeingKOFromZeroHealth()
    {
        if(isVanquished)
        {
            return;
        }
        Regenerate();
        GetToIdleState();
    }

    public void Charge()
    {
        if (isVanquished)
        {
            return;
        }
        actualState = aggroState;
        aggroState.OnEnter();
        ChangeGuardBox(false);
        ChangeParryBox(false);
    }

    public void GetToIdleState()
    {
        switchTime = Random.Range(7.0f, 10.0f);
        if (isVanquished)
        {
            return;
        }
        actualState = idleState;
        idleState.OnEnter();
        ChangeGuardBox(true);
        ChangeParryBox(false);
    }

    public void SwitchHitboxes()
    {
        guardBox.SetActive(!guardBox.activeInHierarchy);
        guardBox.GetComponent<Collider>().enabled = guardBox.activeInHierarchy;
        parryBox.SetActive(!parryBox.activeInHierarchy);
        parryBox.GetComponent<Collider>().enabled = parryBox.activeInHierarchy;
    }

    public void ChangeParryBox(bool active)
    {
        parryBox.SetActive(active);
        parryBox.GetComponent<Collider>().enabled = active;
    }

    public void ChangeGuardBox(bool active)
    {
        guardBox.SetActive(active);
        guardBox.GetComponent<Collider>().enabled = active;
    }

    public void Regenerate()
    {
        gameObject.GetComponent<GuardHealth>().Regenerate();
    }

    public bool Executed()
    {
        if(actualState != koState)
        {
            return false;
        }
        GetVanquished();
        return true;
    }

    private void GetVanquished()
    {
        isVanquished = true;
        actualState = vanquishedState;
        ChangeGuardBox(false);
        ChangeParryBox(false);
        vanquishedState.OnEnter();
    }

    IEnumerator testCharge()
    {
        yield return new WaitForSeconds(5.0f);
        Charge();
    }

}
