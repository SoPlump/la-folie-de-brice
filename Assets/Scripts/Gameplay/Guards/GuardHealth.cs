﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardHealth : EnemyHealth
{

    private void Start()
    {
        base.Init();
        executeThreshold = 0.0f;        
    }

    public override void TakeDamage(float damage)
    {
        if(!gameObject.GetComponent<GuardBehaviour>().IsAggro())
        {
            return;
        }
        health -= damage;
        healthBar.TakeDamage(damage);
        // If health gets to zero, knowk down for a bit then put health back to a percentage of max_health with Regenerate
        if (health <= 0 && health + damage > 0)
        {
            health = 0.0f;
            //Set to koState
            gameObject.GetComponent<GuardBehaviour>().GetKnockedDown(6.0f);
        }
        if (!execEffectDisplayed && IsInExecuteRange())
        {
            DisplayEffect();
        }
        else if (execEffectDisplayed && !IsInExecuteRange())
        {
            HideEffect();
        }
    }

    public override void Execute()
    {
        // Knock the guard down forever (kinda)
        bool isExecuted = gameObject.GetComponent<GuardBehaviour>().Executed();
        if (isExecuted)
        {
            HideEffect();
            executeThreshold = -1.0f;
        }
    }

    public void Regenerate()
    {
        healthBar.TakeDamage(-0.4f * maxHealth);
        health = maxHealth * 0.4f;
        HideEffect();
    }
}
