﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Baton : MonoBehaviour
{

    private float damage;
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("ParryHitBox"))
        {
            gameObject.GetComponent<CapsuleCollider>().enabled = false;
        }
        else if(other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
            gameObject.GetComponent<CapsuleCollider>().enabled = false;
        }
    }

    public void SetDamage(float damage)
    {
        this.damage = damage;
    }
}
