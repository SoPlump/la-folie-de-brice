﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardVanquishedState : GuardState
{
    public override void LaunchAttack()
    {

    }

    public override void Move(Vector3 playerPosition)
    {

    }

    public override void OnEnter()
    {
        animator.SetTrigger("executed");
        guard.gameObject.GetComponent<Rigidbody>().drag = 1.0f;
        guard.gameObject.GetComponent<Rigidbody>().angularDrag = 1.0f;
        FightManager.Instance.GuardKilled();
    }

    public override void Rotate(Vector3 playerPosition)
    {

    }
}
