﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardAggroState : GuardState
{
    private float attackAnimationTime;
    private float attackDamage;

    private bool attackDone;

    public void Initialize(float distance, float speed, float animTime, float damage)
    {
        base.Initialize(distance, speed);
        attackAnimationTime = animTime;
        attackDamage = damage;
    }

    public override void LaunchAttack()
    {
        if (!attackDone && Vector3.Distance(guard.transform.position, Player.Instance.transform.position) <= 2.0f)
        {
            StartCoroutine(LaunchSmash());
        }
    }

    public override void Move(Vector3 playerPosition)
    {
        guard.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        guard.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        if (attackDone)
            return;
        animator.SetBool("isCharging", true);
        float distance = Vector3.Distance(guard.transform.position, playerPosition);
        if(distance > 1.0f)
        {
            Vector3 moveVector = guard.transform.forward;
            moveVector.y = 0.0f;
            guard.GetComponent<Rigidbody>().AddForce(moveVector * movementSpeed);
        }
    }

    public override void OnEnter()
    {
        attackDone = false;
        animator.SetBool("isParrying", false);
        animator.SetBool("walking", false);
        if (!attackDone && Vector3.Distance(guard.transform.position, Player.Instance.transform.position) <= 2.0f)
        {
            StartCoroutine(LaunchSmash());
        }
    }

    public override void Rotate(Vector3 playerPosition)
    {
        transform.LookAt(new Vector3(playerPosition.x, /*player.position.y + 1.0f*/ transform.position.y, playerPosition.z));
    }

    IEnumerator LaunchSmash()
    {
        // Reset velocities
        guard.GetComponent<Rigidbody>().velocity = Vector3.zero;
        guard.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        attackDone = true;
        // Start Animation
        animator.SetBool("chargeAttackTrigger", true);
        baton.SetDamage(attackDamage);

        yield return new WaitForSeconds(attackAnimationTime / 5.0f);
        animator.SetBool("isCharging", false);
        baton.gameObject.GetComponent<CapsuleCollider>().enabled = true;
        yield return new WaitForSeconds(3.0f * attackAnimationTime / 5.0f);
        animator.SetBool("chargeAttackTrigger", false);

        yield return new WaitForSeconds(attackAnimationTime / 5.0f);
        baton.gameObject.GetComponent<CapsuleCollider>().enabled = false;

        guard.GetComponent<GuardBehaviour>().GetToIdleState();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
