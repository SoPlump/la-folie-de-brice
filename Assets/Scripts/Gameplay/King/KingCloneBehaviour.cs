﻿using System.Collections;
using UnityEngine;

public class KingCloneBehaviour : EnemyBehaviour
{

    public float distanceToPlayer;

    public float cloneDestructionDamage;
    public float debuffDuration;
    public float debuffEfficiency;

    [SerializeField]
    private int numberOfProjectiles = 150;

    private Animator animator;

    private bool isSwitching;

    void Start()
    {
        player = Player.Instance.transform;
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        attackTimer += Time.deltaTime;
    }

    public void SetSwitching(bool state)
    {
        isSwitching = state;
    }

    public void SetCollider(bool state)
    {
        gameObject.GetComponent<Collider>().enabled = state;
    }

    public override void FireAttack()
    {
        // Pretty much the same as the base state

        if (!isSwitching && attackTimer >= attackCooldown)
        {
            Vector3 projectileBasePos = new Vector3(gameObject.transform.position.x, 1.0f, gameObject.transform.position.z);
            //projectileBasePos += gameObject.transform.forward * 5.0f;
            GameObject projectile = GameManager.Instance.Pool.GetObject("Boss02_Clone_Projectile");
            projectile.transform.position = projectileBasePos;
            projectile.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            projectile.GetComponent<Rigidbody>().AddForce(transform.forward * 450);
            projectile.GetComponentInChildren<Boss2_Clone_SpellProjectile>().damage = spellDamage;
            StartCoroutine(projectile.GetComponentInChildren<Boss2_Clone_SpellProjectile>().ReleaseThis());
            attackTimer = 0.0f;
        }
    }

    public override void Move()
    {
        this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        if (Vector3.Distance(transform.position, player.position) <= spotRange && Vector3.Distance(transform.position, player.position) <= distanceToPlayer)
        {
            //transform.position += transform.forward * movementSpeed * Time.deltaTime;
            Vector3 moveVector = transform.forward;
            moveVector.y = 0.0f;
            this.gameObject.GetComponent<Rigidbody>().AddForce(moveVector * -movementSpeed);
        }
    }

    public override void Release()
    {
        this.GetComponent<EnemyHealth>().health = this.GetComponent<EnemyHealth>().maxHealth;
        GameManager.Instance.Pool.ReleaseObject(this.gameObject);
    }

    public override void RotateToFacePlayer()
    {
        transform.LookAt(new Vector3(player.position.x, transform.position.y, player.position.z));
    }

    public void SetTimer(float timer)
    {
        attackTimer = timer;
    }

    public void OnDeath()
    {
        StartCoroutine(DeathEffect());
    }

    public void SetCloneAlpha(float alpha)
    {
        var renderers = gameObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer renderer in renderers)
        {
            foreach (Material material in renderer.materials)
            {
                if (material.HasProperty("_Color"))
                {
                    var color = material.color;
                    color.a = alpha;
                    material.color = color;
                }
            }
        }
    }

    IEnumerator DeathEffect()
    {
        //GameObject explosion;
        movementSpeed = 0;
        // Launch explosion effect
        //explosion = GameManager.Instance.Pool.GetObject("Clone_Explosion");
        //explosion.transform.position = gameObject.transform.position;
        //StartCoroutine(explosion.GetComponent<CloneExplosion>().StopThis());
        // Shoot projectiles
        Transform tr = gameObject.transform;
        for (int i = 0; i < numberOfProjectiles; ++i)
        {
            GameObject projectile = GameManager.Instance.Pool.GetObject("Clone_Explosion_Projectile");
            projectile.transform.position = new Vector3(tr.position.x, 0.7f, tr.position.z);
            projectile.transform.rotation = tr.rotation;
            tr.Rotate(0, 360.0f / (float)numberOfProjectiles, 0);
            projectile.GetComponent<Rigidbody>().AddForce(projectile.transform.forward * 1500);
            projectile.GetComponentInChildren<CloneExplosionProjectile>().damage = cloneDestructionDamage;
            projectile.GetComponentInChildren<CloneExplosionProjectile>().debuffDuration = debuffDuration;
            projectile.GetComponentInChildren<CloneExplosionProjectile>().debuffEfficiency = debuffEfficiency;
        }
        GameObject.Find("King").GetComponent<KingBehaviour>().CloneDead();
        EnemyManager.Instance.CloneDespawned(this.gameObject);
        yield return new WaitForEndOfFrame();
    }
}
