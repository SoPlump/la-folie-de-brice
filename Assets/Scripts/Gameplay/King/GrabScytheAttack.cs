﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabScytheAttack : MonoBehaviour
{
    [System.NonSerialized]
    public float grabDamage;

    [System.NonSerialized]
    public float failDamage;

    [System.NonSerialized]
    public float attackTime;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gameObject.GetComponent<Collider>().enabled = false;
            GameObject.Find("King").GetComponent<KingBehaviour>().HitGrab(); ;
            PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
            StartCoroutine(GrabbedQTE(playerHealth));
        }
    }

    IEnumerator GrabbedQTE(PlayerHealth ph)
    {
        Animator anim = GameObject.Find("King").GetComponent<Animator>();
        yield return StartCoroutine(ScenarioManager.Instance.WaitForQTE());
        if (!GameManager.Instance.QTEisSuccess)
        {
            anim.SetBool("failQTE", true);
            yield return new WaitForSeconds(0.5f * attackTime);
            anim.SetBool("failQTE", false);
            ph.TakeDamage(failDamage);
        }
        ph.TakeDamage(grabDamage);
        GameObject.Find("King").GetComponent<KingBehaviour>().FinishedGrabbing();
    }
}
