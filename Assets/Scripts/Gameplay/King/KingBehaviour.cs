﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingBehaviour : EnemyBehaviour
{
    private bool inGuardState;

    public float safeRange;

    public float timeToFire;

    private BoxCollider scytheCollider;

    public float flameRange;
    private bool isSpittingFlames = false;
    private float spittingTime;
    public float prewarmDelay = 2.0f;
    public float fireDuration = 5.0f;

    public float bigSpellDamage;

    public GameObject laserProjectile;
    public Animator kingAnimator;

    public float wallCooldown;
    private float wallTimer;
    public float wallSpeed;

    private Vector3 wallPosition;

    private GameObject wallInstance;
    private GameObject wallInstance2;
    private GameObject wallInstance3;
    public GameObject expandingWall;

    private float bumpTimer;
    public float bumpCooldown;
    public float bumpEscapeTime;

    private Vector3 bumpPosition;
    private Vector3 bumpPosition2;
    private Vector3 bumpPosition3;
    private Vector3 bumpPosition4;

    private GameObject bumpInstance;
    private GameObject bumpInstance2;
    private GameObject bumpInstance3;
    private GameObject bumpInstance4;

    private GameObject bumpAreaIndicatorInstance;
    private GameObject bumpAreaIndicatorInstance2;
    private GameObject bumpAreaIndicatorInstance3;
    private GameObject bumpAreaIndicatorInstance4;

    public GameObject bumpCollider;
    public GameObject bumpAreaIndicator;

    private GameObject blastFire;
    private GameObject fireHitBox;

    private Vector3 spawnCentre;
    private float spawnSizeX;
    private float spawnSizeZ;

    // Boss 2

    private bool isGrabbing;
    private float grabAnimationTime = 0.0f;
    private float grabHitTime = 0.0f;
    public float grabDamage;
    public float grabFailDamage;

    private bool grabHit;

    private bool isCloned;
    private float cloneSpawnAnimTime = 0.0f;

    public float distanceToPlayer;
    public float cloneDestructionDamage;
    public float debuffDuration;
    public float debuffEfficiency;
    public float spellCooldown;

    private float projectileTimer;
    public float projectileCooldown;

    private float switchPosTimer;
    private float switchPosCooldown;

    private float destructionTimer;
    private float destructionCooldown;

    private float cloneDestructionAnimTime = 0.0f;

    private bool isSwitching;
    private bool isDetonating;

    private int cloneAlive;

    private float cloningTimer;
    private float cloningCooldown;
    private float grabbingTimer;
    private float grabbingCooldown;

    private GameObject eldritchSpawnZone;


    Vector3 kingSize;

    public override void FireAttack()
    {
        if(isGrabbing)
        {

        }
        else if (isCloned)
        {
            if(!isSwitching && !isDetonating && projectileTimer >= projectileCooldown)
            {
                Vector3 projectileBasePos = new Vector3(gameObject.transform.position.x, 1.0f, gameObject.transform.position.z);
                //projectileBasePos += boss.transform.forward * 5.0f;
                GameObject projectile = GameManager.Instance.Pool.GetObject("Boss02_Projectile");
                projectile.transform.position = projectileBasePos;
                projectile.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
                projectile.GetComponent<Rigidbody>().AddForce(transform.forward * 500);
                projectile.GetComponentInChildren<Boss2_SpellProjectile>().damage = spellDamage;
                StartCoroutine(projectile.GetComponentInChildren<Boss2_SpellProjectile>().ReleaseThis());
                projectileTimer = 0.0f;
            }
            if(!isSwitching && !isDetonating && switchPosTimer >= switchPosCooldown)
            {
                StartCoroutine(SwitchPositions());
            }
            else if (!isSwitching && !isDetonating && destructionTimer >= destructionCooldown)
            {
                StartCoroutine(DetonateClones());
            }
        }
        else
        {
            if (attackTimer >= attackCooldown)
            {
                //int attackType = Random.Range(0, 3);
                if (EnemyManager.Instance.EldritchCount() < 6)
                {
                    kingAnimator.SetTrigger("spawn");
                    SpawnEldritchs();
                }
                else if (Vector3.Distance(transform.position, player.position) <= flameRange && spittingTime < 5.0f && !isSpittingFlames)
                {
                    //animation
                    StartCoroutine(WaitForFire());
                }
                else if (!isSpittingFlames)
                {
                    kingAnimator.SetTrigger("spellA");
                    FireLaser();
                }
            }
            SpawnWall();
            DoBumps();
        }
    }

    public override void Move()
    {
        this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        if (!inGuardState && Vector3.Distance(transform.position, player.position) <= spotRange && Vector3.Distance(transform.position, player.position) <= safeRange)
        {
            //transform.position += transform.forward * movementSpeed * Time.deltaTime;
            Vector3 moveVector = transform.forward;
            moveVector.y = 0.0f;
            this.gameObject.GetComponent<Rigidbody>().AddForce(moveVector * -movementSpeed);
        }
    }

    public override void RotateToFacePlayer()
    {
        transform.LookAt(new Vector3(player.position.x, player.position.y, player.position.z));
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Transform>();
        kingSize = gameObject.GetComponent<Collider>().bounds.size;
        kingAnimator = this.gameObject.GetComponent<Animator>();
        this.gameObject.GetComponent<KingHealth>().animator = kingAnimator;

        scytheCollider = gameObject.GetComponentInChildren<BoxCollider>();
        scytheCollider.enabled = false;

        AnimationClip[] clips = kingAnimator.runtimeAnimatorController.animationClips;
        foreach (AnimationClip clip in clips)
        {
            switch(clip.name)
            {
                case "scytheAttack2":
                    grabAnimationTime = clip.length;
                    break;
                case "scytheAttack1":
                    grabHitTime = clip.length;
                    break;
                case "scytheAttack3":
                    cloneSpawnAnimTime = clip.length;
                    cloneDestructionAnimTime = clip.length;
                    break;
            }
        }

        eldritchSpawnZone = GameObject.Find("EldritchSpawnZone");

        spawnCentre = eldritchSpawnZone.transform.position;
        spawnSizeX = eldritchSpawnZone.GetComponent<Renderer>().bounds.size.x;
        spawnSizeZ = eldritchSpawnZone.GetComponent<Renderer>().bounds.size.z;
        //EnemyManager.Instance.EldritchsSpawned(this.gameObject);

        GrabScytheAttack grabAttack = gameObject.GetComponentInChildren<GrabScytheAttack>();
        grabAttack.grabDamage = grabDamage;
        grabAttack.failDamage = grabFailDamage;
        grabAttack.attackTime = grabHitTime;

        StartCoroutine(StartingSpawn());

        blastFire = GameObject.Find("FireBlast");
        fireHitBox = GameObject.Find("FlameHitBox");
        
        fireHitBox.SetActive(false);
        blastFire.SetActive(false);

        spittingTime = 0.0f;

        attackTimer = 0.0f;

        projectileTimer = 0.0f;

        switchPosTimer = 0.0f;
        switchPosCooldown = Random.Range(10.0f, 15.0f);

        destructionTimer = 0.0f;
        destructionCooldown = Random.Range(20.0f, 28.0f);

        cloningTimer = 0.0f;
        cloningCooldown = Random.Range(18.0f, 25.0f);


        grabbingCooldown = Random.Range(15.0f, 20.0f);
        grabbingTimer = grabbingCooldown -1.5f;
        this.enabled = false;

        // DELETE THIS LATER
        //EnemyManager.Instance.StartBossFight();
    }

    // Update is called once per frame
    void Update()
    {
        attackTimer += Time.deltaTime;
        wallTimer += Time.deltaTime;
        bumpTimer += Time.deltaTime;
        if(isCloned)
        {
            if (!isSwitching && !isDetonating)
            {
                projectileTimer += Time.deltaTime;
                switchPosTimer += Time.deltaTime;
                destructionTimer += Time.deltaTime;
            }
        }

        if(!isCloned && !isGrabbing)
        {
            if(grabbingTimer >=grabbingCooldown)
            {
                StartCoroutine(GrabAnimation());
            }
            else if(cloningTimer >= cloningCooldown)
            {
                StartCoroutine(SpawnClones());
            }
            else
            {
                grabbingTimer += Time.deltaTime;
                cloningTimer += Time.deltaTime;
            }
        }

        // Here we're going to orchestrate the fight



        /*
        if (EnemyManager.Instance.EldritchCount() >= 6)
        {
            inGuardState = true;
        }
        else
        {
            inGuardState = false;
        }
        */
    }

    private void SpawnEldritchs()
    {
        int spawnType = Random.Range(0, 100);
        if(spawnType <= 10)
        {
            EnemyManager.Instance.SpawnMeleeEldritch(eldritchSpawnZone);
        }
        else if (spawnType <= 30)
        {
            EnemyManager.Instance.SpawnRangedEldritch(eldritchSpawnZone);
        }
        else if (spawnType <= 50)
        {
            EnemyManager.Instance.SpawnRangedEldritch(eldritchSpawnZone);
            EnemyManager.Instance.SpawnRangedEldritch(eldritchSpawnZone);
        }
        else if (spawnType <= 70)
        {
            EnemyManager.Instance.SpawnRangedEldritch(eldritchSpawnZone);
            EnemyManager.Instance.SpawnMeleeEldritch(eldritchSpawnZone);
        }
        else if (spawnType <= 90)
        {
            EnemyManager.Instance.SpawnRangedEldritch(eldritchSpawnZone);
            EnemyManager.Instance.SpawnRangedEldritch(eldritchSpawnZone);
            EnemyManager.Instance.SpawnRangedEldritch(eldritchSpawnZone);
        }
        else
        {
            EnemyManager.Instance.SpawnRangedEldritch(eldritchSpawnZone);
            EnemyManager.Instance.SpawnMeleeEldritch(eldritchSpawnZone);
            EnemyManager.Instance.SpawnRangedEldritch(eldritchSpawnZone);
        }
        attackTimer = 0.0f;
    }

    // TODO : pour le test de la garde unquement
    private void FireLaser()
    {
        attackTimer = 0.0f;
        StartCoroutine(IncreaseSize());
    }

    IEnumerator IncreaseSize()
    {
        
        Vector3 position = this.gameObject.transform.position;
        position += transform.forward * 2.5f;
        GameObject clone = Instantiate(laserProjectile, position, transform.rotation);
        clone.GetComponent<BossProjectile>().damage = bigSpellDamage;
        clone.GetComponent<Rigidbody>().AddForce(clone.transform.forward * 0);
        float timer = 0.0f;
        float elapsed;
        float increase;
        while(timer < timeToFire)
        {
            elapsed = Time.deltaTime;
            Transform newTransform = clone.transform;
            increase = 4.0f * elapsed / timeToFire;
            newTransform.localScale += new Vector3(increase, increase, increase);
            newTransform.position = this.gameObject.transform.position + this.gameObject.transform.forward * 2.5f;
            clone.transform.position = newTransform.position;
            clone.transform.localScale = newTransform.localScale;
            clone.transform.rotation = this.gameObject.transform.rotation;
            timer += elapsed;
            yield return new WaitForSeconds(0.001f);
        }
        yield return new WaitForSeconds(2.0f);
        
        clone.GetComponent<Rigidbody>().AddForce(clone.transform.forward * 3000);
        Destroy(clone, 2.0f);
        
    }

    public bool IsGuarding()
    {
        return inGuardState;
    }

    IEnumerator StartingSpawn()
    {
        EnemyManager.Instance.SpawnMeleeEldritch(eldritchSpawnZone);
        EnemyManager.Instance.SpawnMeleeEldritch(eldritchSpawnZone);
        yield return new WaitForEndOfFrame();
    }

    public override void Release()
    {
        throw new System.NotImplementedException();
    }

    //Attaques du boss 1

    public void SpawnWall()
    {
        if (wallTimer >= wallCooldown)
        {
            wallTimer = 0.0f;

            wallPosition = new Vector3(transform.position.x, player.position.y, transform.position.z);
            Quaternion rotation = Quaternion.Euler(90, 0, 0);
            Quaternion rotation2 = Quaternion.Euler(90, 30, 0);
            Quaternion rotation3 = Quaternion.Euler(90, -30, 0);
            wallInstance = Instantiate(expandingWall, wallPosition, rotation);
            wallInstance2 = Instantiate(expandingWall, wallPosition, rotation2);
            wallInstance3 = Instantiate(expandingWall, wallPosition, rotation3);

            StartCoroutine(ExpandWall());
        }
    }

    IEnumerator ExpandWall()
    {
        wallPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Quaternion rotation = Quaternion.Euler(90, 0, 0);
        Quaternion rotation2 = Quaternion.Euler(90, 30, 0);
        Quaternion rotation3 = Quaternion.Euler(90, -30, 0);
        wallInstance = Instantiate(expandingWall, wallPosition, rotation);
        wallInstance2 = Instantiate(expandingWall, wallPosition, rotation2);
        wallInstance3 = Instantiate(expandingWall, wallPosition, rotation3);

        float elapsed = 0.0f;
        float increase = 0.0f;
        while (wallTimer < wallCooldown - 1.8f)
        {
            elapsed = Time.deltaTime;
            Transform newTransform = wallInstance.transform;
            Transform newTransform2 = wallInstance2.transform;
            Transform newTransform3 = wallInstance3.transform;
            increase = wallSpeed * elapsed;

            newTransform.localScale += new Vector3(increase, increase, 0);
            newTransform2.localScale += new Vector3(increase * 1.6f, increase * 1.6f, 0);
            newTransform3.localScale += new Vector3(increase * 2.2f, increase * 2.2f, 0);

            wallInstance.transform.localScale = newTransform.localScale;
            wallInstance2.transform.localScale = newTransform2.localScale;
            wallInstance3.transform.localScale = newTransform3.localScale;

            yield return new WaitForSeconds(0.001f);
        }

        Destroy(wallInstance, 0);
        Destroy(wallInstance2, 0);
        Destroy(wallInstance3, 0);
    }

    public void DoBumps()
    {
        if (bumpTimer >= bumpCooldown)
        {
            bumpTimer = 0.0f;

            bumpPosition = new Vector3(player.position.x, player.position.y, player.position.z);
            bumpAreaIndicatorInstance = Instantiate(bumpAreaIndicator, bumpPosition, player.rotation);

            bumpPosition2 = new Vector3(Random.Range(-35.0f, 35.0f), player.position.y, Random.Range(12.5f, 50.0f));
            bumpAreaIndicatorInstance2 = Instantiate(bumpAreaIndicator, bumpPosition2, player.rotation);

            bumpPosition3 = new Vector3(Random.Range(-35.0f, 35.0f), player.position.y, Random.Range(12.5f, 50.0f));
            bumpAreaIndicatorInstance3 = Instantiate(bumpAreaIndicator, bumpPosition3, player.rotation);

            bumpPosition4 = new Vector3(Random.Range(-35.0f, 35.0f), player.position.y, Random.Range(12.5f, 50.0f));
            bumpAreaIndicatorInstance4 = Instantiate(bumpAreaIndicator, bumpPosition4, player.rotation);

            StartCoroutine(WaitForBumpToOccur());
        }
    }

    public void CloneDead()
    {
        if(--cloneAlive == 0)
        {
            isCloned = false;
        }
    }

    IEnumerator DetonateClones()
    {
        isDetonating = true;
        kingAnimator.SetTrigger("cloneDestruction");
        float ANIM_TIME = 4.0f;
        float timer = 0.0f;
        float maxSize = 4.0f;
        float startSize = 1.0f;

        GameObject detonationCircle;
        detonationCircle = GameManager.Instance.Pool.GetObject("DetonationCircle");
        detonationCircle.transform.parent = gameObject.transform;
        detonationCircle.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        detonationCircle.transform.localPosition = new Vector3(0.0f, .5f, 0.0f);

        Vector3 localScale;
        while (timer < ANIM_TIME)
        {
            float elapsed = Time.deltaTime;
            float increase = elapsed / ANIM_TIME * (maxSize - startSize);
            float rotation = elapsed / ANIM_TIME * 360.0f;
            localScale = detonationCircle.transform.localScale;
            localScale.x += increase;
            localScale.y += increase;
            localScale.z += increase;
            detonationCircle.transform.localScale = localScale;
            detonationCircle.transform.Rotate(new Vector3(0.0f, 0.0f, rotation));
            timer += elapsed;
            yield return new WaitForEndOfFrame();
        }

        kingAnimator.SetTrigger("cloneDestructionEnd");
        yield return new WaitForSeconds(cloneDestructionAnimTime * 0.6f);
        GameManager.Instance.Pool.ReleaseObject(detonationCircle);
        GameObject[] clones = GameObject.FindGameObjectsWithTag("Clone");
        foreach(GameObject clone in clones)
        {
            clone.GetComponent<KingCloneBehaviour>().OnDeath();
        }
        isDetonating = false;
    }

    IEnumerator WaitForBumpToOccur()
    {
        float timer = 0.0f;
        float elapsed;
        float increase;
        while (timer < bumpEscapeTime)
        {
            elapsed = Time.deltaTime;
            Transform newTransform = bumpAreaIndicatorInstance.transform;
            Transform newTransform2 = bumpAreaIndicatorInstance2.transform;
            Transform newTransform3 = bumpAreaIndicatorInstance3.transform;
            Transform newTransform4 = bumpAreaIndicatorInstance4.transform;
            increase = 3.0f * elapsed / bumpEscapeTime;
            newTransform.localScale += new Vector3(0, increase, 0);
            newTransform2.localScale += new Vector3(0, increase, 0);
            newTransform3.localScale += new Vector3(0, increase, 0);
            newTransform4.localScale += new Vector3(0, increase, 0);
            bumpAreaIndicatorInstance.transform.localScale = newTransform.localScale;
            bumpAreaIndicatorInstance2.transform.localScale = newTransform2.localScale;
            bumpAreaIndicatorInstance3.transform.localScale = newTransform3.localScale;
            bumpAreaIndicatorInstance4.transform.localScale = newTransform4.localScale;
            timer += elapsed;
            yield return new WaitForSeconds(0.001f);
        }
        Destroy(bumpAreaIndicatorInstance, 1);
        Destroy(bumpAreaIndicatorInstance2, 1);
        Destroy(bumpAreaIndicatorInstance3, 1);
        Destroy(bumpAreaIndicatorInstance4, 1);

        bumpInstance = Instantiate(bumpCollider, bumpPosition, player.rotation);
        bumpInstance2 = Instantiate(bumpCollider, bumpPosition, player.rotation);
        bumpInstance3 = Instantiate(bumpCollider, bumpPosition, player.rotation);
        bumpInstance4 = Instantiate(bumpCollider, bumpPosition, player.rotation);

        bumpInstance.GetComponent<SphereCollider>().isTrigger = true;
        bumpInstance.GetComponent<Rigidbody>().AddForce(transform.up * 5000);
        bumpInstance2.GetComponent<SphereCollider>().isTrigger = true;
        bumpInstance2.GetComponent<Rigidbody>().AddForce(transform.up * 5000);
        bumpInstance3.GetComponent<SphereCollider>().isTrigger = true;
        bumpInstance3.GetComponent<Rigidbody>().AddForce(transform.up * 5000);
        bumpInstance4.GetComponent<SphereCollider>().isTrigger = true;
        bumpInstance4.GetComponent<Rigidbody>().AddForce(transform.up * 5000);
        Destroy(bumpInstance, 1);
        Destroy(bumpInstance2, 1);
        Destroy(bumpInstance3, 1);
        Destroy(bumpInstance4, 1);
    }

    IEnumerator WaitForFire()
    {
        spittingTime += Time.deltaTime;
        isSpittingFlames = true;
        blastFire.SetActive(true);

        yield return new WaitForSeconds(prewarmDelay);
        fireHitBox.SetActive(true);

        yield return new WaitForSeconds(fireDuration);
        fireHitBox.SetActive(false);
        blastFire.SetActive(false);
        spittingTime = 0.0f;
        isSpittingFlames = false;
    }

    // Boss 2

    // Clones
        // Anim de spawn = Schyte Attack 3

        
    IEnumerator SpawnClones()
    {
        gameObject.GetComponent<Collider>().enabled = false;
        isCloned = true;
        kingAnimator.SetBool("cloneSpawning", true);
        yield return new WaitForSeconds(cloneSpawnAnimTime * 0.5f);
        kingAnimator.SetBool("cloneSpawning", false);
        StartCoroutine(SpawnClonesInBattleField());
    }

    IEnumerator SpawnClonesInBattleField()
    {
        float oldCd = projectileCooldown;
        projectileCooldown = 9999.0f;
        cloneAlive = 2;
        GameObject spawnZone1 = GameObject.Find("CloneSpawnZone1");
        GameObject spawnZone2 = GameObject.Find("CloneSpawnZone2");

        Vector3 spawnZone1Size = spawnZone1.GetComponent<Renderer>().bounds.size;
        Vector3 spawnZone2Size = spawnZone2.GetComponent<Renderer>().bounds.size;

        // Find a suitable point in spawnZone1
        float posX1 = spawnZone1.transform.position.x + Random.Range(-spawnZone1Size.x, spawnZone1Size.x) / 2.0f;
        float posZ1 = spawnZone1.transform.position.z + Random.Range(-spawnZone1Size.z, spawnZone1Size.z) / 2.0f;

        // Find a suitable point in spawnZone2
        float posX2 = spawnZone2.transform.position.x + Random.Range(-spawnZone2Size.x, spawnZone2Size.x) / 2.0f;
        float posZ2 = spawnZone2.transform.position.z + Random.Range(-spawnZone2Size.z, spawnZone2Size.z) / 2.0f;

        Vector3 kingPos = gameObject.transform.position;
        Vector3 pos2 = new Vector3(posX1, gameObject.transform.position.y, posZ1);
        Vector3 pos3 = new Vector3(posX2, gameObject.transform.position.y, posZ2);


        Vector3[] positions = { kingPos, pos2, pos3 };

        // Now that we got the positions, we need to spawn our clones, but we also need to place our boss, so first we decide which
        // iteration will simply be a displacement of our boss
        int bossIndex = Random.Range(0, positions.Length);
        yield return new WaitForSeconds(2.2f);
        for (int i = 0; i < positions.Length; ++i)
        {
            if (i == bossIndex)
            {
                gameObject.transform.position = positions[i];
            }
            else
            {
                GameObject clone = GameManager.Instance.Pool.GetObject("KingClone");
                clone.transform.position = positions[i];
                clone.GetComponent<KingCloneBehaviour>().SetTimer(spellCooldown / (positions.Length - i)); // This line could go away, it's simply to have clone's attacks be delayed
                clone.GetComponent<KingCloneBehaviour>().distanceToPlayer = distanceToPlayer;
                clone.GetComponent<KingCloneBehaviour>().attackCooldown = spellCooldown;
                clone.GetComponent<KingCloneBehaviour>().spellDamage = spellDamage;
                clone.GetComponent<KingCloneBehaviour>().movementSpeed = movementSpeed;
                clone.GetComponent<KingCloneBehaviour>().cloneDestructionDamage = cloneDestructionDamage;
                clone.GetComponent<KingCloneBehaviour>().debuffDuration = debuffDuration;
                clone.GetComponent<KingCloneBehaviour>().debuffEfficiency = debuffEfficiency;
                EnemyManager.Instance.CloneSpawned(clone);
            }
        }
        projectileCooldown = oldCd; 
        cloningTimer = 0.0f;
        cloningCooldown = Random.Range(18.0f, 25.0f);
        destructionTimer = 0.0f;
        destructionCooldown = Random.Range(25.0f, 35.0f);
        switchPosTimer = 0.0f;
        switchPosCooldown = Random.Range(10.0f, 15.0f);
        gameObject.GetComponent<Collider>().enabled = true;
    }

    IEnumerator SwitchPositions()
    {
        isSwitching = true;
        gameObject.GetComponent<Collider>().enabled = false;
        // Get all clone positions and make them inactive
        GameObject[] clones = GameObject.FindGameObjectsWithTag("Clone");
        Vector3 kingPosition = gameObject.transform.position;
        Vector3[] positions = new Vector3[1 + clones.Length];
        positions[0] = kingPosition;
        int i = 1;
        foreach(GameObject clone in clones)
        {
            clone.GetComponent<KingCloneBehaviour>().SetCollider(false);
            clone.GetComponent<KingCloneBehaviour>().SetSwitching(true);
            positions[i++] = clone.transform.position;
        }

        //Set king and clones to transparent
        float time = 0.0f;
        float ANIM_DUARTION = 2.0f;
        float alpha = 1.0f;
        while (time < ANIM_DUARTION)
        {
            float elapsed = Time.deltaTime;
            alpha = alpha - (elapsed / ANIM_DUARTION);
            SetKingAlpha(alpha);
            foreach (GameObject clone in clones)
            {
                clone.GetComponent<KingCloneBehaviour>().SetCloneAlpha(alpha);
            }
            time += elapsed;
            yield return new WaitForEndOfFrame();
        }
        alpha = 0.0f;
        SetKingAlpha(alpha);
        foreach (GameObject clone in clones)
        {
            clone.GetComponent<KingCloneBehaviour>().SetCloneAlpha(alpha);
        }

        //Switch positions
        int bossIndex = Random.Range(0, positions.Length);
        int cloneIndex = 0;
        yield return new WaitForSeconds(2.2f);
        for (int j = 0; j < positions.Length; ++j)
        {
            if (j == bossIndex)
            {
                gameObject.transform.position = positions[j];
            }
            else
            {
                clones[cloneIndex++].transform.position = positions[j];
            }
        }

        //Set them to opaque again
        time = 0.0f;
        ANIM_DUARTION = 0.6f;
        alpha = 0.0f;
        while (time < ANIM_DUARTION)
        {
            float elapsed = Time.deltaTime;
            alpha = alpha + (elapsed / ANIM_DUARTION);
            SetKingAlpha(alpha);
            foreach (GameObject clone in clones)
            {
                clone.GetComponent<KingCloneBehaviour>().SetCloneAlpha(alpha);
            }
            time += elapsed;
            yield return new WaitForEndOfFrame();
        }
        alpha = 1.0f;
        SetKingAlpha(alpha);
        foreach (GameObject clone in clones)
        {
            clone.GetComponent<KingCloneBehaviour>().SetCloneAlpha(alpha);
        }

        // Set them back to active
        isSwitching = false;
        gameObject.GetComponent<Collider>().enabled = true;
        foreach (GameObject clone in clones)
        {
            clone.GetComponent<KingCloneBehaviour>().SetSwitching(false);
            clone.GetComponent<KingCloneBehaviour>().SetCollider(true);
        }
        switchPosCooldown = Random.Range(10.0f, 15.0f);
        switchPosTimer = 0.0f;
    }

    // Grab
    // Anim --> Reduce Alpha then tp near player

    IEnumerator GrabAnimation()
    {

        isGrabbing = true;
        grabHit = false;
        gameObject.GetComponent<Collider>().enabled = false;
        // Become Invisible
        float time = 0.0f;
        float ANIM_DUARTION = 2.0f;
        float alpha = 1.0f;
        while(time < ANIM_DUARTION)
        {
            float elapsed = Time.deltaTime;
            alpha = alpha - (elapsed / ANIM_DUARTION);
            SetKingAlpha(alpha);
            time += elapsed;
            yield return new WaitForEndOfFrame();
        }
        alpha = 0.0f;
        SetKingAlpha(alpha);

        float restTime = Random.Range(2.0f, 3.3f);
        // Teleport near player
        yield return new WaitForSeconds(restTime);
        CheckEmptySpot();
        RotateToFacePlayer();

        time = 0.0f;
        ANIM_DUARTION = 0.6f;
        while (time < ANIM_DUARTION)
        {
            float elapsed = Time.deltaTime;
            alpha = alpha + (elapsed / ANIM_DUARTION);
            SetKingAlpha(alpha);
            time += elapsed;
            yield return new WaitForEndOfFrame();
        }
        alpha = 1.0f;
        SetKingAlpha(alpha);

        StartCoroutine(LaunchGrab());
    }

    private IEnumerator LaunchGrab()
    {
        kingAnimator.SetBool("grabbing", true);
        yield return new WaitForSeconds(0.3f * grabAnimationTime);
        scytheCollider.enabled = true;
        yield return new WaitForSeconds(0.5f * grabAnimationTime);
        kingAnimator.SetBool("grabbing", false);
        scytheCollider.enabled = false;

        if(!grabHit)
        {
            FinishedGrabbing();
        }

    }

    public void FinishedGrabbing()
    {
        isGrabbing = false;
        gameObject.GetComponent<Collider>().enabled = true;
        grabbingTimer = 0.0f;
        grabbingCooldown = Random.Range(15.0f, 20.0f);
    }

    public void HitGrab()
    {
        grabHit = true;
    }

    private void CheckEmptySpot()
    {
        GameObject grabZone = GameObject.Find("GrabZone");
        Vector3 playerPosition = player.transform.position;

        Vector3 grabZoneSize = grabZone.GetComponent<Renderer>().bounds.size;

        // Place itself behind if enough distance.
        if (playerPosition.x <= grabZone.transform.position.x + grabZoneSize.x / 2.0f && playerPosition.x >= grabZone.transform.position.x - grabZoneSize.x / 2.0f
            && playerPosition.z <= grabZone.transform.position.z + grabZoneSize.z / 2.0f && playerPosition.z >= grabZone.transform.position.z - grabZoneSize.z / 2.0f)
        {
            gameObject.transform.position = playerPosition - player.transform.forward * kingSize.z * 1.0f;
        }
        else
        {
            Vector3 toCentre = new Vector3(grabZone.transform.position.x - player.transform.position.x, 0.0f, grabZone.transform.position.x - player.transform.position.x).normalized;
            gameObject.transform.position = playerPosition + toCentre * kingSize.z * 1.0f;
        }
    }

    private void SetKingAlpha(float alpha)
    {
        var renderers = gameObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer renderer in renderers)
        {
            foreach (Material material in renderer.materials)
            {
                if (material.HasProperty("_Color"))
                {
                    var color = material.color;
                    color.a = alpha;
                    material.color = color;
                }
            }
        }
    }
}
