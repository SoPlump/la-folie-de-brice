﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingCloneHealth : EnemyHealth
{

    // Start is called before the first frame update
    void Start()
    {
        executeThreshold = -1.0f;
    }

    public override void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            gameObject.GetComponent<KingCloneBehaviour>().OnDeath();
        }
        else
        {
            gameObject.GetComponent<Animator>().SetTrigger("damaged");
        }
    }
}
