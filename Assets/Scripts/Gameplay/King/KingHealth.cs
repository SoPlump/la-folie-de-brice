﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingHealth : EnemyHealth
{
    public Animator animator;

    private void Awake()
    {
        deathFlag = gameObject.AddComponent<FlagComponent>();
    }
    public override void TakeDamage(float damage)
    {
        if(this.gameObject.GetComponent<KingBehaviour>().IsGuarding())
        {
            return;
        }
        health -= damage;
        if (health <= 0)
        {
            deathFlag.Trigger();
            animator.SetTrigger("dead");
            Destroy(this.gameObject, 3.5f);
        }
        else
        {
            animator.SetTrigger("damaged");
        }
    }
}
