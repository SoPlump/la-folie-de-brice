﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MentalHealthState
{

    public virtual void Enter()
    {

    }

    public virtual void Leave()
    {

    }

    protected float eldritchsAlpha;

    public float getEldritchAlpha() { return eldritchsAlpha; }

}

[System.Serializable]
public class SaneState : MentalHealthState
{

    public SaneState()
    {
        eldritchsAlpha = 0.0f;
    }
    public override void Enter()
    {
        GameManager.Instance.SetMonsterAggro(false);
    }

    public override void Leave()
    {

    }
}

[System.Serializable]
public class MediumState : MentalHealthState
{

    public MediumState()
    {
        eldritchsAlpha = 0.0f;
    }
    public override void Enter()
    {
        GameManager.Instance.SetMonsterAggro(false);
    }

    public override void Leave()
    {

    }
}

[System.Serializable]
public class BorderlineState : MentalHealthState
{
    public BorderlineState()
    {
        eldritchsAlpha = 0.1f;
    }
    public override void Enter()
    {
        GameManager.Instance.SetMonsterAggro(true);
    }

    public override void Leave()
    {

    }
}

[System.Serializable]
public class InsaneState : MentalHealthState
{

    public InsaneState( )
    {
        eldritchsAlpha = 1.0f;
    }

    public override void Enter()
    {
        GameManager.Instance.SetMonsterAggro(true);
        GameManager.Instance.DisplayHealthBar();
        GameManager.Instance.EnterEldritchWorld();
        Player.Instance.PlayerMentalHealth.sanityLocked = true;
        GameManager.Instance.UpdateAnimatorSpeed(Player.Instance.insanityMovementSpeedModifier);
    }

    public override void Leave()
    {
        GameManager.Instance.DisplaySanityBar();
        Player.Instance.PlayerMentalHealth.Sanity = Player.Instance.PlayerMentalHealth.mediumBorderlineThreshHold;
        GameManager.Instance.ExitEldritchWorld();
        Player.Instance.PlayerMentalHealth.sanityLocked = false;
        GameManager.Instance.UpdateAnimatorSpeed(-1.0f * Player.Instance.insanityMovementSpeedModifier);
    }
}
