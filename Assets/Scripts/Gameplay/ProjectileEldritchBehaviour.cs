﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileEldritchBehaviour : EnemyBehaviour
{
    Animator eldritchAnimator;

    public GameObject projectile;
    public float projectileSpeed;

    //public bool isRange;
    //public float enemySpeed = 3.0f;
    //private float speed;
    //public float minRange = 2.0f;
    //public GameObject ParryHitBox;

    // Start is called before the first frame update
    void Start()
    {
        eldritchAnimator = GetComponentInParent<Animator>();
        player = GameObject.Find("Player").GetComponent<Transform>();
        attackTimer = attackCooldown;
    }

    // Update is called once per frame
    void Update()
    {
        attackTimer += Time.deltaTime;
    }
    

    public override void RotateToFacePlayer()
    {
        if (Vector3.Distance(transform.position, player.position) <= spotRange)
        {
            transform.LookAt(new Vector3(player.position.x, player.position.y + 1.0f, player.position.z));
        }
        else
        {
            eldritchAnimator.SetTrigger("idleBreak");
        }
    }

    public override void Move()
    {
        eldritchAnimator.SetTrigger("startWalk");
    }

    public override void FireAttack()
    {
        if (Vector3.Distance(transform.position, player.position) <= spellRange && attackTimer >= attackCooldown)
        {
            attackTimer = 0.0f;

            GameObject projectileInstance = GameManager.Instance.Pool.GetObject("EldritchBaseProjectile");
            projectileInstance.transform.rotation = transform.rotation;
            projectileInstance.transform.position = transform.position;
            //GameObject projectileInstance = Instantiate(projectile, transform.position, transform.rotation);
            eldritchAnimator.SetTrigger("attack3");
            projectileInstance.GetComponent<Rigidbody>().AddForce(transform.forward * projectileSpeed);
            projectileInstance.GetComponent<BaseProjectile>().damage = spellDamage;
            StartCoroutine(DestroyProjectile(projectileInstance));
        }
    }

    public override void Release()
    {
        this.GetComponent<EnemyHealth>().health = this.GetComponent<EnemyHealth>().maxHealth;
        GameManager.Instance.Pool.ReleaseObject(this.gameObject);
    }

    IEnumerator DestroyProjectile(GameObject projectile)
    {
        yield return new WaitForSeconds(5.0f);
        projectile.transform.position = new Vector3(100, -50, 100);
        projectile.GetComponent<Rigidbody>().velocity = Vector3.zero;
        GameManager.Instance.Pool.ReleaseObject(projectile);
    }
}
