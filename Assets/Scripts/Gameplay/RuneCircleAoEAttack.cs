﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneCircleAoEAttack : MonoBehaviour
{

    public float animationTime;

    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        float elapsed = Time.deltaTime;
        if(timer < animationTime)
        {
            //Debug.Log("ani : " + animationTime);
            float increase = elapsed / animationTime * 10.0f;
            transform.localScale += new Vector3(increase, increase, increase);
            transform.Rotate(0, 0, elapsed / animationTime * 540.0f);
            timer += elapsed;
        }
        else
        {
            Destroy(this.gameObject, 0.5f);
        }
    }
}
