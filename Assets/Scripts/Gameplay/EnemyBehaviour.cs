﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBehaviour : MonoBehaviour
{

    public Transform player; 

    public float attackDamage;
    public float attackRange;

    public float spellDamage;
    public float spellRange;

    public float spotRange;
    public float attackCooldown;

    protected float attackTimer;

    public float movementSpeed;

    // Start is called before the first frame update
    public void Start()
    {
        player = Player.Instance.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public abstract void RotateToFacePlayer();

    abstract public void Move();

    abstract public void FireAttack();

    // This method will take the object out of the pool.
    // Use it to reset the enemy's characteristics
    abstract public void Release();

}
