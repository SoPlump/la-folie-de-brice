﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Player : MonoBehaviour
{

    //SINGLETON
    public static Player Instance;


    void Awake()
    {

        if (Instance == null)
        {

            Instance = this;
            //DontDestroyOnLoad(this.gameObject);

            //Rest of your Awake code

        }
        else
        {
            Destroy(this);
        }
    }

    //CONSTANTS 
    [SerializeField]
    public float maxHitPoints = 100.0f;

    [SerializeField]
    private float maxSanityLossPerHitReceived = 25.0f;

    [SerializeField]
    private float sanityLossPerFailedAttack = 5.0f;

    [SerializeField]
    private float sanityLossPerWrongQTEkey = 5.0f;

    [SerializeField]
    private float sanityGainPerRightQTEkey = 5.0f;

    [SerializeField]
    private float sanityGainPerSuccessfulAttack = 5.0f;

    [SerializeField]
    private float sanityGainPerEnemyKilled = 5.0f;

    [SerializeField]
    public float insanityDamageModifier = 2.0f;

    [SerializeField]
    public float insanityMovementSpeedModifier = 0.25f;

    //ATTRIBUTES

    private float hitPoints; //Character life points , between 0 and maxHitPoints

    public bool isInvincible = false;

    public float HitPoints
    {
        get => hitPoints;
        set
        {
            var previous_hp = hitPoints;
            hitPoints = Mathf.Clamp( value,0,maxHitPoints);
            if(previous_hp != hitPoints)
            {
                OnHitPointsUpdate(hitPoints, previous_hp - hitPoints);
            }
        }
    }

    public float GetHealthPercentage()
    {
        return ((hitPoints / maxHitPoints));
    }

    public MentalHealth PlayerMentalHealth { get; set; } = new MentalHealth();

    //METHODS

    // Start is called before the first frame update
    void Start()
    {
        hitPoints = maxHitPoints;

        //Debug.Log("Current sanity is " + PlayerMentalHealth.Sanity);
        //MentalHealth.LoseSanity(75.0f);
        PlayerMentalHealth.InitializeSanityPotion(80.0f);
        //EnemyManager.Instance.SpawnPersoEldritch(transform.position);

        //StartCoroutine(TakeDmgTest());
    }

    IEnumerator TakeDmgTest()
    {
        yield return new WaitForSeconds(1);
        PlayerMentalHealth.LoseSanity(25.0f);
        yield return new WaitForSeconds(1);
        PlayerMentalHealth.LoseSanity(25.0f);
        yield return new WaitForSeconds(1);
        PlayerMentalHealth.LoseSanity(26.0f);
        yield return new WaitForSeconds(1);


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Die()
    {
        GameManager.Instance.PlayerDied();
    }

    public void FullyHeal()
    {
        HitPoints = maxHitPoints;
    }

    void Heal(float healAmount)
    {
       Assert.IsTrue(healAmount >= 0.0f);
       HitPoints += healAmount;
    }

    public void InitHealthBar()
    {
        GameManager.Instance.hitPoints.Init(0.0f, maxHitPoints);

    }

    //EVENTS

    //This method is called when the character is hit by an attack
    public void OnHitReceived (float damageReceived)
    {
        if (PlayerMentalHealth.IsInsane())
        {
            HitPoints -= damageReceived;
            //Debug.Log("Player is hit for " + damageReceived + " dmg");
        }
        else
        {
            var dmg = Mathf.Min(damageReceived , maxSanityLossPerHitReceived);
            PlayerMentalHealth.Sanity -= dmg;
            //Debug.Log("Player is hit for " + dmg + " sanity");
        }
    }

    //This method is called when the character attacks
    public void OnAttack(bool successful)
    {
        if (successful)
            PlayerMentalHealth.Sanity += sanityGainPerSuccessfulAttack;
        else
            PlayerMentalHealth.Sanity -= sanityLossPerFailedAttack;

    }

    //This method is called when the character loses HP
    void OnHitPointsUpdate(float value, float delta)
    {
        if (delta > 0)
        {
            GameManager.Instance.hitPoints.TakeDamage(delta);
        } else if (delta < 0)
        {
            GameManager.Instance.hitPoints.Heal( -1.0f * delta);
        }

        //GameManager.Instance.UpdateValueToDisplay();
        if (value <= 0)
        {
            Die();
        }
    }

    //This method is called when the character kills an enemy
    void OnEnemyKill()
    {
        PlayerMentalHealth.SanityPotion = +sanityGainPerEnemyKilled;
    }

    //This method is called when the player presses the wrong key in a QTE
    public void OnWrongQTEkey()
    {
        PlayerMentalHealth.LoseSanity(sanityLossPerWrongQTEkey);
    }

    public void onRightQTEkey()
    {
        PlayerMentalHealth.HealSanity(sanityGainPerRightQTEkey);
    }

}
