﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneExplosionProjectile : MonoBehaviour
{
    [System.NonSerialized]
    public float damage;

    [System.NonSerialized]
    public float debuffDuration;

    [System.NonSerialized]
    public float debuffEfficiency;

    private void Start()
    {
        StartCoroutine(ReleaseThis());
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            this.gameObject.transform.parent.gameObject.transform.position = new Vector3(100, -50, 100);
            this.gameObject.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(damage);
            other.gameObject.GetComponent<PlayerAttacks>().MultiplyDamage(debuffEfficiency, debuffDuration);
            GameManager.Instance.Pool.ReleaseObject(this.gameObject.transform.parent.gameObject);
        }
        else if (other.CompareTag("ParryHitBox"))
        {
            this.gameObject.transform.parent.gameObject.transform.position = new Vector3(100, -50, 100);
            this.gameObject.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            GameManager.Instance.Pool.ReleaseObject(this.gameObject.transform.parent.gameObject);
        }
        /*
        else if (other.CompareTag("ArenaWall"))
        {
            this.gameObject.transform.parent.gameObject.transform.position = new Vector3(100, -50, 100);
            this.gameObject.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            GameManager.Instance.Pool.ReleaseObject(this.gameObject.transform.parent.gameObject);
        }
        */
    }

    IEnumerator ReleaseThis()
    {
        yield return new WaitForSeconds(2.0f);
        this.gameObject.transform.parent.gameObject.transform.position = new Vector3(100, -50, 100);
        this.gameObject.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
        GameManager.Instance.Pool.ReleaseObject(this.gameObject.transform.parent.gameObject);
    }
}
