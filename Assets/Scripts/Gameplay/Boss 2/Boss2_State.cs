﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Boss2_State : MonoBehaviour
{

    protected float distanceToPlayer; // Distance (between the player and the boss) that the boss will try to keep
    protected float movementSpeed;

    [System.NonSerialized]
    public GameObject boss;

    [System.NonSerialized]
    public Animator spooderAnimator;

    public void Initialize(float distance, float speed)
    {
        distanceToPlayer = distance;
        movementSpeed = speed;
        boss = GameObject.Find("Boss_Boss02");
        spooderAnimator = gameObject.GetComponent<Animator>();
    }

    public abstract void LaunchAttack();

    public virtual void Move(Vector3 playerPosition)
    {
        //Debug.Log("DistanceToPlayer : " + distanceToPlayer);
        //Debug.Log("d = " + Vector3.Distance(boss.transform.position, playerPosition));
        //Debug.Log("s = " + movementSpeed);
        float distance = Vector3.Distance(boss.transform.position, playerPosition);
        if (distance > distanceToPlayer*1.1)
        {
            spooderAnimator.SetBool("isWalking", true);
            Vector3 moveVector = boss.transform.forward;
            moveVector.y = 0.0f;
            boss.GetComponent<Rigidbody>().AddForce(moveVector * movementSpeed);
        }
        else if (distance < distanceToPlayer*0.9)
        {
            spooderAnimator.SetBool("isWalking", true);
            Vector3 moveVector = boss.transform.forward;
            moveVector.y = 0.0f;
            boss.GetComponent<Rigidbody>().AddForce(moveVector * -movementSpeed);
        }
        else
        {
            spooderAnimator.SetBool("isWalking", false);
        }
        
    }

    public abstract void OnEnter();

    // No Spot Range for this boss (seems kinda useless)
    public virtual void Rotate(Vector3 playerPosition)
    {
        boss.transform.LookAt(new Vector3(playerPosition.x, boss.transform.position.y, playerPosition.z));
    }

    public virtual void SwitchToBaseState()
    {
        boss.GetComponent<Boss2_Behaviour>().SetToBaseState();
    }

    public virtual void Update() { }

    public abstract void HitBoxDown();
}
