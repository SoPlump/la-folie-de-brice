﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2_Behaviour : EnemyBehaviour
{
    // What State is the boss on at each frame
    private Boss2_State actualBossState;

    // Possible Boss States
    private Boss2_BaseState baseState;
    private Boss2_CloneState cloneState;
    private Boss2_GrabState grabState;
    private Boss2_StunnedState stunnedState;
    private InactiveState inactiveState;
    private Boss2_DeathState deathState;

    public float baseDistance;
    public float baseMoveSpeed;

    public float grabDamage;
    public float grabHitDamage;
    public float grabDuration;
    public int triesToGrab;
    public float distanceToGrab;
    public float speedWhenMarching;

    public float distanceWhenCloned;
    public float moveSpeedWhenCloned;
    public float cloneDestructionDamage;
    public float cloneDebuffEfficiency;
    public float cloneDebuffDuration;

    public float stunDuration;

    private float switchTimer;
    private float nextSwitch;

    // Hitboxes
    [SerializeField]
    private GameObject[] hitboxes;


    // Start is called before the first frame update
    void Start()
    {

        player = Player.Instance.transform;

        baseState = this.gameObject.GetComponent<Boss2_BaseState>();
        baseState.Initialize(baseDistance, baseMoveSpeed, spellDamage, attackCooldown);

        grabState = this.gameObject.GetComponent<Boss2_GrabState>();
        grabState.Initialize(distanceToGrab, speedWhenMarching, grabDamage, grabHitDamage, grabDuration, triesToGrab);

        cloneState = this.gameObject.GetComponent<Boss2_CloneState>();
        cloneState.Initialize(distanceWhenCloned, moveSpeedWhenCloned, spellDamage, attackCooldown, cloneDestructionDamage, cloneDebuffDuration, cloneDebuffEfficiency);

        stunnedState = this.gameObject.GetComponent<Boss2_StunnedState>();
        stunnedState.Initialize(0.0f, 0.0f, stunDuration);

        inactiveState = this.gameObject.GetComponent<InactiveState>();
        inactiveState.Initialize(0.0f, 0.0f);

        deathState = gameObject.GetComponent<Boss2_DeathState>();
        deathState.Initialize(0.0f, 0.0f);

        SetToBaseState();

        //StartCoroutine(LaunchClones());
        //StartCoroutine(LaunchGrab());
        //SwitchHitBoxes();

        //StartClonePhase();

        //StartGrabPhase();


        //EnemyManager.Instance.EldritchsSpawned(this.gameObject);
        //EnemyManager.Instance.StartBossFight();
    }

    // Update is called once per frame
    void Update()
    {
        // Here we switch phases : Clone phase is done with DamageTaken()
        // We need to switch between Grab and Base States.

        if (actualBossState == baseState)
        {
            switchTimer += Time.deltaTime;
            if (switchTimer > nextSwitch)
            {
                StartGrabPhase();
            }
        }

    }

    public override void FireAttack()
    {
        actualBossState.LaunchAttack();
    }

    public override void RotateToFacePlayer()
    {
        actualBossState.Rotate(player.position);
    }

    public override void Move()
    {
        actualBossState.Move(player.position);
    }

    public Boss2_State GetActualState()
    {
        return actualBossState;
    }

    public void BossDie()
    {
        actualBossState = deathState;
        deathState.OnEnter();
        
    }

    public void SetToStunned(float newDuration)
    {
        SetActivityOfHitboxes(true);
        stunnedState.SetDuration(newDuration);
        stunnedState.OnEnter();
        actualBossState = stunnedState;
    }

    public void SetToBaseState()
    {
        switchTimer = 0.0f;
        nextSwitch = Random.Range(7, 10);
        actualBossState = baseState;
        SwitchHitBoxes();
        baseState.OnEnter();
    }

    public void StartClonePhase()
    {
        SetActivityOfHitboxes(false);
        hitboxes[2].SetActive(true);
        hitboxes[2].GetComponent<HitboxesHealth>().maxHealth = Random.Range(15, 25);
        hitboxes[2].GetComponent<HitboxesHealth>().health = hitboxes[3].GetComponent<HitboxesHealth>().maxHealth;
        actualBossState = cloneState;
        cloneState.OnEnter();
    }

    public void HitBoxDown()
    {
        actualBossState.HitBoxDown();
        //SetToStunned(5.0f);
        //SwitchHitBoxes();
    }

    public bool AllBoxesDown()
    {
        foreach (GameObject obj in hitboxes)
        {
            if (obj.activeInHierarchy)
            {
                return false;
            }
        }
        return true;
    }

    public void SwitchHitBoxes()
    {
        SetActivityOfHitboxes(false);
        // We first roll which two hitboxes are going to be set active.
        int box1 = Random.Range(0, 2);
        int box2 = (Random.Range(1, 2) + box1) % (hitboxes.Length - 1); // Ensures we get two different numbers and not the back hitbox

        int hp1 = Random.Range(15, 25);
        int hp2 = Random.Range(15, 25);

        hitboxes[box1].GetComponent<HitboxesHealth>().maxHealth = hp1;
        hitboxes[box1].GetComponent<HitboxesHealth>().health = hp1;
        hitboxes[box1].SetActive(true);

        hitboxes[box2].GetComponent<HitboxesHealth>().maxHealth = hp2;
        hitboxes[box2].GetComponent<HitboxesHealth>().health = hp2;
        hitboxes[box2].SetActive(true);
    }

    public void SetActivityOfHitboxes(bool active)
    {
        foreach (GameObject obj in hitboxes)
        {
            obj.SetActive(active);
        }
    }

    public void StartGrabPhase()
    {
        SetActivityOfHitboxes(false);
        grabState.OnEnter();
        actualBossState = grabState;
    }

    public void ContinueGrabPhase()
    {
        SetActivityOfHitboxes(false);
        actualBossState = grabState;
    }

    public void SetInactive(float duration = 3.0f)
    {
        SetActivityOfHitboxes(false);
        hitboxes[2].SetActive(true);
        hitboxes[2].GetComponent<HitboxesHealth>().maxHealth = 15;
        hitboxes[2].GetComponent<HitboxesHealth>().health = 15;
        inactiveState.OnEnter();
        actualBossState = inactiveState; 
    }

    public override void Release()
    {
        // First, kill any clone
        GameObject[] clonesLeft = GameObject.FindGameObjectsWithTag("Clone");
        foreach (GameObject go in clonesLeft)
        {
            EnemyManager.Instance.EldritchKilled(go);
        }
        StartCoroutine(Die());
    }

    //Launxhes clone phases
    public void DamageTaken(float remainingHealth, float damageTaken, float maxHealth)
    {
        if(remainingHealth <= maxHealth * 0.85f && (remainingHealth + damageTaken) > maxHealth * 0.85f)
        {
            StartClonePhase();
        }
        else if (remainingHealth <= maxHealth * 0.5f && (remainingHealth + damageTaken) > maxHealth * 0.5f)
        {
            StartClonePhase();
        }
        else if (remainingHealth <= maxHealth * 0.15f && (remainingHealth + damageTaken) > maxHealth * 0.15f)
        {
            StartClonePhase();
        }
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(3.0f);
        GameManager.Instance.Pool.ReleaseObject(this.gameObject);
    }

    IEnumerator LaunchClones()
    {
        yield return new WaitForSeconds(10.0f);
        StartClonePhase();
    }
    IEnumerator LaunchGrab()
    {
        yield return new WaitForSeconds(10.0f);
        StartGrabPhase();
    }
}
