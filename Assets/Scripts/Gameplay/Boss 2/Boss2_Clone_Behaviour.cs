﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2_Clone_Behaviour : EnemyBehaviour
{
    public float distanceToPlayer;

    public float cloneDestructionDamage;
    public float debuffDuration;
    public float debuffEfficiency;

    [SerializeField]
    private int numberOfProjectiles = 150;

    private Animator animator;
    public override void FireAttack()
    {
        // Pretty much the same as the base state

        if (attackTimer >= attackCooldown)
        {
            Vector3 projectileBasePos = new Vector3(gameObject.transform.position.x, 1.0f, gameObject.transform.position.z);
            //projectileBasePos += gameObject.transform.forward * 5.0f;
            GameObject projectile = GameManager.Instance.Pool.GetObject("Boss02_Clone_Projectile");
            projectile.transform.position = projectileBasePos;
            projectile.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            projectile.GetComponent<Rigidbody>().AddForce(transform.forward * 700);
            projectile.GetComponentInChildren<Boss2_Clone_SpellProjectile>().damage = spellDamage;
            StartCoroutine(projectile.GetComponentInChildren<Boss2_Clone_SpellProjectile>().ReleaseThis());
            attackTimer = 0.0f;
        }
    }

    public override void Move()
    {
        float distance = Vector3.Distance(gameObject.transform.position, player.position);
        if (distance > distanceToPlayer*1.1)
        {
            animator.SetBool("isWalking", true);
            //transform.position += transform.forward * movementSpeed * Time.deltaTime;
            //eldritchAnimator.SetTrigger("startWalk");
            Vector3 moveVector = gameObject.transform.forward;
            moveVector.y = 0.0f;
            gameObject.GetComponent<Rigidbody>().AddForce(moveVector * movementSpeed);
        }
        else if (distance < distanceToPlayer*0.9)
        {
            animator.SetBool("isWalking", true);
            //transform.position += transform.forward * movementSpeed * Time.deltaTime;
            //eldritchAnimator.SetTrigger("startWalk");
            Vector3 moveVector = gameObject.transform.forward;
            moveVector.y = 0.0f;
            gameObject.GetComponent<Rigidbody>().AddForce(moveVector * -movementSpeed);
        }
        else
        {
            animator.SetBool("isWalking", false);
        }
    }

    public override void Release()
    {
        this.GetComponent<EnemyHealth>().health = this.GetComponent<EnemyHealth>().maxHealth;
        GameManager.Instance.Pool.ReleaseObject(this.gameObject);
    }

    public override void RotateToFacePlayer()
    {
        transform.LookAt(new Vector3(player.position.x, transform.position.y, player.position.z));
    }

    public void SetTimer(float timer)
    {
        attackTimer = timer;
    }

    // Start is called before the first frame update
    void Start()
    {
        player = Player.Instance.transform;
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        attackTimer += Time.deltaTime;
    }

    public void OnDeath()
    {
        StartCoroutine(DeathEffect());
    }

    IEnumerator DeathEffect()
    {
        //GameObject explosion;
        movementSpeed = 0;
        // Launch explosion effect
        //explosion = GameManager.Instance.Pool.GetObject("Clone_Explosion");
       // explosion.transform.position = gameObject.transform.position;
       // StartCoroutine(explosion.GetComponent<CloneExplosion>().StopThis());
        // Shoot projectiles
        Transform tr = gameObject.transform;
        for (int i = 0; i < numberOfProjectiles; ++i)
        {
            GameObject projectile = GameManager.Instance.Pool.GetObject("Clone_Explosion_Projectile");
            projectile.transform.position = new Vector3(tr.position.x, 0.7f, tr.position.z);
            projectile.transform.rotation = tr.rotation;
            tr.Rotate(0, 360.0f / (float)numberOfProjectiles, 0);
            projectile.GetComponent<Rigidbody>().AddForce(projectile.transform.forward * 1500);
            projectile.GetComponentInChildren<CloneExplosionProjectile>().damage = cloneDestructionDamage;
            projectile.GetComponentInChildren<CloneExplosionProjectile>().debuffDuration = debuffDuration;
            projectile.GetComponentInChildren<CloneExplosionProjectile>().debuffEfficiency = debuffEfficiency;
        }
        EnemyManager.Instance.CloneDespawned(this.gameObject);
        yield return new WaitForEndOfFrame();
    }
}
