﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxesHealth : EnemyHealth
{

    public override void TakeDamage(float damage)
    {
        health -= damage;
        float realDamage = damage;
        if (health <= 0)
        {
            realDamage += health; // We offset by the negative health in case we did overkill damage
            gameObject.SetActive(false);
            gameObject.GetComponentInParent<Boss2_Behaviour>().HitBoxDown(); // Notify one of the hitboxes was destroyed. We'll remove to add in State to create different behaviours.
        }
        gameObject.GetComponentInParent<BossHealth>().TakeDamage(realDamage);
    }
}
