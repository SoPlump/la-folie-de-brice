﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2_CloneState : Boss2_State

// Clones will be enemies with one health, so they get one shot and we can detect they are getting hit
{

    private float spellDamage;
    private float spellCooldown;

    private float cloneDestructionDamage;
    private float debuffDuration;
    private float debuffEfficiency;

    private float attackTimer;

    [SerializeField]
    private Vector3[] lesPos;

    public void Initialize(float distance, float speed, float spellDamage, float attackCooldown, float cloneDestructionDamage, float debuffDuration, float debuffStrength)
    {
        base.Initialize(distance, speed);
        this.spellDamage = spellDamage;
        spellCooldown = attackCooldown;
        this.cloneDestructionDamage = cloneDestructionDamage;
        this.debuffDuration = debuffDuration;
        debuffEfficiency = debuffStrength;
    }

    private void Update()
    {
        attackTimer += Time.deltaTime;
    }

    public override void LaunchAttack()
    {
        // Pretty much the same as the base state

        if (attackTimer >= spellCooldown)
        {
            Vector3 projectileBasePos = new Vector3(boss.transform.position.x, 1.0f, boss.transform.position.z);
            //projectileBasePos += boss.transform.forward * 5.0f;
            GameObject projectile = GameManager.Instance.Pool.GetObject("Boss02_Projectile");
            projectile.transform.position = projectileBasePos;
            projectile.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            projectile.GetComponent<Rigidbody>().AddForce(transform.forward * 700);
            projectile.GetComponentInChildren<Boss2_SpellProjectile>().damage = spellDamage;
            StartCoroutine(projectile.GetComponentInChildren<Boss2_SpellProjectile>().ReleaseThis());
            attackTimer = 0.0f;
        }
    }

    public override void OnEnter()
    {
        spooderAnimator.SetTrigger("cloneSpawning");
        StartCoroutine(SpawnClones());
    }

    private bool IsDistantEnough(Vector3 posToTest, Vector3[] posToCompare, int numberOfPos)
    {
        for (int i = 0; i < numberOfPos; ++i)
        {
            if(Vector3.Distance(posToTest, posToCompare[i]) < 20)
            {
                return false;
            }
        }
        return true;
    }

    private int NumberOfSpawn()
    {

        float bossHealth = boss.GetComponent<BossHealth>().health;
        float maxBossHealth = boss.GetComponent<BossHealth>().maxHealth;

        float ratio = bossHealth / maxBossHealth;
        if(ratio > 0.75)
        {
            return 1;
        }
        else if (ratio > 0.4)
        {
            return 2;
        }
        return 3;

    }

    public override void HitBoxDown()
    {
        float maxClones = NumberOfSpawn();
        GameObject[] clonesLeft = GameObject.FindGameObjectsWithTag("Clone");
        boss.GetComponent<Boss2_Behaviour>().SetToStunned(5.0f * (float)clonesLeft.Length / maxClones);
        foreach (GameObject go in clonesLeft)
        {
            EnemyManager.Instance.CloneDespawned(go);
        }
    }

    IEnumerator SpawnClones()
    {
        attackTimer = spellCooldown / 2.0f;
        // Spawn the clones (between 1 and 3 depending on boss' health)

        // Get number of clones to spawn
        int cloneToSpawn = NumberOfSpawn();

        // We create an array of Vector3 for the positions
        Vector3[] positions = new Vector3[cloneToSpawn + 1]; // Including the boss itself

        // First we store the boss' position to use it as one of the entities' position
        positions[0] = boss.transform.position;

        float yPos = positions[0].y;
        Vector3 playerPosition = Player.Instance.transform.position;

        GameObject spawnZone = GameObject.Find("CloneSpawn");
        Vector3 sizes = spawnZone.GetComponent<Renderer>().bounds.size;

        for (int i = 1; i < positions.Length; ++i)
        {
            Vector3 pos = new Vector3(positions[0].x, yPos, positions[0].z);
            while (Vector3.Distance(pos, playerPosition) < 20 || !IsDistantEnough(pos, positions, i))
            {
                pos.x = spawnZone.transform.position.x + Random.Range(-sizes.x / 2.0f, sizes.x / 2.0f);
                pos.z = spawnZone.transform.position.z + Random.Range(-sizes.z / 2.0f, sizes.z / 2.0f);
            }
            positions[i] = pos;
        }

        // Simple debug in inspector
        lesPos = positions;

        // Now that we got the positions, we need to spawn our clones, but we also need to place our boss, so first we decide which
        // iteration will simply be a displacement of our boss
        int bossIndex = Random.Range(0, positions.Length);
        yield return new WaitForSeconds(2.2f);
        for (int i = 0; i < positions.Length; ++i)
        {
            if (i == bossIndex)
            {
                boss.transform.position = positions[i];
            }
            else
            {
                GameObject clone = GameManager.Instance.Pool.GetObject("Boss02_Clone");
                clone.transform.position = positions[i];
                clone.GetComponent<Boss2_Clone_Behaviour>().SetTimer(spellCooldown / (positions.Length - i)); // This line could go away, it's simply to have clone's attacks be delayed
                clone.GetComponent<Boss2_Clone_Behaviour>().distanceToPlayer = distanceToPlayer;
                clone.GetComponent<Boss2_Clone_Behaviour>().attackCooldown = spellCooldown;
                clone.GetComponent<Boss2_Clone_Behaviour>().spellDamage = spellDamage;
                clone.GetComponent<Boss2_Clone_Behaviour>().movementSpeed = movementSpeed;
                clone.GetComponent<Boss2_Clone_Behaviour>().cloneDestructionDamage = cloneDestructionDamage;
                clone.GetComponent<Boss2_Clone_Behaviour>().debuffDuration = debuffDuration;
                clone.GetComponent<Boss2_Clone_Behaviour>().debuffEfficiency = debuffEfficiency;
                EnemyManager.Instance.CloneSpawned(clone);
            }
        }
    }
    /*
public void CloneTouched()
{
   // First we release every clone
   GameObject[] clones = GameObject.FindGameObjectsWithTag("Clone");

   foreach (GameObject clone in clones)
   {
       GameManager.Instance.Pool.ReleaseObject(clone);
   }

   // Then the player needs to take damage
   Player.Instance.OnHitReceived()
}
*/
}
