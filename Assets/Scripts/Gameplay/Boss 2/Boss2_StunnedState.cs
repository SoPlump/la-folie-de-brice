﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2_StunnedState : Boss2_State
{

    private float stunDuration;

    public void Initialize(float distance, float speed, float duration)
    {
        base.Initialize(distance, speed);
        stunDuration = duration;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void LaunchAttack()
    {
        // Cannot launch attacks either
        
    }

    // In this state, the boss should do anyrhing, except switch back to base state after some time.
    public override void Move(Vector3 playerPosition)
    {
        // Stays empty to allow player to hit any hitbox
    }

    public override void OnEnter()
    {
        if(stunDuration != 0.0f)
        {
            spooderAnimator.SetBool("stunned", true);
        }
        StartCoroutine(StopBeingStunned());
    }

    public override void Rotate(Vector3 playerPosition)
    {
        // Stays empty to allow player to hit any hitbox
    }

    public void SetDuration(float newDuration)
    {
        stunDuration = newDuration;
    }

    private IEnumerator StopBeingStunned()
    {
        yield return new WaitForSeconds(stunDuration);
        spooderAnimator.SetBool("stunned", false);
        SwitchToBaseState();
    }

    public override void HitBoxDown()
    {

    }
}
