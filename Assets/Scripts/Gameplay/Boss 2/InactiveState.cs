﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InactiveState : Boss2_State
{

    private bool toActive = false;
    public override void HitBoxDown()
    {
        
    }

    public override void Move(Vector3 playerPosition)
    {
        
    }

    public override void Rotate(Vector3 playerPosition)
    {
        
    }

    public override void LaunchAttack()
    {
        
    }

    public override void OnEnter()
    {
        toActive = false;
        StartCoroutine(GoBackToGrabState());
    }

    public void FinishedInactivity()
    {
        toActive = true;
    }

    IEnumerator GoBackToGrabState()
    {
        while(!toActive)
        {
            yield return new WaitForEndOfFrame();
        }
        boss.GetComponent<Boss2_Behaviour>().ContinueGrabPhase();
    }

    public IEnumerator SetActiveInSeconds(float duration = 3.0f)
    {
        yield return new WaitForSeconds(duration);
        toActive = true;
    }
}
