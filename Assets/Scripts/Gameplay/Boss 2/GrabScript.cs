﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabScript : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject.Find("Boss_Boss02").GetComponent<Boss2_GrabState>().grabSuccessful = true;
            gameObject.GetComponent<Collider>().enabled = false;
            PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(GameObject.Find("Boss_Boss02").GetComponent<Boss2_GrabState>().GetGrabDamage());
            StartCoroutine(GrabbedQTE(playerHealth));
        }
    }

    IEnumerator GrabbedQTE(PlayerHealth ph)
    {
        yield return StartCoroutine(ScenarioManager.Instance.WaitForQTE());
        GameObject.Find("Boss_Boss02").GetComponent<InactiveState>().FinishedInactivity();

        if (!GameManager.Instance.QTEisSuccess)
        {
            ph.TakeDamage(GameObject.Find("Boss_Boss02").GetComponent<Boss2_GrabState>().GetGrabHitDamage());
        }
        GameObject.Find("Boss_Boss02").GetComponent<Boss2_GrabState>().grabSucceeded = true;
    }
}
