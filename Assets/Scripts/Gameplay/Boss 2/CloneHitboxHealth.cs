﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneHitboxHealth : EnemyHealth
{
    public override void TakeDamage(float damage)
    {
        gameObject.GetComponentInParent<CloneHealth>().TakeDamage(damage);
    }
}
