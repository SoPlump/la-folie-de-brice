﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneExplosion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator StopThis()
    {
        yield return new WaitForSeconds(0.7f);
        GameManager.Instance.Pool.ReleaseObject(gameObject);
    }
}
