﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2_GrabState : Boss2_State
{

    private float grabDamage;
    private float onHitDamage;
    private float marchDuration; // Duration of the walk animation of the boss before he launches the grab.
    private int numberOfTries; // Number of time the boss can try to grab during the march animation.

    private float attacksDone;

    private GameObject head;
    private float animTime = 0.0f;

    public bool grabSucceeded;
    public bool inAnimation = false;

    public bool grabSuccessful;

    public override void HitBoxDown()
    {

    }

    public void Initialize(float distance, float speed, float baseDamage, float hitDamage, float duration, int tries)
    {
        base.Initialize(distance, speed);
        grabDamage = baseDamage;
        onHitDamage = hitDamage;
        marchDuration = duration;
        numberOfTries = tries;
    }

    public override void LaunchAttack()
    {
        if(attacksDone == numberOfTries || grabSucceeded)
        {
            boss.GetComponent<Boss2_Behaviour>().SetToBaseState();
        }
        if((attacksDone != numberOfTries) && !inAnimation && !grabSuccessful && !grabSucceeded && Vector3.Distance(transform.position, Player.Instance.transform.position) < 5)
        {
            //boss.GetComponent<Boss2_Behaviour>().SetInactive();
            StartCoroutine(LaunchGrabAnimation());
        }

    }

    public override void OnEnter()
    {
        attacksDone = 0;
        grabSucceeded = false;
        grabSuccessful = false;
        head.GetComponent<Collider>().enabled = false;
        if(animTime == 0.0f)
        {
            AnimationClip[] clips = spooderAnimator.runtimeAnimatorController.animationClips;
            foreach (AnimationClip clip in clips)
            {
                switch (clip.name)
                {
                    case "bite":
                        animTime = clip.length;
                        break;
                }
            }
        }
        
    }

    private void Start()
    {
        head = gameObject.transform.Find("base").Find("head").gameObject;
        head.GetComponent<Collider>().enabled = false;
    }

    IEnumerator LaunchGrabAnimation()
    {
        inAnimation = true;
        spooderAnimator.SetTrigger("biting");
        yield return new WaitForSeconds(0.3f*animTime);
        head.GetComponent<Collider>().enabled = true;
        yield return new WaitForSeconds(0.5f*animTime);
        head.GetComponent<Collider>().enabled = false;
        inAnimation = false;
        attacksDone++;
        boss.GetComponent<Boss2_Behaviour>().SetInactive();
        if (!grabSuccessful)
        {
            StartCoroutine(boss.GetComponent<InactiveState>().SetActiveInSeconds(3.0f));
        }
    }

    public float GetGrabDamage()
    {
        return onHitDamage;
    }

    public float GetGrabHitDamage()
    {
        return grabDamage;
    }
}
