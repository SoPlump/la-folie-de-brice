﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2_DeathState : Boss2_State
{

    public void Initialize(float distance, float speed)
    {
        base.Initialize(distance, speed);
    }
    public override void HitBoxDown()
    {
        
    }

    public override void LaunchAttack()
    {
        
    }

    public override void OnEnter()
    {
        foreach(Collider c in boss.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;
        }
    }

    public override void Move(Vector3 playerPosition)
    {
        
    }

    public override void Rotate(Vector3 playerPosition)
    {
        
    }

    public override void SwitchToBaseState()
    {
        
    }
}
