﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2_Clone_SpellProjectile : MonoBehaviour
{
    [System.NonSerialized]
    public float damage;
    private void Start()
    {
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            this.gameObject.transform.parent.gameObject.transform.position = new Vector3(100, -50, 100);
            this.gameObject.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(damage);
            GameManager.Instance.Pool.ReleaseObject(this.gameObject.transform.parent.gameObject);
        }
        else if (other.CompareTag("ParryHitBox"))
        {
            this.gameObject.transform.parent.gameObject.transform.position = new Vector3(100, -50, 100);
            this.gameObject.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            GameManager.Instance.Pool.ReleaseObject(this.gameObject.transform.parent.gameObject);
        }
        else if (other.CompareTag("ArenaWall"))
        {
            this.gameObject.transform.parent.gameObject.transform.position = new Vector3(100, -50, 100);
            this.gameObject.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            GameManager.Instance.Pool.ReleaseObject(this.gameObject.transform.parent.gameObject);
        }
    }

    public IEnumerator ReleaseThis()
    {
        yield return new WaitForSeconds(7.0f);
        GameManager.Instance.Pool.ReleaseObject(this.gameObject.transform.parent.gameObject);
    }
}
