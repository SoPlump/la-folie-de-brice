﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneHealth : EnemyHealth
{
    GameObject explosion;
    public override void TakeDamage(float damage)
    {
        gameObject.GetComponent<Boss2_Clone_Behaviour>().OnDeath();
        //GameObject.Find("Boss_Boss02").GetComponent<Boss2_CloneState>().CloneTouched();
    }
}
