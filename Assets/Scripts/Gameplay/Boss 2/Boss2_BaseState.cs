﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2_BaseState : Boss2_State
{

    private float spellDamage;
    private float spellCooldown;

    private float attackTimer;

    public override void Update()
    {
        attackTimer += Time.deltaTime;
    }

    public void Initialize(float distance, float speed, float spellDamage, float attackCooldown)
    {
        base.Initialize(distance, speed);
        this.spellDamage = spellDamage;
        spellCooldown = attackCooldown;
        spooderAnimator = gameObject.GetComponent<Animator>();
    }

    public override void OnEnter()
    {
        attackTimer = spellCooldown / 2.0f;
    }

    // In this state, the attack will be a simple fireball
    public override void LaunchAttack() 
    {
        if (attackTimer >= spellCooldown)
        {
            Vector3 projectileBasePos = new Vector3(boss.transform.position.x, 1.0f, boss.transform.position.z);
            //projectileBasePos += boss.transform.forward * 1.0f;
            GameObject projectile = GameManager.Instance.Pool.GetObject("Boss02_Projectile");
            projectile.transform.position = projectileBasePos;
            projectile.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
            projectile.GetComponent<Rigidbody>().AddForce(transform.forward * 700);
            projectile.GetComponentInChildren<Boss2_SpellProjectile>().damage = spellDamage;
            StartCoroutine(projectile.GetComponentInChildren<Boss2_SpellProjectile>().ReleaseThis());
            attackTimer = 0.0f;
        }
    }

    public override void HitBoxDown()
    {
        if(boss.GetComponent<Boss2_Behaviour>().AllBoxesDown())
        {
            boss.GetComponent<Boss2_Behaviour>().SwitchHitBoxes();
        }
    }
}
