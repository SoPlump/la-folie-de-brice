﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : EnemyHealth
{

    private bool hesAlreadyDead = false;
    void Start()
    {
        executeThreshold = -1.0f;
    }

    private void Awake()
    {
        deathFlag = gameObject.AddComponent<FlagComponent>();
    }

    public override void TakeDamage(float damage)
    {
        if(hesAlreadyDead)
        {
            return;
        }
        health -= damage;
        if (health <= 0)
        {
            this.GetComponent<BoxCollider>().enabled = false;
            hesAlreadyDead = true;
            gameObject.GetComponent<Animator>().SetBool("stunned", true);

            deathFlag.Trigger();
            gameObject.GetComponent<Boss2_Behaviour>().BossDie();

            GameManager.Instance.BossDefeated();

            Destroy(gameObject, 1.5f);
        }
        else
        {
            gameObject.GetComponent<Animator>().SetTrigger("damaged");
            gameObject.GetComponent<Boss2_Behaviour>().DamageTaken(health, damage, maxHealth);
        }
    }
}
