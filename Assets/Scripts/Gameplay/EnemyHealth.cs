﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnemyHealth : MonoBehaviour
{
    public float maxHealth = 25.0f;
    public float health = 25f;

    public float executeThreshold = 0.3f;

    protected HealthBar healthBar;

    protected bool execEffectDisplayed;

    [System.NonSerialized]
    public FlagComponent deathFlag;

    private void Awake()
    {
        deathFlag = gameObject.AddComponent<FlagComponent>();
    }

    private void Start()
    {
        healthBar = GetComponentInChildren<HealthBar>();
        Init();
    }

    public void Init()
    {
        HideEffect();
        healthBar = GetComponentInChildren<HealthBar>();
        if(null != healthBar)
        {
            healthBar.Init(0.0f,maxHealth);
        }
    }


    public virtual void TakeDamage(float damage)
    {
        health -= damage;
        if(health > 0)healthBar.TakeDamage(damage);
        if (health <= 0)
        {
            EnemyManager.Instance.EldritchKilled(this.gameObject);
            //this.gameObject.GetComponent<EnemyBehaviour>().Release();
            //Destroy(this.gameObject);
        }
        else if (!execEffectDisplayed && IsInExecuteRange())
        {
            DisplayEffect();
        }
        else if (execEffectDisplayed && !IsInExecuteRange())
        {
            HideEffect();
        }
    }

    public bool IsInExecuteRange()
    {
        return (health <= (maxHealth * executeThreshold)); 
    }

    public virtual void Execute()
    {
        TakeDamage(9999);
    }

    protected void DisplayEffect()
    {
        execEffectDisplayed = true;
        GameObject effect = GameManager.Instance.Pool.GetObject("ExecEffect");
        effect.transform.parent = gameObject.transform;
        effect.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        effect.transform.localPosition = Vector3.zero;
    }

    protected void HideEffect()
    {
        execEffectDisplayed = false;
        Transform effect = gameObject.transform.Find("ExecEffect");
        if(effect)
        {
            GameManager.Instance.Pool.ReleaseObject(effect.gameObject);
        }
    }
}
