﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerAttacks : MonoBehaviour
{
    public Camera cam;
    Animator playerAnimator;
    public GameObject player;

    [System.NonSerialized]
    public Weapon cane;
    private AudioSource audioSource;

    public PlayerActions playerActions;

    //public SpellAoE spellAoE;

    //AOE
    public float spellAoeTimer;
    public GameObject aoeZone;
    private float spellAoEAnimationTime = 1.067f; // TODO : it's dirty :(
    public float spellAoECooldown = 10.0f;
    public float aoeSpellDamage = 20.0f;
    public GameObject aoeRuneCircle;
    public GameObject aoeZoneCollider;
    AudioClip aoeClip;

    //ATTACK
    private float attackTimer;
    private float attackAnimationTime;
    AudioClip caneClip;
    //Son cooldown c'est cane.attackCooldown

    //HEAVY ATTACK
    private float heavyAttackTimer;
    private float heavyAttackAnimationTime;

    //PARRY
    public GameObject ParryHitBox;

    //ROLL
    public float rollCooldown;
    public float rollTimer;
    private float rollAnimationTime;

    //FIREBALL
    public GameObject projectile;
    public float projectileSpeed;
    public float spell01Timer;
    private float spell01AnimationTime;
    public float spell01damage;
    public float spell01range;
    public float spell01cooldown;

    public float executeCooldown;
    private float executeTimer;
    private float[] executeAnimationTime = { 0.0f, 0.0f, 0.0f };
    // SPELLS
    private bool isAOE;
    private bool isAttack;
    private bool isFireball;
    private bool isRoll;
    private bool isExecuting;

    public bool isInvincible;


    //public GameObject spell01RuneCircle;

    public GameObject buff;
    public GameObject debuff;

    private float damageCoeff = 1.0f;

    private string[] executeTriggers = { "triggerExecuteMagic", "triggerExecuteMagic2", "triggerExecutePhysic" };

    // Start is called before the first frame update
    void Start()
    {
        playerAnimator = player.GetComponent<Animator>();
        cane = player.GetComponentInChildren<Weapon>();
        playerActions = GetComponentInParent<PlayerActions>();

        ParryHitBox.SetActive(false);

        attackTimer = cane.attackCooldown;
        rollTimer = rollCooldown;
        spellAoeTimer = spellAoECooldown;
        spell01Timer = spell01cooldown;
        executeTimer = executeCooldown;

        isInvincible = false;
        isAOE = false;
        isAttack = false;
        isFireball = false;
        isRoll = false;
        isExecuting = false;

        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.volume = 0.2f;
        caneClip = (AudioClip)Resources.Load("Sounds/woosh");
        aoeClip = (AudioClip)Resources.Load("Sounds/aoe");
    }

    // Update is called once per frame
    void Update()
    {
        if(!AttackReady())
            attackTimer += Time.deltaTime;
        if(!RollReady())
            rollTimer += Time.deltaTime;
        if(!Spell01Ready())
            spell01Timer += Time.deltaTime;
        if(!AoeSpellReady())
            spellAoeTimer += Time.deltaTime;
        if (!ExecuteReady())
            executeTimer += Time.deltaTime;
        if (!HeavyAttackReady())
            heavyAttackTimer += Time.deltaTime;
        playerAnimator.SetFloat("timer", attackTimer);
    }

    public void Stun()
    {
        playerAnimator.SetTrigger("hasBeenStunned");
    }

    public void DoExecute()
    {
        if (ExecuteReady() && !playerAnimator.GetBool("isParrying") && IsPlayerReady())
        {
            isExecuting = true;
            StartCoroutine(Execute());
        }
    }

    public void DoSpell01()
    {
        if (Spell01Ready() && !playerAnimator.GetBool("isParrying") && IsPlayerReady())
        {
            AudioManager.Instance.PlaySpellSound();
            isFireball = true;
            //Debug.Log("Timer : " + spell01Timer);
            spell01Timer = 0.0f;
            playerAnimator.SetTrigger("triggerSpell01");

            StartCoroutine(WaitFor01Animation());
        }
    }

    public void DoAttack()
    {
        if (AttackReady() && !playerAnimator.GetBool("isParrying") && IsPlayerReady())
        {
            AudioManager.Instance.PlayLightAttackSound();
            isAttack = true;
            //Debug.Log("Timer : " + attackTimer);
            attackTimer = 0.0f;
            playerAnimator.SetTrigger("triggerAttack");
            playerAnimator.SetInteger("Attack", Random.Range(1, 7));
            StartCoroutine(AttackCaneHitbox1());
        }
    }

    public void DoHeavyAttack()
    {
        if (HeavyAttackReady() && !playerAnimator.GetBool("isParrying") && IsPlayerReady() && GameManager.Instance.hasUnlockedHeavyAttack)
        {
            AudioManager.Instance.PlayHeavyAttackSound();
            isAttack = true;
            heavyAttackTimer = 0.0f;
            playerAnimator.SetTrigger("triggerHeavyAttack");
            // Ici lancer une coroutine qui active la cane 
            StartCoroutine(HAttackCaneHitbox1());
        }
    }

    public void StartParry()
    {
        playerAnimator.SetBool("isParrying", true);
        ParryHitBox.SetActive(true);
    }

    public void StopParry()
    {
        playerAnimator.SetBool("isParrying", false);
        ParryHitBox.SetActive(false);
    }

    public void TriggerRolling()
    {
        if (RollReady() && IsPlayerReady())
        {
            isRoll = true;
            rollTimer = 0f;
            playerAnimator.SetBool("isParrying", false);
            ParryHitBox.SetActive(false);
            playerAnimator.Play("Forward Roll");
            this.gameObject.GetComponent<Collider>().enabled = false;
            StartCoroutine(ResetTimer());
        }
    }

    public void MultiplyDamage(float coeff, float duration)
    {
        spell01damage *= coeff;
        cane.attackDamage *= coeff;
        aoeZone.GetComponent<SpellAoE>().aoeSpellDamage *= coeff;
        if (coeff > 1.0f)
        {
            StartCoroutine(BuffDamage(coeff, duration));
        }
        else
        {
            StartCoroutine(DebuffDamage(coeff, duration));
        }
    }

    private IEnumerator BuffDamage(float coeff, float buffDuration)
    {
        buff.SetActive(true);
        yield return new WaitForSeconds(buffDuration);
        spell01damage /= coeff;
        cane.attackDamage /= coeff;
        aoeZone.GetComponent<SpellAoE>().aoeSpellDamage /= coeff;
        buff.SetActive(false);
    }

    private IEnumerator DebuffDamage(float coeff, float debuffDuration)
    {

        debuff.SetActive(true);
        yield return new WaitForSeconds(debuffDuration);
        spell01damage /= coeff;
        cane.attackDamage /= coeff;
        aoeZone.GetComponent<SpellAoE>().aoeSpellDamage /= coeff;
        debuff.SetActive(false);
    }


    IEnumerator ResetTimer()
    {
        yield return new WaitForSeconds(rollAnimationTime - 0.5f);
        playerAnimator.SetBool("rollReady", false);
        this.gameObject.GetComponent<Collider>().enabled = true;
        isRoll = false;
    }

    public float GetAttackTimer()
    {
        return attackTimer;
    }

    public float GetHeavyAttackTimer()
    {
        return heavyAttackTimer;
    }

    public float GetRollAnimationDuration()
    {
        return rollAnimationTime;
    }

    public float GetAttackAnimationDuration()
    {
        return attackAnimationTime;
    }

    public float GetHAttackAnimationDuration()
    {
        return heavyAttackAnimationTime;
    }

    public float GetSpellAoEAnimationDuration()
    {
        return spellAoEAnimationTime;
    }

    public float GetSpell01AnimationDuration()
    {
        return spell01AnimationTime;
    }

    public float[] GetExecuteAnimationDuration()
    {
        return executeAnimationTime;
    }

    public void SetRollAnimationDuration(float t)
    {
        rollAnimationTime = t;
    }

    public void SetAttackAnimationDuration(float t)
    {
        attackAnimationTime = t;
    }

    public void SetHAttackAnimationDuration(float t)
    {
        heavyAttackAnimationTime = t;
    }

    public void SetSpellAoEAnimationDuration(float t)
    {
        spellAoEAnimationTime = t;
    }

    public void SetSpell01AnimationDuration(float t)
    {
        spell01AnimationTime = t;
    }

    public void SetExecuteAnimationDuration(float t1, float t2, float t3)
    {
        executeAnimationTime[0] = t1;
        executeAnimationTime[1] = t2;
        executeAnimationTime[2] = t3;
    }

    public void DoAoeSpell()
    {
        if (AoeSpellReady() && !playerAnimator.GetBool("isParrying") && IsPlayerReady())
        {
            AudioManager.Instance.PlayAoeSound();
            isAOE = true;
            //Debug.Log("Timer : " + spellAoeTimer);
            spellAoeTimer = 0.0f;
            playerAnimator.SetTrigger("triggerAoeSpell");

            StartCoroutine(WaitForAOEAnimation());
        }
    }

    IEnumerator Execute()
    {
        GameManager.Instance.SetInvincible(true);

        // Get closest enemy in front of Brice that is in executeRange
        GameObject enemy = GameManager.Instance.FindClosestEldritchInExecuteRange();
        if (enemy == null)
        {
            isExecuting = false;
            yield break;
        }

        int choice = Random.Range(0, 3);

        if(enemy.CompareTag("GuardEnemy"))
        {
            choice = 2;
        }

        if(choice == 0)
        {
            playerAnimator.SetBool(executeTriggers[0], true);
            yield return new WaitForSeconds(executeAnimationTime[0] / 2.0f);

            GameObject explosion = GameManager.Instance.Pool.GetObject("ExecDeath1");
            Vector3 position = enemy.transform.position;
            position.y = player.transform.position.y + 1.0f;
            explosion.transform.position = position;
            enemy.GetComponent<EnemyHealth>().Execute();

            playerAnimator.SetBool(executeTriggers[0], false);

            yield return new WaitForSeconds(executeAnimationTime[0] / 2.0f);
            GameManager.Instance.Pool.ReleaseObject(explosion);
        }
        else if(choice == 1)
        {
            playerAnimator.SetBool(executeTriggers[1], true);
            yield return new WaitForSeconds(executeAnimationTime[1] / 2.0f);

            GameObject blast = GameManager.Instance.Pool.GetObject("ExecDeath2");
            Vector3 position = enemy.transform.position;
            //position.y = player.transform.position.y + 1.0f;
            blast.transform.position = position;
            enemy.GetComponent<EnemyHealth>().Execute();

            playerAnimator.SetBool(executeTriggers[1], false);

            yield return new WaitForSeconds(executeAnimationTime[1] / 2.0f);
            GameManager.Instance.Pool.ReleaseObject(blast);
        }
        else if(choice == 2)
        {
            playerAnimator.SetBool("isGuard", enemy.CompareTag("GuardEnemy"));
            playerAnimator.SetBool(executeTriggers[2], true);
            yield return new WaitForSeconds(executeAnimationTime[2] / 6.0f);
            playerAnimator.SetBool(executeTriggers[2], false);
            yield return new WaitForSeconds(executeAnimationTime[2] / 8.0f);
            enemy.GetComponent<EnemyHealth>().Execute();
        }

        isExecuting = false;
        executeCooldown = executeAnimationTime[choice];
        executeTimer = 0.0f;
        GameManager.Instance.SetInvincible(false);
    }

    IEnumerator WaitForAOEAnimation()
    {
        GameManager.Instance.SetInvincible(true);
        Transform clone = transform;
        Vector3 pos = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);
        GameObject cloneRune = Instantiate(aoeRuneCircle, pos, transform.rotation);
        cloneRune.transform.Rotate(90, 0, 0);
        cloneRune.GetComponent<RuneCircleAoEAttack>().animationTime = spellAoEAnimationTime;
        Destroy(cloneRune, spellAoEAnimationTime + 0.05f);
        
        Transform projectileClone = transform;
        Vector3 projPos = new Vector3(transform.position.x, transform.position.y + 4.0f, transform.position.z);
        projPos += transform.forward * 3.0f;
        GameObject cloneProj = Instantiate(aoeZone, projPos, transform.rotation);
        cloneProj.GetComponent<SpellAoE>().animationTime = spellAoEAnimationTime;
        cloneProj.GetComponent<SpellAoE>().baseX = transform.position.x;
        cloneProj.GetComponent<SpellAoE>().baseZ = transform.position.z;
        
        yield return new WaitForSeconds(spellAoEAnimationTime);
        
        GameObject spellAoeInstance = Instantiate(aoeZoneCollider, transform.position, transform.rotation);
        if (GameManager.Instance.hasUpgradedSpells)
        {
            spellAoeInstance.GetComponent<SpellAoECollider>().aoeSpellDamage = aoeSpellDamage + aoeSpellDamage/2;
        }
        else
        {
            spellAoeInstance.GetComponent<SpellAoECollider>().aoeSpellDamage = aoeSpellDamage;
        }
        spellAoeInstance.GetComponent<Rigidbody>().AddForce(transform.up * 5000);
        Destroy(spellAoeInstance, 3);
        GameManager.Instance.SetInvincible(false);
        isAOE = false;
    }

    IEnumerator WaitFor01Animation()
    {
        yield return new WaitForSeconds(spell01AnimationTime/3.0f);

        Vector3 spawnpoint = transform.position + transform.forward;
        spawnpoint.y += 0.5f;

        Transform clone = transform;
        //clone.Rotate(0, -50, 0);
        GameObject projectileInstance = Instantiate(projectile, spawnpoint, clone.rotation);
        projectileInstance.GetComponent<Rigidbody>().AddForce(clone.forward * projectileSpeed);
        if (GameManager.Instance.hasUpgradedSpells)
        {
            projectileInstance.GetComponent<Spell01Projectile>().damage = spell01damage + spell01damage/2;

        }
        else
        {
            projectileInstance.GetComponent<Spell01Projectile>().damage = spell01damage;
        }
        projectileInstance.GetComponent<Spell01Projectile>().maxRange = spell01range;
        projectileInstance.GetComponent<Spell01Projectile>().startPosition = spawnpoint;
        isFireball = false;
    }

    public bool AttackReady()
    {
        return attackTimer >= cane.attackCooldown;
    }

    public bool ExecuteReady()
    {
        return executeTimer >= executeCooldown;
    }

    public bool HeavyAttackReady()
    {
        return heavyAttackTimer >= cane.heavyAttackCooldown;
    }

    public bool Spell01Ready()
    {
        return spell01Timer >= spell01cooldown;
    }

    public bool RollReady()
    {
        return rollTimer >= rollCooldown;
    }

    public bool AoeSpellReady()
    {
        return spellAoeTimer >= spellAoECooldown;
    }

    public bool IsPlayerReady()
    {
        return !(isAttack || isAOE || isFireball || isRoll || isExecuting);
    }

    IEnumerator AttackCaneHitbox1()
    {
        cane.gameObject.GetComponent<BoxCollider>().enabled = false;
        yield return new WaitForSeconds(0.25f * GetAttackAnimationDuration());
        cane.gameObject.GetComponent<BoxCollider>().enabled = true;

        float timer = 0.0f;
        float timeToWait = 0.75f * GetAttackAnimationDuration();
        bool hitSomebody = false;
        while (!hitSomebody && timer < timeToWait)
        {
            if (cane.eldritcHit)
            {
                hitSomebody = true;
                if (cane.eldritch)
                {
                    EnemyHealth eHealth = cane.eldritch.gameObject.GetComponent<EnemyHealth>();
                    float damageDealt = cane.attackDamage;
                    if (Player.Instance.PlayerMentalHealth.IsInsane())
                    {
                        damageDealt *= Player.Instance.insanityDamageModifier;
                    }
                    eHealth.TakeDamage(damageDealt);
                    //Debug.Log("Health : " + eHealth.health);
                    Player.Instance.OnAttack(true);
                }
            }
            else if (cane.guardHit)
            {
                EnemyHealth eHealth = cane.guard.gameObject.transform.parent.gameObject.GetComponent<EnemyHealth>();
                float damageDealt = cane.attackDamage;
                if (Player.Instance.PlayerMentalHealth.IsInsane())
                {
                    damageDealt *= Player.Instance.insanityDamageModifier;
                }
                eHealth.TakeDamage(damageDealt);
                Player.Instance.OnAttack(true);
                hitSomebody = true;
            }
            else if (cane.guardParryHit)
            {
                cane.guard.gameObject.transform.parent.gameObject.GetComponent<GuardIdleState>().HitWhileParrying();
                Player.Instance.OnAttack(true);
                hitSomebody = true;
            }
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        if(!hitSomebody)
        {
            Player.Instance.OnAttack(false);
        }
        else if(timer < timeToWait)
        {
            yield return new WaitForSeconds(timeToWait - timer);
        }
        cane.HitDone();
        isAttack = false;
    }

    IEnumerator HAttackCaneHitbox1()
    {
        cane.gameObject.GetComponent<BoxCollider>().enabled = false;
        yield return new WaitForSeconds(0.28f * GetHAttackAnimationDuration());
        cane.gameObject.GetComponent<BoxCollider>().enabled = true;

        float timer = 0.0f;
        float timeToWait = 0.62f * GetHAttackAnimationDuration();
        bool hitSomebody = false;
        while (!hitSomebody && timer < timeToWait)
        {
            if (cane.eldritcHit)
            {
                if (cane.eldritch)
                {
                    EnemyHealth eHealth = cane.eldritch.gameObject.GetComponent<EnemyHealth>();
                    float damageDealt = cane.heavyAttackDamage;
                    if (Player.Instance.PlayerMentalHealth.IsInsane())
                    {
                        damageDealt *= Player.Instance.insanityDamageModifier;
                    }
                    eHealth.TakeDamage(damageDealt);
                    Player.Instance.OnAttack(true);
                    hitSomebody = true;
                }
            }
            else if (cane.guardHit)
            {
                EnemyHealth eHealth = cane.guard.gameObject.transform.parent.gameObject.GetComponent<EnemyHealth>();
                float damageDealt = cane.heavyAttackDamage;
                if (Player.Instance.PlayerMentalHealth.IsInsane())
                {
                    damageDealt *= Player.Instance.insanityDamageModifier;
                }
                eHealth.TakeDamage(damageDealt);
                Player.Instance.OnAttack(true);
                hitSomebody = true;
            }
            else if (cane.guardParryHit)
            {
                cane.guard.gameObject.transform.parent.gameObject.GetComponent<GuardIdleState>().GuardBroke();
                Player.Instance.OnAttack(true);
                hitSomebody = true;
            }
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        if(!hitSomebody)
        {
            Player.Instance.OnAttack(false);
        }
        else if (timer < timeToWait)
        {
            yield return new WaitForSeconds(timeToWait - timer);
        }
        cane.HitDone();
        isAttack = false;
    }
}
