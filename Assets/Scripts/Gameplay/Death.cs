﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Death : MonoBehaviour
{
    public GameObject deathScreenOfDoom;

    public void OnStart()
    {
        StartCoroutine(WaitForDeathScreen());
    }

    IEnumerator WaitForDeathScreen()
    {
        if (!GameMode.Instance.gameIsPaused)
        {
            Player.Instance.FullyHeal();
            EnemyManager.Instance.Clear();
            FightManager.Instance.ForceEndOfFight();
            GameManager.Instance.Pool.ClearPool();
            GameManager.Instance.ExitEldritchWorld();
            AudioManager.Instance.PlayMusic(AudioManager.Music.DEATH, 0.0f);
            GameMode.Instance.gameIsPaused = true;
            deathScreenOfDoom.SetActive(true);
            GameManager.Instance.enableInput = false;

            yield return new WaitForSeconds(7.0f);

            GameMode.Instance.RespawnAtLastCheckpoint();

            while (GameMode.Instance.gameIsPaused)
            {
                yield return null;
            }

            deathScreenOfDoom.SetActive(false);
            GameManager.Instance.enableInput = true;
            ScenarioManager.Instance.RestartScenario();
            Player.Instance.PlayerMentalHealth.Initialize();
            Player.Instance.InitHealthBar();
            GameManager.Instance.DisplaySanityBar();


        }
    }
}
