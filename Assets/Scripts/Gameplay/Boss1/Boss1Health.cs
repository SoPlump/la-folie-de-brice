﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1Health : EnemyHealth
{
    private Animator juggernautAnimator;

    private void Awake()
    {
        deathFlag = gameObject.AddComponent<FlagComponent>();
    }

    public override void TakeDamage(float damage)
    {
        health -= damage;
        juggernautAnimator = this.GetComponent<Animator>();
        if (health <= 0)
        {
            this.GetComponent<BoxCollider>().enabled = false;
            GetComponentInParent<Boss1Behavior>().OnDeath();

            deathFlag.Trigger();
            GameManager.Instance.BossDefeated();

            juggernautAnimator.Play("Dead", -1, 0f);

            Destroy(gameObject, 1.5f);
        }
        else
        {
            juggernautAnimator.Play("Get_Hit_R", -1, 0f);
        }
    }
}
