﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleColliderDetector : MonoBehaviour
{
    public float damage;

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(damage);
        }
    }
}
