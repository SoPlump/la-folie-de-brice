﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1Behavior : EnemyBehaviour
{
    public float flameRange;
    private bool isSpittingFlames = false;
    private float spittingTime;
    public float prewarmDelay = 0.2f;
    public float fireDuration = 5.0f;
    public float fireCooldown = 2.0f;
    private float fireTimer;

    private Vector3 bossPos;

    public float wallCooldown;
    public float wallSpeed;
    private float wallTimer;

    private float wallAnimationTime = 1.8f;

    private Vector3 wallPosition;

    private GameObject wallInstance;
    private GameObject wallInstance2;
    private GameObject wallInstance3;
    public GameObject expandingWall;

    private float bumpTimer;
    public float bumpCooldown;
    public float bumpEscapeTime;

    private Vector3 bumpPosition;
    private Vector3 bumpPosition2;
    private Vector3 bumpPosition3;
    private Vector3 bumpPosition4;

    private GameObject bumpInstance;
    private GameObject bumpInstance2;
    private GameObject bumpInstance3;
    private GameObject bumpInstance4;

    private GameObject bumpAreaIndicatorInstance;
    private GameObject bumpAreaIndicatorInstance2;
    private GameObject bumpAreaIndicatorInstance3;
    private GameObject bumpAreaIndicatorInstance4;

    public GameObject bumpCollider;
    public GameObject bumpAreaIndicator;

    private GameObject blastFire;
    private GameObject fireHitBox;

    private Animator juggernautAnimator;

    private bool alive = true;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Transform>();

        juggernautAnimator = this.GetComponent<Animator>();

        blastFire = GameObject.Find("FireBlast");
        fireHitBox = GameObject.Find("FlameHitBox");

        fireHitBox.SetActive(false);
        blastFire.SetActive(false);

        spittingTime = 0.0f;

        fireCooldown += fireDuration + prewarmDelay;
        fireTimer = fireCooldown;
        bumpTimer = bumpCooldown;
        wallTimer = wallCooldown;

        bossPos = new Vector3(-10.19f, -0.85f, -30.08f);
    }

    // Update is called once per frame
    void Update()
    {
        bumpTimer += Time.deltaTime;
        wallTimer += Time.deltaTime;
        fireTimer += Time.deltaTime;
    }

    public override void RotateToFacePlayer()
    {
        Vector3 lTargetDir = player.position - transform.position;
        lTargetDir.y = 0.0f;

        if (Vector3.Distance(bossPos, player.position) <= spotRange && !isSpittingFlames && alive)
        {
            //transform.LookAt(new Vector3(player.position.x, player.position.y, player.position.z));
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lTargetDir), Time.deltaTime * 200.0f);
        }
        else if (Vector3.Distance(bossPos, player.position) <= flameRange && isSpittingFlames && alive)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lTargetDir), Time.deltaTime * 25.0f);
        }
    }

    public void Flamethrow()
    {
        if (Vector3.Distance(bossPos, player.position) <= flameRange && spittingTime < 5.0f && !isSpittingFlames && fireTimer>fireCooldown)
        {
            fireTimer = 0.0f;
            juggernautAnimator.Play("Prepare_Fire", -1, 0f);
            StartCoroutine(WaitForFire());
        }
        if(Vector3.Distance(bossPos, player.position) >= flameRange)
        {
            fireHitBox.SetActive(false);
            blastFire.SetActive(false);
            isSpittingFlames = false;
        }

        if (Vector3.Distance(bossPos, player.position) >= flameRange && !isSpittingFlames)
        {
            wallTimer = wallTimer + Time.deltaTime * 2;
        }
    }

    IEnumerator WaitForFire()
    {
        spittingTime += Time.deltaTime;
        isSpittingFlames = true;

        yield return new WaitForSeconds(prewarmDelay);
        blastFire.SetActive(true);
        fireHitBox.SetActive(true);

        yield return new WaitForSeconds(fireDuration);
        fireHitBox.SetActive(false);
        blastFire.SetActive(false);
        spittingTime = 0.0f;
        isSpittingFlames = false;
    }

    public void SpawnWall()
    {
        if (wallTimer >= wallCooldown)
        {
            fireTimer = fireCooldown / 2;
            fireHitBox.SetActive(false);
            blastFire.SetActive(false);
            isSpittingFlames = false;

            wallTimer = 0.0f;

            StartCoroutine(ExpandWall());
        }
    }

    IEnumerator ExpandWall()
    {
        yield return new WaitForSeconds(0.1f);

        this.GetComponent<BoxCollider>().enabled = false;
        juggernautAnimator.Play("Jump", -1, 0f);

        yield return new WaitForSeconds(wallAnimationTime);
        this.GetComponent<BoxCollider>().enabled = true;

        wallPosition = new Vector3(-10.19f, -0.85f, -30.08f);
        Quaternion rotation = Quaternion.Euler(90, 0, 0);
        Quaternion rotation2 = Quaternion.Euler(90, 30, 0);
        Quaternion rotation3 = Quaternion.Euler(90, -30, 0);
        wallInstance = Instantiate(expandingWall, wallPosition, rotation);
        wallInstance2 = Instantiate(expandingWall, wallPosition, rotation2);
        wallInstance3 = Instantiate(expandingWall, wallPosition, rotation3);

        float elapsed = 0.0f;
        float increase = 0.0f;
        while (wallTimer < wallCooldown - wallAnimationTime)
        {
            elapsed = Time.deltaTime;
            Transform newTransform = wallInstance.transform;
            Transform newTransform2 = wallInstance2.transform;
            Transform newTransform3 = wallInstance3.transform;
            increase = wallSpeed * elapsed;

            newTransform.localScale += new Vector3(increase, increase, 0);
            newTransform2.localScale += new Vector3(increase * 1.6f, increase * 1.6f, 0);
            newTransform3.localScale += new Vector3(increase * 2.2f, increase * 2.2f, 0);

            wallInstance.transform.localScale = newTransform.localScale;
            wallInstance2.transform.localScale = newTransform2.localScale;
            wallInstance3.transform.localScale = newTransform3.localScale;

            yield return new WaitForSeconds(0.001f);
        }

        Destroy(wallInstance, 0);
        Destroy(wallInstance2, 0);
        Destroy(wallInstance3, 0);
    }

    public void DoBumps()
    {
        if (bumpTimer >= bumpCooldown)
        {
            bumpTimer = 0.0f;

            bumpPosition = new Vector3(player.position.x, player.position.y, player.position.z);
            bumpAreaIndicatorInstance = Instantiate(bumpAreaIndicator, bumpPosition, player.rotation);

            bumpPosition2 = new Vector3(Random.Range(transform.position.x - 22.0f, transform.position.x + 22.0f), player.position.y, Random.Range(transform.position.z - 15.0f, transform.position.z + 15.0f));
            bumpAreaIndicatorInstance2 = Instantiate(bumpAreaIndicator, bumpPosition2, player.rotation);                                                                                               
                                                                                                                                                                                                       
            bumpPosition3 = new Vector3(Random.Range(transform.position.x - 22.0f, transform.position.x + 22.0f), player.position.y, Random.Range(transform.position.z - 15.0f, transform.position.z + 15.0f));
            bumpAreaIndicatorInstance3 = Instantiate(bumpAreaIndicator, bumpPosition3, player.rotation);                                                                                               
                                                                                                                                                                                                       
            bumpPosition4 = new Vector3(Random.Range(transform.position.x - 22.0f, transform.position.x + 22.0f), player.position.y, Random.Range(transform.position.z - 15.0f, transform.position.z + 15.0f));
            bumpAreaIndicatorInstance4 = Instantiate(bumpAreaIndicator, bumpPosition4, player.rotation);

            StartCoroutine(WaitForBumpToOccur());
        }
    }

    IEnumerator WaitForBumpToOccur()
    {
        float timer = 0.0f;
        float elapsed;
        float increase;
        while (timer < bumpEscapeTime)
        {
            elapsed = Time.deltaTime;
            Transform newTransform = bumpAreaIndicatorInstance.transform;
            Transform newTransform2 = bumpAreaIndicatorInstance2.transform;
            Transform newTransform3 = bumpAreaIndicatorInstance3.transform;
            Transform newTransform4 = bumpAreaIndicatorInstance4.transform;
            increase = 3.0f * elapsed / bumpEscapeTime;
            newTransform.localScale += new Vector3(0, increase, 0);
            newTransform2.localScale += new Vector3(0, increase, 0);
            newTransform3.localScale += new Vector3(0, increase, 0);
            newTransform4.localScale += new Vector3(0, increase, 0);
            bumpAreaIndicatorInstance.transform.localScale = newTransform.localScale;
            bumpAreaIndicatorInstance2.transform.localScale = newTransform2.localScale;
            bumpAreaIndicatorInstance3.transform.localScale = newTransform3.localScale;
            bumpAreaIndicatorInstance4.transform.localScale = newTransform4.localScale;
            timer += elapsed;
            yield return new WaitForSeconds(0.001f);
        }
        Destroy(bumpAreaIndicatorInstance, 1);
        Destroy(bumpAreaIndicatorInstance2, 1);
        Destroy(bumpAreaIndicatorInstance3, 1);
        Destroy(bumpAreaIndicatorInstance4, 1);

        bumpInstance = Instantiate(bumpCollider, bumpPosition, player.rotation);
        bumpInstance2 = Instantiate(bumpCollider, bumpPosition, player.rotation);
        bumpInstance3 = Instantiate(bumpCollider, bumpPosition, player.rotation);
        bumpInstance4 = Instantiate(bumpCollider, bumpPosition, player.rotation);

        bumpInstance.GetComponent<SphereCollider>().isTrigger = true;
        bumpInstance.GetComponent<Rigidbody>().AddForce(transform.up * 5000);
        bumpInstance2.GetComponent<SphereCollider>().isTrigger = true;
        bumpInstance2.GetComponent<Rigidbody>().AddForce(transform.up * 5000);
        bumpInstance3.GetComponent<SphereCollider>().isTrigger = true;
        bumpInstance3.GetComponent<Rigidbody>().AddForce(transform.up * 5000);
        bumpInstance4.GetComponent<SphereCollider>().isTrigger = true;
        bumpInstance4.GetComponent<Rigidbody>().AddForce(transform.up * 5000);
        Destroy(bumpInstance, 1);
        Destroy(bumpInstance2, 1);
        Destroy(bumpInstance3, 1);
        Destroy(bumpInstance4, 1);
    }

    public override void Move()
    {
        //Doesn't move
    }

    public override void FireAttack()
    {
        if (alive)
        {
            Flamethrow();
            DoBumps();
            SpawnWall();
        }
    }

    public override void Release()
    {
        throw new System.NotImplementedException();
    }

    public void OnDeath()
    {
        alive = false;
        StopAllCoroutines();
        fireHitBox.SetActive(false);
        blastFire.SetActive(false);
        spittingTime = 0.0f;
        isSpittingFlames = false;

        if(wallInstance)
            Destroy(wallInstance, 0);
        if(wallInstance2)
            Destroy(wallInstance2, 0);
        if(wallInstance3)
            Destroy(wallInstance3, 0);

        if (bumpInstance)
            Destroy(bumpInstance, 0);
        if (bumpInstance2)
            Destroy(bumpInstance2, 0);
        if (bumpInstance3)
            Destroy(bumpInstance3, 0);
        if (bumpInstance4)
            Destroy(bumpInstance4, 0);

        if (bumpAreaIndicatorInstance)
            Destroy(bumpAreaIndicatorInstance, 0);
        if (bumpAreaIndicatorInstance2)
            Destroy(bumpAreaIndicatorInstance2, 0);
        if (bumpAreaIndicatorInstance3)
            Destroy(bumpAreaIndicatorInstance3, 0);
        if (bumpAreaIndicatorInstance4)
            Destroy(bumpAreaIndicatorInstance4, 0);
    }
}
