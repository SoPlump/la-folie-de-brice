﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMeleeEldritchBehaviour : EnemyBehaviour
{
    Animator eldritchAnimator;
    public float minRange;
    private float attackAnimationTime;

    bool isAttacking = false;

    void Start()
    {
        eldritchAnimator = GetComponentInParent<Animator>();
        player = GameObject.Find("Player").GetComponent<Transform>();
        attackTimer = attackCooldown;

        AnimationClip[] clips = eldritchAnimator.runtimeAnimatorController.animationClips;
        foreach(AnimationClip clip in clips)
        {
            switch(clip.name)
            {
                case "Stab Attack":
                    attackAnimationTime = clip.length;
                    break;
            }
        }

    }

    void Update()
    {
        attackTimer += Time.deltaTime;
    }

    public float GetAttackAnimationDuration()
    {
        return attackAnimationTime;
    }

    public void SetAttackAnimationDuration(float t)
    {
        attackAnimationTime = t;
    }

    public override void FireAttack()
    {

        if (Vector3.Distance(transform.position, player.position) <= attackRange && attackTimer >= attackCooldown)
        {
            attackTimer = 0.0f;
            eldritchAnimator.SetTrigger("attack1");
            StartCoroutine(WaitForAttackAnimation());
        }
    }
    public override void RotateToFacePlayer()
    {
        if (Vector3.Distance(transform.position, player.position) <= spotRange)
        {
            transform.LookAt(new Vector3(player.position.x, /*player.position.y + 1.0f*/ transform.position.y, player.position.z));
        }
        else
        {
            eldritchAnimator.SetTrigger("idleBreak");
        }
    }

    public override void Move()
    {
        if (!isAttacking && Vector3.Distance(transform.position, player.position) <= spotRange && Vector3.Distance(transform.position, player.position) >= minRange)
        {
            //transform.position += transform.forward * movementSpeed * Time.deltaTime;
            eldritchAnimator.SetTrigger("startWalk");
            Vector3 moveVector = transform.forward;
            moveVector.y = 0.0f;
            //this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            //this.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            GetComponent<Rigidbody>().AddForce(moveVector * movementSpeed);
        }
        else if(isAttacking)
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }

    // TODO : need une variable pour indiquer
    public override void Release()
    {
        this.GetComponent<EnemyHealth>().health = this.GetComponent<EnemyHealth>().maxHealth;
        GameManager.Instance.Pool.ReleaseObject(this.gameObject);
    }

    IEnumerator WaitForAttackAnimation()
    {
        isAttacking = true;
        yield return new WaitForSeconds(0.4f * attackAnimationTime);
        float angle = player.transform.eulerAngles.y - transform.eulerAngles.y;
        //print(angle);
        if (GameManager.Instance.IsParrying() && Mathf.Abs(angle) > 140 && Mathf.Abs(angle) < 220)
        {
            Debug.Log("Successfully parried melee");
        }
        else if (player.GetComponent<Collider>().enabled == false)
        {
            Debug.Log("Successfully avoided melee");
        }
        else
        {
            //Debug.Log("Hit melee");
            PlayerHealth playerHealth = player.gameObject.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(attackDamage);
        }
        isAttacking = false;
    }
}
