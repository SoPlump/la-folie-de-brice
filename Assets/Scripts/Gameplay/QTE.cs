﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class QTE : MonoBehaviour
{
    private int remainingTries;
    [SerializeField]
    private int maxTries = 3;

    public Image keyToPressImage;

    private bool hasFailedOne = false;

    public enum Keys
    {
        Q,
        D,
        Z,
        S,
        R,
        SPACE,
        A,
        E
    };

    public List<Sprite> images = new List<Sprite>();

    [SerializeField]
    private float timeToPressKey = 1.2f;
    private float timeElapsed = 0f;
    [SerializeField]
    private float timeBetween = 2f;
    private bool waitingForKey = false;
    [SerializeField]
    private int nbKeysToPressed = 4;

    private Keys keyToPress;

    private bool isKeyboard;

    public float failDisplayTime;
    public float shakeRange = 5.0f;


    public int RemainingTries
    {
        get => remainingTries;
        set
        {
            remainingTries = Mathf.Clamp(value, 0, maxTries);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        RemainingTries = maxTries;
        GenerateBind();
    }

    public void Initialize(Image image, bool isKeyboard)
    {
        keyToPressImage = image;
        this.isKeyboard = isKeyboard;
    }

    // Timer
    void Update()
    {
        if (waitingForKey)
        {
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= timeToPressKey)
            {
                --nbKeysToPressed;
                Fail();
            }
        }
    }

    /* Action */

    public void CheckKeyPressed(Keys pressedKey)
    {
        if (waitingForKey == true)
        {
            waitingForKey = false;
            --nbKeysToPressed;
            if (pressedKey == keyToPress)
            {
                GoodKeyPressed();
            }
            else
            {
                Fail();
            }
        }
    }

    /* Generation */

    void GenerateBind()
    {
        keyToPress = (Keys)UnityEngine.Random.Range(0, (Enum.GetNames(typeof(Keys)).Length));
        GenerateVisuals();

        waitingForKey = true;
    }

    void GenerateVisuals()
    {
        if (isKeyboard)
            keyToPressImage.sprite = images[(int)keyToPress + 8];
        else
            keyToPressImage.sprite = images[(int)keyToPress];

        var tempColor = keyToPressImage.color;
        tempColor.a = 1.0f;
        tempColor = new Color32(255, 255, 255, 255);
        keyToPressImage.color = tempColor;
    }

    IEnumerator WaitToGenerate()
    {
        if (hasFailedOne)
        {
            StartCoroutine(WaitForVibration());
            yield return new WaitForSeconds(failDisplayTime);
            hasFailedOne = false;
        }
        else
        {
            yield return new WaitForSeconds(failDisplayTime);
        }
        keyToPressImage.sprite = null;
        var tempColor = keyToPressImage.color;
        tempColor.a = 0.0f;
        keyToPressImage.color = tempColor;
        yield return new WaitForSeconds(timeBetween);
        GenerateBind();
        timeElapsed = 0.0f;
    }

    IEnumerator WaitForVibration()
    {
        float elapsed = 0.0f;
        Quaternion originalRotation = keyToPressImage.GetComponent<Transform>().rotation;

        while (elapsed < failDisplayTime)
        {
            elapsed += Time.deltaTime;
            float z = (UnityEngine.Random.value * shakeRange - (shakeRange / 2))/2;
            keyToPressImage.GetComponent<Transform>().eulerAngles = new Vector3(originalRotation.x, originalRotation.y, originalRotation.z + z);
            yield return null;
        }

        keyToPressImage.GetComponent<Transform>().rotation = originalRotation;
    }

    void Fail()
    {
        keyToPressImage.color = new Color32(255, 0, 0, 255);
        hasFailedOne = true;
        Player.Instance.OnWrongQTEkey();
        waitingForKey = false;
        if (--remainingTries == 0)
        {
            QteOver(false);
        }
        else if (nbKeysToPressed == 0)
        {
            QteOver(true);
        }
        else
        {
            StartCoroutine(WaitToGenerate());
        }
    }

    void GoodKeyPressed()
    {
        keyToPressImage.color = new Color32(0, 255, 0, 255);
        Player.Instance.onRightQTEkey();
        if (nbKeysToPressed == 0)
        {
            QteOver(true);
        }
        else
        {
            StartCoroutine(WaitToGenerate());
        }
    }

    void QteOver(bool isSuccess)
    {
        StartCoroutine(WaitForWrongKeyAnimation(isSuccess));
    }

    IEnumerator WaitForWrongKeyAnimation(bool isSuccess)
    {
        if (!isSuccess)
        {
            StartCoroutine(WaitForVibration());
            yield return new WaitForSeconds(failDisplayTime);
            hasFailedOne = false;
        }
        else
        {
            yield return new WaitForSeconds(failDisplayTime);
        }
        
        Debug.Log("RIP QTE. results = " + isSuccess.ToString());
        GameManager.Instance.StopQTE(isSuccess);
    }
}
