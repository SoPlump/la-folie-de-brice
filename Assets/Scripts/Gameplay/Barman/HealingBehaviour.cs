﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingBehaviour : MonoBehaviour
{
    public void HealBrice()
    {
        AudioManager.Instance.PlayHealingSound();

        Player.Instance.FullyHeal();
    }
}
