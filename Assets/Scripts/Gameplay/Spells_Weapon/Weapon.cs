﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public float attackDamage;
    public float attackRange;
    public float attackCooldown;

    public float heavyAttackDamage;
    public float heavyAttackCooldown;
    public bool eldritcHit = false;
    public Collider eldritch;

    public bool guardHit = false;
    public Collider guard;
    public bool guardParryHit = false;

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Eldritch") || other.CompareTag("EnemyHitBox") || (other.CompareTag("EldritchBoss") && GameObject.Find("Boss_Boss02") != null))
        {
            eldritch = other;
            eldritcHit = true;
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        else if(other.CompareTag("GuardParry"))
        {
            guard = other;
            guardHit = false;
            guardParryHit = true;
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        else if (other.CompareTag("Guard"))
        {
            guard = other;
            guardParryHit = false;
            guardHit = true;
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }

    }

    public void HitDone()
    {
        eldritcHit = false;
        guardHit = false;
        guardParryHit = false;
        eldritch = null;
        guard = null;
    }
}
