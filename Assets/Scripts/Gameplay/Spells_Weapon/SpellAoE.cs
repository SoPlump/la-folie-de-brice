﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellAoE : MonoBehaviour
{
    public float aoeSpellDamage = 20.0f;

    public float animationTime;

    public float baseX;
    public float baseZ;

    private float timer;
    
    private void Start()
    {
        //this.gameObject.GetComponent<Collider>().enabled = false;
        animationTime = animationTime * 0.8f;
    }
    void Update()
    {
        
        float elapsed = Time.deltaTime;
        if (timer < animationTime)
        {
            //Debug.Log("ani : " + animationTime);
            //float increase = elapsed / animationTime * 10.0f;
            //transform.localScale += new Vector3(increase, increase, increase);
            //transform.Rotate(0, 0, elapsed / animationTime * 540.0f);
            float coeff = elapsed / animationTime;
            transform.Rotate(0, coeff * 720.0f, 0);
            float forwardDiff = transform.position.x - baseX - coeff * 3.0f;
            float yDiff = -coeff * 4.0f;
            Vector3 newPos = new Vector3(baseX + forwardDiff*transform.forward.x, transform.position.y + yDiff, baseZ + forwardDiff * transform.forward.z);
            transform.position = newPos;
            timer += elapsed;
        }
        else
        {
            transform.position = new Vector3(baseX, 0.0f, baseZ);
            transform.localScale += new Vector3(7.5f, 7.5f, 7.5f);
           // this.gameObject.GetComponent<Collider>().enabled = true;
            Destroy(this.gameObject, 1.0f);
        }
        
    }
}
