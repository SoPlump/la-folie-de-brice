﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell01Projectile : MonoBehaviour
{
    public float damage;
    public float maxRange = 10000.0f;
    public Vector3 startPosition;

    private void Update()
    {
        if(Vector3.Distance(transform.position, startPosition) > maxRange)
        {
            Destroy(this.gameObject);
        }
    }

    public void OnTriggerEnter(Collider other)
    { 
        if (other.CompareTag("Eldritch") || other.CompareTag("EnemyHitBox") || (other.CompareTag("EldritchBoss") && GameObject.Find("Boss_Boss02")==null) || other.CompareTag("Clone"))
        {
            EnemyHealth eHealth = other.gameObject.GetComponent<EnemyHealth>();
            eHealth.TakeDamage(damage);
            Destroy(this.gameObject);
        }
        else if (other.CompareTag("ArenaWall") || other.CompareTag("Building"))
        {
            Destroy(this.gameObject);
        }
    }
}