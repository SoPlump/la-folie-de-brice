﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellAoECollider : MonoBehaviour
{
    public float aoeSpellDamage;
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Eldritch") || other.CompareTag("EnemyHitBox") || (other.CompareTag("EldritchBoss") && GameObject.Find("Boss_Boss02") == null))
        {
            EnemyHealth eHealth = other.gameObject.GetComponent<EnemyHealth>();
            eHealth.TakeDamage(aoeSpellDamage);
            //Debug.Log("Health : " + eHealth.health);
        }
    }
}
