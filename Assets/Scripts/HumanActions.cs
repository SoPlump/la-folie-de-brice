﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HumanActions : MonoBehaviour
{
    [Header("Human attributes")]
    public float moveSpeed = 10;
    public float walkSpeed = 12f;
    public float runSpeed = 6f;
    public float movementAcceleration = 90.0f;
    public float rotateSpeed = 70;

    [Header("Animation")]
    public Animator anim;

    public virtual void Move(Vector2 direction)
    {
        direction *= 6.0f;

        anim.SetFloat("verticalSpeed", direction.y);
        anim.SetFloat("horizontalSpeed", direction.x);
    }

    public virtual void Look(Vector2 rotate)
    {
        // TODO: ca changera surement d'emplacement pour aller dans guardActions
        if (rotate.sqrMagnitude < 0.01)
            return;

        var scaledRotateSpeed = rotateSpeed * Time.deltaTime;
        transform.rotation *= Quaternion.AngleAxis(rotate.y * scaledRotateSpeed, Vector3.right);
    }
}
