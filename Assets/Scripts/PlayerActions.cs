﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class PlayerActions : HumanActions
{

    [Header("Camera")]
    public GameObject followTransform;
    public float maxVertCam = 40;
    public float minVertCam = 15;

    [Header("Interaction")]
    public float interactionDistance = 2.0f;
    private Outline currentOutline;
    int layerMask = 1 << 8;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        CheckInteractableObjects();
    }


    public override void Move(Vector2 direction)
    {
        base.Move(direction);

        if (direction.sqrMagnitude < 0.01)
            return;

        transform.rotation = Quaternion.Euler(0, followTransform.transform.rotation.eulerAngles.y, 0);
        followTransform.transform.localEulerAngles = new Vector3(followTransform.transform.localEulerAngles.x, 0, 0);

    }

    public override void Look(Vector2 rotate)
    {
        if (rotate.sqrMagnitude < 0.01)
            return;

        var scaledRotateSpeed = rotateSpeed * Time.deltaTime;
        followTransform.transform.rotation *= Quaternion.AngleAxis(rotate.x * scaledRotateSpeed, Vector3.up);
        followTransform.transform.rotation *= Quaternion.AngleAxis(rotate.y * scaledRotateSpeed, Vector3.right);

        var angles = followTransform.transform.localEulerAngles;
        angles.z = 0;

        var angle = followTransform.transform.localEulerAngles.x;

        // Clamp the Up/Down rotation
        if (angle < minVertCam)
        {
            angles.x = minVertCam;
        }
        else if (angle > maxVertCam)
        {
            angles.x = maxVertCam;
        }

        followTransform.transform.localEulerAngles = angles;
    }

    void CheckInteractableObjects()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out var hitInfo,
                        interactionDistance, layerMask))
        {
            if (currentOutline == null)
            {
                currentOutline = hitInfo.transform.gameObject.AddComponent<Outline>();

                currentOutline.OutlineMode = Outline.Mode.OutlineAll;
                currentOutline.OutlineColor = Color.yellow;
                currentOutline.OutlineWidth = 5f;
            }
            else if (currentOutline.gameObject != hitInfo.transform.gameObject)
            {
                Destroy(currentOutline);

                currentOutline = hitInfo.transform.gameObject.AddComponent<Outline>();

                currentOutline.OutlineMode = Outline.Mode.OutlineAll;
                currentOutline.OutlineColor = Color.yellow;
                currentOutline.OutlineWidth = 5f;
            }
        }
        else
        {
            if (currentOutline)
            {
                Destroy(currentOutline);
            }
        }
    }

    public void Interaction()
    {
        if ((GameManager.Instance.enableInput)&&(!Player.Instance.PlayerMentalHealth.IsInsane()))
        {
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out var hitInfo,
                        interactionDistance, layerMask))
            {
                InteractableComponent interComp;
                if (hitInfo.transform.gameObject.TryGetComponent<InteractableComponent>(out interComp))
                {
                    interComp.Interact();
                }
                else if (hitInfo.transform.parent.gameObject.TryGetComponent<InteractableComponent>(out interComp))
                {
                    interComp.Interact();
                }
                else
                {
                    Debug.LogError("No interactable component to interact with");
                }
            }
        }
    }
}
