﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Gamemode is common to every scene, even to the intro, outro scene
 * which is not for GameManager
 */
public class GameMode : MonoBehaviour
{
    public static GameMode Instance;

    public Text infoText;
    public Image infoBackground;
    
    public bool gameIsPaused = false;

    // Save
    [System.NonSerialized]
    public bool requestedGameLoad = false;
    [System.NonSerialized]
    public bool gameHasLoaded = false;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            enabled = false;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Load game

    public void StartLoadingGame()
    {
        var temp = StartCoroutine(LoadGame());

        UpdateInfos(true, "Loading");
    }

    private void UpdateInfos(bool show, string text = "")
    {
        infoText.GetComponent<Text>().text = text;
        infoBackground.gameObject.SetActive(show);
    }

    public void RespawnAtLastCheckpoint()
    {
        AudioManager.Instance.FadeOutMusic(2.0f);
        Time.timeScale = 0.0f;

        var temp = StartCoroutine(LoadGame());

        UpdateInfos(true, "Loading");
    }

    private IEnumerator LoadGame()
    {
        gameHasLoaded = false;
        requestedGameLoad = true;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
        Save save = (Save)bf.Deserialize(file);
        file.Close();

        AsyncOperation asyncLoadLevel = SceneManager.LoadSceneAsync(save.scene, LoadSceneMode.Single);
        while (!asyncLoadLevel.isDone)
        {
            yield return null;
        }

        Player.Instance.HitPoints = save.hitPoints;
        Player.Instance.PlayerMentalHealth = save.mentalHealth;
        Player.Instance.gameObject.transform.position = (Vector3)save.playerPosition;
        GameManager.Instance.lastPositionInMap = save.lastPositionOfPlayer.Deserialize();
        ScenarioManager.Instance.currentShortcut = save.scenarioShortcut;

        // Skills
        GameManager.Instance.hasUnlockedHeavyAttack = save.hasUnlockedHeavyAttack;
        GameManager.Instance.hasUpgradedPotion = save.hasUpgradedPotion;
        GameManager.Instance.hasUnlockedRoll = save.hasUnlockedRoll;
        GameManager.Instance.hasUnlockedLifesteal = save.hasUnlockedLifesteal;
        GameManager.Instance.hasUnlockedDodge = save.hasUnlockedDodge;
        GameManager.Instance.hasUpgradedSpells = save.hasUpgradedSpells;

        Time.timeScale = 1.0f;
        GameManager.Instance.enableInput = true;
        gameHasLoaded = true;
        requestedGameLoad = false;

        gameIsPaused = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        UpdateInfos(false);
    }

    // Save game

    public void SaveGame()
    {
        UpdateInfos(true, "Saving");
        Save save = CreateSaveGameObject();

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, save);
        file.Close();

        Debug.Log("Game saved");
        StartCoroutine(PrintGameSaved());
    }

    private IEnumerator PrintGameSaved()
    {
        UpdateInfos(true, "Game Saved");
        yield return new WaitForSeconds(2.0f);
        UpdateInfos(false);
    }

    private Save CreateSaveGameObject()
    {
        Save save = new Save();

        save.mentalHealth = Player.Instance.PlayerMentalHealth;
        save.hitPoints = Player.Instance.HitPoints;
        save.playerPosition = (SerializableVector3)Player.Instance.gameObject.transform.position;
        save.lastPositionOfPlayer = new SerializableDictionary(GameManager.Instance.lastPositionInMap);
        save.scenarioShortcut = ScenarioManager.Instance.currentShortcut;
        save.scene = SceneManager.GetActiveScene().buildIndex;

        // Skills
        save.hasUnlockedHeavyAttack = GameManager.Instance.hasUnlockedHeavyAttack;
        save.hasUpgradedPotion = GameManager.Instance.hasUpgradedPotion;
        save.hasUnlockedRoll = GameManager.Instance.hasUnlockedRoll;
        save.hasUnlockedLifesteal = GameManager.Instance.hasUnlockedLifesteal;
        save.hasUnlockedDodge = GameManager.Instance.hasUnlockedDodge;
        save.hasUpgradedSpells = GameManager.Instance.hasUpgradedSpells;

        return save;
    }

    public IEnumerator WaitForSceneSwitchFromActualScene(int idScene)
    {
        while (SceneManager.GetActiveScene().buildIndex == idScene)
        {
            yield return null;
        }
    }
}
