﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    //DEFINITIONS

    public Image sanityPotion;

    //SINGLETON
    public static GameManager Instance;

    // Camera
    Camera camera;
    CinemachineVirtualCamera cmVcam1;
    CinemachineVirtualCamera cmVcam2;

    // Pause menu

    public GameObject PauseMenuUI;

    //Player
    private PlayerInput playerInput;
    private GameObject player;
    private Transform playerTransform;
    public GameObject ParryHitBox;
    private GameObject executeZone;
    public GameObject healthbars;
    [System.NonSerialized]
    public HealthBar hitPoints;
    [System.NonSerialized]
    public HealthBar sanity;



    [System.NonSerialized]
    public bool isKeyboard = false;
    [System.NonSerialized]
    public Dictionary<int, Vector3> lastPositionInMap = new Dictionary<int, Vector3>();

    // QTE

    public bool enableInput = true;
    public GameObject QTEManagerPrefab;
    [System.NonSerialized]
    public GameObject QTEManager;
    private QTE qteScript;
    private GameObject QTEcanvas;
    [System.NonSerialized]
    public bool QTEisSuccess;

    // interaction

    public bool interactionAllowed = true;

    // Object Pool

    public GameObjectPool Pool { get; set; }

    //Skills
    public bool hasUnlockedHeavyAttack;
    public bool hasUpgradedPotion;
    public bool hasUnlockedRoll = true;
    public bool hasUnlockedLifesteal;
    public bool hasUnlockedDodge;
    public bool hasUpgradedSpells;


    void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            Pool = GetComponent<GameObjectPool>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    //Methods

    // Start is called before the first frame update
    void Start()
    {
        //valueToDisplay = valueType.Sanity;
        playerTransform = GameObject.Find("Player").GetComponent<Transform>();
        camera = FindObjectOfType<Camera>();
        cmVcam1 = Camera.main.gameObject.transform.Find("CM vcam1").gameObject.GetComponent<CinemachineVirtualCamera>();
        cmVcam2 = Camera.main.gameObject.transform.Find("CM vcam2").gameObject.GetComponent<CinemachineVirtualCamera>();
        cmVcam2.Follow = playerTransform;
        cmVcam2.LookAt = playerTransform;
        cmVcam2.enabled = false;
        cmVcam1.enabled = true;
        hitPoints = healthbars.GetComponentsInChildren<HealthBar>()[0];
        sanity = healthbars.GetComponentsInChildren<HealthBar>()[1];

        hitPoints.Init(0f,Player.Instance.maxHitPoints);
        hitPoints.gameObject.SetActive(false);
        sanity.Init(Player.Instance.PlayerMentalHealth.insaneThreshHold, Player.Instance.PlayerMentalHealth.maxSanity);

    }

    // Player 

    public void InitializePlayerInfo(PlayerInput input)
    {
        playerInput = input;
        player = input.gameObject;

        SetPlayerAnimationTimes();
    }

    public void SpawnPlayer(int currentScene)
    {
        if (lastPositionInMap.ContainsKey(currentScene))
        {
            Player.Instance.gameObject.transform.position = lastPositionInMap[currentScene];
        }
        else
        {
            var spawnPoint = GameObject.FindGameObjectWithTag("SpawnPoint");
            if (spawnPoint)
            {
                Player.Instance.gameObject.transform.position = GameObject.FindGameObjectWithTag("SpawnPoint").transform.position;
            }
            else
            {
                Debug.LogError("No Spawn Point : ok if intro or outro.");
            }
        }
    }

    // Pause

    public void Pause()
    {
        if ((enableInput) && (!GameMode.Instance.gameIsPaused))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            enableInput = false;
            //GetComponentInParent<PauseMenuActions>().OnPause();
            PauseMenuUI.GetComponent<PauseMenuActions>().OnPause();
        }
        else if ((!enableInput) && (GameMode.Instance.gameIsPaused))
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            enableInput = true;
            //GetComponentInParent<PauseMenuActions>().OnPause();
            PauseMenuUI.GetComponent<PauseMenuActions>().OnPause();
        }
    }

    public void DisplayHealthBar()
    {
        sanity.gameObject.SetActive(false);
        hitPoints.gameObject.SetActive(true);
        hitPoints.Update(Player.Instance.HitPoints);
    }

    public void DisplaySanityBar()
    {
        hitPoints.gameObject.SetActive(false);
        sanity.gameObject.SetActive(true);
        sanity.Update(Player.Instance.PlayerMentalHealth.Sanity);

    }

    /* Health and Insanity HUD */

    /*public void SwitchValueToDisplay()
    {
        if (valueToDisplay == valueType.Health)
        {
            valueToDisplay = valueType.Sanity;
            healthImage.color = SanityColor;
        }
        else
        {
            valueToDisplay = valueType.Health;
            healthImage.color = HealthColor;
        }
        //UpdateValueToDisplay();

    }*/

    /*public void UpdateValueToDisplay()
    {
        switch (valueToDisplay)
        {
            case valueType.Health:
                healthBar.value = Player.Instance.GetHealthPercentage();
                break;

            case valueType.Sanity:
                healthBar.value = Player.Instance.PlayerMentalHealth.GetSanityPercentage();
                break;

            default:
                Debug.Log("undefined value to display");
                break;
        }
    }*/

    public void UpdateAnimatorSpeed(float newSpeed)
    {
        Animator playerAnimator = Player.Instance.GetComponent<Animator>();
        playerAnimator.speed += newSpeed;
        playerAnimator.speed = Mathf.Max(playerAnimator.speed, 1.0f);
    }

    public void UpdateSanityPotionDisplay()
    {
        sanityPotion.fillAmount = Player.Instance.PlayerMentalHealth.GetPotionSanityPercentage();
    }

    /* QTE */

    public void StartQTE(bool isKeyboard)
    {
        // TODO : réparer et vérifier que ça ne fait pas d'InvalidOperationException
        if ((enableInput)&&(!Player.Instance.PlayerMentalHealth.IsInsane()))
        {
            cmVcam1.enabled = false;
            cmVcam2.enabled = true;
            this.isKeyboard = isKeyboard;

            enableInput = false;
            playerInput.SwitchCurrentActionMap("QTE");

            QTEManager = Instantiate(QTEManagerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            qteScript = QTEManager.GetComponent<QTE>();

            QTEcanvas = camera.transform.Find("QTE").gameObject;
            QTEcanvas.SetActive(true);

            qteScript.Initialize(QTEcanvas.GetComponentInChildren<Image>(), isKeyboard);
        }
    }

    public void OnQTEAction(QTE.Keys key)
    {
        qteScript.CheckKeyPressed(key);
    }

    public void StopQTE(bool isSuccess)
    {
        if (!enableInput)
        {
            cmVcam2.enabled = false;
            cmVcam1.enabled = true;
            QTEisSuccess = isSuccess;

            enableInput = true;
            playerInput.SwitchCurrentActionMap("Player");

            QTEcanvas.SetActive(false);

            if (QTEManager.TryGetComponent<FlagComponent>(out var flag))
            {
                flag.Trigger();
            }

            Destroy(qteScript);
            Destroy(QTEManager);

            // Todo : GC ? So
        }
    }

    public void ChangeMentalState(MentalHealthState state)
    {
        EnemyManager.Instance.ChangeTargetAlpha(state.getEldritchAlpha());

    }

    public void SetMonsterAggro(bool isAggro)
    {
        EnemyManager.Instance.setAggro(isAggro);
    }

    public void EnterEldritchWorld()
    {
        AudioManager.Instance.PlayMusic(AudioManager.Music.ELDRITCH_WORLD);
        ArtManager.Instance.ActivateEldritchWorldFilter();
        EnemyManager.Instance.GeneratePersoEldritchs();
    }

    public void ExitEldritchWorld()
    {
        AudioManager.Instance.PlayMusic(AudioManager.Music.PREVIOUS);
        ArtManager.Instance.DeactivateEldritchWorldFilter();
    }

    public void QuickAttack()
    {
        if (enableInput)
        {
            PlayerAttacks playerAttacks = player.GetComponent<PlayerAttacks>();
            playerAttacks.DoAttack();
        }
    }

    public void HeavyAttack()
    {
        if (enableInput)
        {
            PlayerAttacks playerAttacks = player.GetComponent<PlayerAttacks>();
            playerAttacks.DoHeavyAttack();
        }
    }

    public void Spell01()
    {
        if (enableInput)
        {
            PlayerAttacks playerAttacks = player.GetComponent<PlayerAttacks>();
            playerAttacks.DoSpell01();
        }
    }

    public void ActivateParry()
    {
        if (enableInput)
        {
            PlayerAttacks playerAttacks = player.GetComponent<PlayerAttacks>();
            playerAttacks.StartParry();
        }
    }

    public void StopParry()
    {
        if (enableInput)
        {
            PlayerAttacks playerAttacks = player.GetComponent<PlayerAttacks>();
            playerAttacks.StopParry();
        }
    }

    public bool IsParrying()
    {
        if (ParryHitBox.activeSelf)
        {
            return true;
        }
        return false;
    }

    public void TriggerRoll()
    {
        if (enableInput && hasUnlockedRoll)
        {
            if (player.GetComponent<PlayerAttacks>().GetAttackTimer() >=
                    player.GetComponent<PlayerAttacks>().GetAttackAnimationDuration())
            {
                player.GetComponent<PlayerAttacks>().TriggerRolling();
            }
        }
    }

    private void SetPlayerAnimationTimes()
    {
        Animator anim = player.GetComponent<Animator>();
        if (anim == null)
        {
            Debug.LogError("No anim found ?");
        }

        AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;

        float aoeLength = 0.0f;
        float execMagicLength = 0.0f;
        float execMagic2Length = 0.0f;
        float execPhysicLength = 0.0f;
        foreach (AnimationClip clip in clips)
        {
            switch (clip.name)
            {
                case "Forward Roll":
                    player.GetComponent<PlayerAttacks>().SetRollAnimationDuration(clip.length);
                    break;
                case "Quick Attack 1":
                    player.GetComponent<PlayerAttacks>().SetAttackAnimationDuration(clip.length);
                    break;
                case "Heavy Attack":
                    player.GetComponent<PlayerAttacks>().SetHAttackAnimationDuration(clip.length);
                    break;
                case "AoE Spell":
                    player.GetComponent<PlayerAttacks>().SetSpellAoEAnimationDuration(clip.length);
                    break;
                case "Spell01":
                    player.GetComponent<PlayerAttacks>().SetSpell01AnimationDuration(clip.length);
                    break;
                case "Armed-Cast-L-Summon1_start":
                    execMagicLength += clip.length;
                    break;
                case "Armed-Cast-L-Summon1_loop":
                    execMagicLength += clip.length;
                    break;
                case "Armed-Cast-L-Summon1_end":
                    execMagicLength += clip.length;
                    break;
                case "Armed-Cast-R-Summon1_start":
                    execMagic2Length += clip.length;
                    break;
                case "Armed-Cast-R-Summon1_loop":
                    execMagic2Length += clip.length;
                    break;
                case "Armed-Cast-R-Summon1_end":
                    execMagic2Length += clip.length;
                    break;
                case "Sword-Attack-R1":
                    player.GetComponent<PlayerAttacks>().SetAttackAnimationDuration(clip.length);
                    execPhysicLength += clip.length;
                    break;
                case "Sword-Attack-R2":
                    execPhysicLength += clip.length;
                    break;
                case "Sword-Attack-R4":
                    execPhysicLength += clip.length;
                    break;
                case "Sword-Attack-R7":
                    execPhysicLength += clip.length;
                    break;
            }
        }
        player.GetComponent<PlayerAttacks>().SetExecuteAnimationDuration(execMagicLength, execMagic2Length, execPhysicLength);
    }

    public void TriggerAoeSpell()
    {
        if (enableInput)
        {
            PlayerAttacks playerAttacks = player.GetComponent<PlayerAttacks>();
            playerAttacks.DoAoeSpell();
        }
    }

    public void TriggerExecute()
    {
        if (enableInput)
        {
            PlayerAttacks playerAttacks = player.GetComponent<PlayerAttacks>();
            playerAttacks.DoExecute();
        }
    }

    public void SetInvincible(bool i)
    {
        Player.Instance.isInvincible = i;
    }

    public void StunPlayer()
    {
        PlayerAttacks playerAttacks = player.GetComponent<PlayerAttacks>();
        playerAttacks.Stun();
    }

    public bool IsPlayerInsane()
    {
        return Player.Instance.PlayerMentalHealth.IsInsane();
    }

    public void BossDefeated()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        GetComponentInParent<SkillAction>().OnStart();
    }

    public GameObject FindClosestEldritchInExecuteRange()
    {
        return EnemyManager.Instance.FindClosestEnemyInExecuteRange(Player.Instance.transform.position, Player.Instance.transform.Find("ExecuteZone"), Player.Instance.transform.Find("GuardKOZone"));
    }

    public void UnlockHeavyAttack()
    {
        hasUnlockedHeavyAttack = true;
    }

    public void UpgradePotionQuantity()
    {
        hasUpgradedPotion = true;
        Player.Instance.PlayerMentalHealth.UpgradePotionSize();
    }
    public void UnlockRoll()
    {
        hasUnlockedRoll = true;
    }
    public void UnlockLifesteal()
    {
        hasUnlockedLifesteal = true;
    }
    public void UnlockDodge()
    {
        hasUnlockedDodge = true;
    }
    public void UpgradeSpellDamage()
    {
        hasUpgradedSpells = true;
    }

    public List<bool> HasUnlockedList()
    {
        List<bool> skills = new List<bool>();
        skills.Add(hasUnlockedHeavyAttack);
        skills.Add(hasUpgradedPotion);
        skills.Add(hasUnlockedRoll);
        skills.Add(hasUnlockedLifesteal);
        skills.Add(hasUnlockedDodge);
        skills.Add(hasUpgradedSpells);

        return skills;
    }

    public void PlayerDied()
    {
        GetComponentInParent<Death>().OnStart();
    }

    public void StartTutorial()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        GetComponentInParent<TutorialActions>().OnStart();
    }
}
