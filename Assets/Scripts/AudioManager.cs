﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public enum Music
    {
        MENU,
        TAVERN,
        JEALOUS_VOICE,
        BOSS,
        END,
        CALM,
        FIGHT,
        SPOOKY,
        TEA_ROOM,
        DEATH,
        ELDRITCH_WORLD,
        PREVIOUS,
        FIGHT_ACT2,
    }

    //SINGLETON
    public static AudioManager Instance;

    // Sources

    AudioSource musicSource;
    AudioSource soundSource;

    // Music Clips
    AudioClip previousClip;
    AudioClip tavernClip;
    AudioClip bossClip;
    AudioClip endClip;
    AudioClip jealousVoiceClip;
    AudioClip villageClip;
    AudioClip spookyClip;
    AudioClip fightClip;
    AudioClip teaRoomClip;
    AudioClip deathClip;
    AudioClip eldritchClip;
    AudioClip longFightClip;

    // Sounds Clips

    AudioClip lightAttackClip;
    AudioClip heavyAttackClip;
    AudioClip spellClip;
    AudioClip aoeClip;

    AudioClip healingClip;

    // Volume

    [System.NonSerialized]
    public float musicVolume = 0.1f;
    [System.NonSerialized]
    public float soundVolume = 0.15f;
    [System.NonSerialized]
    public float dialogueVolume = 1.0f;

    // Effects

    [System.NonSerialized]
    public float fadeOutTime = 5.0f;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }


    private void Start()
    {
        musicSource = gameObject.AddComponent<AudioSource>();
        soundSource = gameObject.AddComponent<AudioSource>();

        musicSource.loop = true;

        // Music clips

        tavernClip = (AudioClip)Resources.Load("Music/tavern");
        bossClip = (AudioClip)Resources.Load("Music/bossFight");
        endClip = (AudioClip)Resources.Load("Music/end");
        jealousVoiceClip = (AudioClip)Resources.Load("Music/jealousVoice");
        villageClip = (AudioClip)Resources.Load("Music/villageAmbiance");
        spookyClip = (AudioClip)Resources.Load("Music/spooky");
        fightClip = (AudioClip)Resources.Load("Music/fight");
        teaRoomClip = (AudioClip)Resources.Load("Music/halloween");
        deathClip = (AudioClip)Resources.Load("Music/death");
        eldritchClip = (AudioClip)Resources.Load("Music/itsComing");
        longFightClip = (AudioClip)Resources.Load("Music/breakingTheSiege");

        // Sound clips

        lightAttackClip = (AudioClip)Resources.Load("Sounds/woosh");
        heavyAttackClip = (AudioClip)Resources.Load("Sounds/heavyWoosh");
        aoeClip = (AudioClip)Resources.Load("Sounds/aoe");
        spellClip = (AudioClip)Resources.Load("Sounds/fireWoosh");

        healingClip = (AudioClip)Resources.Load("Sounds/healing");

        StartCoroutine(PlayMusicOnLevelSwitch());
    }

    private IEnumerator PlayMusicOnLevelSwitch()
    {
        int actualScene = SceneManager.GetActiveScene().buildIndex;

        switch (actualScene)
        {
            case 0: // L_Intro
                PlayMusic(Music.MENU, 0.0f);
                break;
            case 1: // Act1_Part0
                PlayMusic(Music.SPOOKY, 0.0f);
                break;
            case 2: // Act1_Tavern
                PlayMusic(Music.TAVERN);
                break;
            case 3: // Act1_Part1
                PlayMusic(Music.SPOOKY, 0.0f);
                break;
            case 4: // Act2_Part0
                PlayMusic(Music.SPOOKY, 0.0f);
                break;
            case 5: // Act2_Tavern
                PlayMusic(Music.TAVERN);
                break;
            case 6: // Act2_Act1
                PlayMusic(Music.SPOOKY);
                break;
            case 7: // Act3_Part0
                PlayMusic(Music.SPOOKY);
                break;
            case 8: // Act3_TeaRoom
                PlayMusic(Music.SPOOKY);
                break;
            case 9: // Act3_InsanePart
                PlayMusic(Music.SPOOKY);
                break;
            case '0':
                PlayMusic(Music.SPOOKY, 0.0f);
                break;
        }

        yield return StartCoroutine(GameMode.Instance.WaitForSceneSwitchFromActualScene(actualScene));

        StartCoroutine(PlayMusicOnLevelSwitch());
    }

    // Volume controllers

    public void SetMusicVolume(float volume)
    {
        musicSource.volume = musicVolume = volume;
    }
    public void SetSoundVolume(float volume)
    {
        soundSource.volume = soundVolume = volume;
    }
    public void SetDialogueVolume(float volume)
    {
        dialogueVolume = volume;
    }

    // Music effects

    public IEnumerator FadeOutMusic(float fadeTime = 5.0f)
    {
        //StopAllCoroutines();
        //StartCoroutine(PlayMusicOnLevelSwitch());

        while (musicSource.volume > 0.0f)
        {
            musicSource.volume -= musicVolume * Time.deltaTime / fadeTime;
            yield return null;
        }

        musicSource.volume = 0.0f;
    }

    private IEnumerator FadeInMusic(float fadeTime = 5.0f)
    {
        //StopAllCoroutines();
        //StartCoroutine(PlayMusicOnLevelSwitch());

        float startVolume = musicSource.volume;

        while (musicSource.volume < musicVolume)
        {
            musicSource.volume += musicVolume * Time.deltaTime / fadeTime;
            yield return null;
        }

        musicSource.volume = musicVolume;
    }

    // Musics

    public void PlayMusic(Music choice, float fadeInTime = 5.0f)
    {
        bool isSameMusic = false;
        switch (choice)
        {
            case Music.MENU:
                if (musicSource.clip == villageClip)
                    isSameMusic = true;
                musicSource.clip = villageClip;
                break;
            case Music.TAVERN:
                if (musicSource.clip == tavernClip)
                    isSameMusic = true;
                musicSource.clip = tavernClip;
                break;
            case Music.BOSS:
                if (musicSource.clip == bossClip)
                    isSameMusic = true;
                musicSource.clip = bossClip;
                break;
            case Music.END:
                if (musicSource.clip == endClip)
                    isSameMusic = true;
                musicSource.clip = endClip;
                break;
            case Music.JEALOUS_VOICE:
                if (musicSource.clip == jealousVoiceClip)
                    isSameMusic = true;
                musicSource.clip = jealousVoiceClip;
                break;
            case Music.SPOOKY:
                if (musicSource.clip == spookyClip)
                    isSameMusic = true;
                musicSource.clip = spookyClip;
                break;
            case Music.FIGHT:
                if (musicSource.clip == fightClip)
                    isSameMusic = true;
                musicSource.clip = fightClip;
                break;
            case Music.TEA_ROOM:
                if (musicSource.clip == teaRoomClip)
                    isSameMusic = true;
                musicSource.clip = teaRoomClip;
                break;
            case Music.DEATH:
                if (musicSource.clip == deathClip)
                    isSameMusic = true;
                musicSource.clip = deathClip;
                break;
            case Music.ELDRITCH_WORLD:
                if (musicSource.clip == eldritchClip)
                    isSameMusic = true;
                previousClip = musicSource.clip;
                musicSource.clip = eldritchClip;
                break;
            case Music.PREVIOUS:
                if (previousClip)
                {
                    if (musicSource.clip == previousClip)
                        isSameMusic = true;
                    musicSource.clip = previousClip;
                    previousClip = null;
                }
                break;
            case Music.FIGHT_ACT2:
                if (musicSource.clip == longFightClip)
                    isSameMusic = true;
                musicSource.clip = longFightClip;
                break;

        }

        if (!isSameMusic)
        {
            musicSource.Play();
            StartCoroutine(FadeInMusic(fadeInTime));
        }
    }

    // Sounds

    public void PlayLightAttackSound()
    {
        soundSource.PlayOneShot(lightAttackClip);
    }

    public void PlayHeavyAttackSound()
    {
        soundSource.PlayOneShot(heavyAttackClip);
    }

    public void PlaySpellSound()
    {
        soundSource.PlayOneShot(spellClip);
    }

    public void PlayAoeSound()
    {
        soundSource.PlayOneShot(aoeClip);
    }

    public void PlayHealingSound()
    {
        soundSource.PlayOneShot(healingClip);
    }
}
