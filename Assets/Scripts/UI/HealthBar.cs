﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private float max_value;
    private float min_value;
    private float current_value;

    private float hidden_current_value;

    public Slider primaryHealthBar;
    public Slider secondaryHealthBar;



        
    // Start is called before the first frame update
    void Start()
    {
        current_value = max_value;
        hidden_current_value = current_value;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(float value)
    {
        Assert.IsTrue(value >= 0);
        if(this.isActiveAndEnabled)StartCoroutine(TakeDamageCoroutine(value));
    }

    IEnumerator TakeDamageCoroutine(float value)
    {
        if ((current_value - value) > min_value)
        {
            //StartCoroutine(changeSize());
            current_value = Mathf.Clamp(current_value - value, 0.0f, max_value);
            primaryHealthBar.value = current_value / max_value;
            StartCoroutine(takeSecondaryDamage());
        } else
        {
            current_value = Mathf.Clamp(current_value - value, 0.0f, min_value);
            primaryHealthBar.value = current_value / max_value;
            hidden_current_value = current_value;
            secondaryHealthBar.value = hidden_current_value / max_value;

        }
        yield return null;
    }

    IEnumerator takeSecondaryDamage()
    {
        yield return new WaitForSeconds(0.5f);
        hidden_current_value = current_value;
        secondaryHealthBar.value = hidden_current_value / max_value;
        yield return null;


    }

    IEnumerator changeSize()
    {
        transform.localScale += new Vector3(0.25f, 0.0f, 0.25f);
        yield return new WaitForSeconds(0.15f);
        transform.localScale -= new Vector3(0.25f, 0.0f, 0.25f);
        yield return null;


    }

    public void Heal(float value)
    {
        if(value < 0.0f)
        {
            Debug.LogError("Health should not be below 0");
            value = 0.0f;
        }
        
        if (this.isActiveAndEnabled) StartCoroutine(HealCoroutine(value));
    }

    IEnumerator HealCoroutine(float value)
    {
        //StartCoroutine(changeSize());
        hidden_current_value = Mathf.Clamp(hidden_current_value + value, 0.0f, max_value);
        secondaryHealthBar.value = hidden_current_value / max_value;
        StartCoroutine(takePrimaryHeal());       
        yield return null;
    }

    IEnumerator takePrimaryHeal()
    {
        yield return new WaitForSeconds(0.5f);
        current_value = hidden_current_value;
        primaryHealthBar.value = current_value / max_value;
        yield return null;

    }

    public void Init(float min, float max)
    {
        max_value = max;
        min_value = min;
        current_value = max_value;
        hidden_current_value = max_value;
        primaryHealthBar.value = current_value;
        secondaryHealthBar.value = current_value;

    }

    public void Update(float value)
    {
        current_value = value;
        hidden_current_value = value;
        primaryHealthBar.value = current_value / max_value;
        secondaryHealthBar.value = hidden_current_value / max_value;
    }
}
