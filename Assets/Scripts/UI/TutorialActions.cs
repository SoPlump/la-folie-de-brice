﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialActions : MonoBehaviour
{
    public int pageNumber = 1;
    public GameObject Tutorial;
    public GameObject Part1;
    public GameObject Part2;
    public GameObject Part3;
    public GameObject HUD;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnStart()
    {
        Tutorial.SetActive(true);
        HUD.SetActive(false);

        GameManager.Instance.enableInput = false;

        Part1.SetActive(true);
        Part2.SetActive(false);
        Part3.SetActive(false);

        pageNumber = 1;

        Time.timeScale = 0f;
    }

    public void OnNext()
    {
        if(pageNumber == 1)
        {
            Part1.SetActive(false);
            Part2.SetActive(true);
            pageNumber++;
        }
        else if (pageNumber == 2)
        {
            Part2.SetActive(false);
            Part3.SetActive(true);

            pageNumber++;
        }
        else if (pageNumber == 3)
        {
            OnQuit();
        }
    }

    public void OnQuit()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        GameManager.Instance.enableInput = true;
        Tutorial.SetActive(false);
        HUD.SetActive(true);

        Time.timeScale = 1f;
    }
}
