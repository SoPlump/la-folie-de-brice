﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownUI : MonoBehaviour
{
    private PlayerAttacks playerAttacks;

    public Image greyFilter1;
    public Image greyFilter2;
    public Image greyFilter3;
    public Image greyFilter4;

    private bool changed;


    // Start is called before the first frame update
    void Start()
    {
        playerAttacks = Player.Instance.GetComponent<PlayerAttacks>();
        if (GameManager.Instance.hasUnlockedHeavyAttack)
        {
            changed = true;
            GameObject.Find("HeavyAttackImage").GetComponent<Image>().enabled = true;
            GameObject.Find("AttackImage").GetComponent<Image>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!playerAttacks.AttackReady())
            greyFilter1.fillAmount = (playerAttacks.cane.attackCooldown - playerAttacks.GetAttackTimer()) / playerAttacks.cane.attackCooldown;
        else
            greyFilter1.fillAmount = 0;
        if (!playerAttacks.HeavyAttackReady())
            greyFilter1.fillAmount = (playerAttacks.cane.heavyAttackCooldown - playerAttacks.GetHeavyAttackTimer()) / playerAttacks.cane.heavyAttackCooldown;
        else
            greyFilter2.fillAmount = 0;
        if (!playerAttacks.Spell01Ready())
            greyFilter2.fillAmount = (playerAttacks.spell01cooldown - playerAttacks.spell01Timer) / playerAttacks.spell01cooldown;
        else
            greyFilter2.fillAmount = 0;
        if (!playerAttacks.AoeSpellReady())
            greyFilter3.fillAmount = (playerAttacks.spellAoECooldown - playerAttacks.spellAoeTimer) / playerAttacks.spellAoECooldown;
        else
            greyFilter3.fillAmount = 0;
        if (!playerAttacks.RollReady())
            greyFilter4.fillAmount = (playerAttacks.rollCooldown - playerAttacks.rollTimer) / playerAttacks.rollCooldown;
        else
            greyFilter4.fillAmount = 0;
        if (!changed && GameManager.Instance.hasUnlockedHeavyAttack)
        {
            changed = true;
            GameObject.Find("HeavyAttackImage").GetComponent<Image>().enabled = true;
            GameObject.Find("AttackImage").GetComponent<Image>().enabled = false;
        }
    }
}
