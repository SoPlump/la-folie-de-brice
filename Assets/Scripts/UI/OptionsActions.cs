﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsActions : MonoBehaviour
{
    public GameObject musicSlider;
    public GameObject soundSlider;
    public GameObject dialogueSlider;

    private void Start()
    {
        musicSlider.GetComponent<Slider>().value = AudioManager.Instance.musicVolume;
        soundSlider.GetComponent<Slider>().value = AudioManager.Instance.soundVolume;
        dialogueSlider.GetComponent<Slider>().value = AudioManager.Instance.dialogueVolume;
    }

    public void SetMusicVolume(float volume)
    {
        AudioManager.Instance.SetMusicVolume(volume);
    }
    public void SetSoundVolume(float volume)
    {
        AudioManager.Instance.SetSoundVolume(volume);
    }
    public void SetDialogueVolume(float volume)
    {
        AudioManager.Instance.SetDialogueVolume(volume);
    }

    public void CloseOptionMenu()
    {
        gameObject.SetActive(false);
    }
}
