﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillAction : MonoBehaviour
{
    private List<bool> skills = new List<bool>();
    public GameObject skillChoice;
    private int numberOfChoicesDone;

    private void Start()
    {
        skills = GameManager.Instance.HasUnlockedList();
    }

    public void OnChoice()
    {
        numberOfChoicesDone += 1;

        GameObject.Find("HeavyAttack").GetComponent<Button>().interactable = !skills[0];
        GameObject.Find("PotionQuantity").GetComponent<Button>().interactable = !skills[1];
        GameObject.Find("Roll").GetComponent<Button>().interactable = !skills[2];
        GameObject.Find("Lifesteal").GetComponent<Button>().interactable = !skills[3];
        GameObject.Find("Dodge").GetComponent<Button>().interactable = !skills[4];
        GameObject.Find("SpellDamage").GetComponent<Button>().interactable = !skills[5];
        skills = GameManager.Instance.HasUnlockedList();

        if(numberOfChoicesDone == 2 || numberOfChoicesDone == 4)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            skillChoice.SetActive(false);
            GameManager.Instance.enableInput = true;
        }
    }

    public void Quit()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        skillChoice.SetActive(false);
        GameManager.Instance.enableInput = true;
    }

    public void OnStart()
    {
        numberOfChoicesDone = 0;
        StartCoroutine(WaitForBossToDie());
    }

    public void UnlockHeavyAttack()
    {
        GameManager.Instance.UnlockHeavyAttack();
        OnChoice();
    }

    public void UpgradePotionQuantity()
    {
        GameManager.Instance.UpgradePotionQuantity();
        OnChoice();
    }
    public void UnlockRoll()
    {
        GameManager.Instance.UnlockRoll();
        OnChoice();
    }
    public void UnlockLifesteal()
    {
        GameManager.Instance.UnlockLifesteal();
        OnChoice();
    }
    public void UnlockDodge()
    {
        GameManager.Instance.UnlockDodge();
        OnChoice();
    }
    public void UpgradeSpellDamage()
    {
        GameManager.Instance.UpgradeSpellDamage();
        OnChoice();
    }

    public void OnLeave()
    {
        GameObject.Find("Description").GetComponent<Text>().text = "";
    }

    public void OnHoverHeavy()
    {
        if (!skills[0])
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Unlock heavy attack";
        }
        else
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Heavy attack already unlocked";
        }
    }
    public void OnHoverPotion()
    {
        if (!skills[1])
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Increase the potion's capacity";
        }
        else
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Potion already upgraded";
        }
    }
    public void OnHoverRoll()
    {
        if (!skills[2])
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Unlock the ability to roll";
        }
        else
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Roll already unlocked";
        }
    }
    public void OnHoverLifesteal()
    {
        if (!skills[3])
        {
            GameObject.Find("Description").GetComponent<Text>().text = "NOT IMPLEMENTED YET | Recover a small amount of health upon hitting enemies";
        }
        else
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Lifesteal already learnt";
        }
    }
    public void OnHoverDodge()
    {
        if (!skills[4])
        {
            GameObject.Find("Description").GetComponent<Text>().text = "NOT IMPLEMENTED YET | Unlock dodging skills by pressing the right key at the right moment";
        }
        else
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Dodge already mastered";
        }
    }
    public void OnHoverSpell()
    {
        if (!skills[5])
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Empower your spells, granting more damage";
        }
        else
        {
            GameObject.Find("Description").GetComponent<Text>().text = "Spells already upgraded";
        }
    }

    IEnumerator WaitForBossToDie()
    {
        yield return new WaitForSeconds(5.0f);

        skillChoice.SetActive(true);

        GameManager.Instance.enableInput = false;

        skills = GameManager.Instance.HasUnlockedList();

        GameObject.Find("HeavyAttack").GetComponent<Button>().interactable = !skills[0];
        GameObject.Find("PotionQuantity").GetComponent<Button>().interactable = !skills[1];
        GameObject.Find("Roll").GetComponent<Button>().interactable = !skills[2];
        GameObject.Find("Lifesteal").GetComponent<Button>().interactable = !skills[3];
        GameObject.Find("Dodge").GetComponent<Button>().interactable = !skills[4];
        GameObject.Find("SpellDamage").GetComponent<Button>().interactable = !skills[5];
    }
}
