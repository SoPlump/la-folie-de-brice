﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenuActions : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public GameObject optionsUI;
    public GameObject saveButton;
    public GameObject quitButton;

    public void OnPause()
    {
        if (GameMode.Instance.gameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    public void SaveGame()
    {
        GameMode.Instance.SaveGame();
    }

    public void MainMenu()
    {
        SaveGame();
        pauseMenuUI.SetActive(false);

        SceneManager.LoadScene("Levels/L_Intro");
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        optionsUI.SetActive(false);

        Time.timeScale = 1f;
        GameMode.Instance.gameIsPaused = false;
        GameManager.Instance.enableInput = true;
    }

    public void OpenOptions()
    {
        optionsUI.SetActive(true);
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);

        if (Player.Instance.PlayerMentalHealth.IsInsane())
        {
            saveButton.GetComponent<Button>().interactable = false;
            quitButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            saveButton.GetComponent<Button>().interactable = true;
            quitButton.GetComponent<Button>().interactable = true;
        }

        Time.timeScale = 0f;
        GameMode.Instance.gameIsPaused = true;
    }
}
