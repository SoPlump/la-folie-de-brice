﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EldritchBar : MonoBehaviour
{
    private float totalEldritchs;
    private float eldritchsKilled;
    private int eldritchsSpawned = 0;
    private int previousEldritchsSpawned = 0;
    private int differenceInSpawn;

    public Slider eldritchBarSlider;

    // Start is called before the first frame update
    void Start()
    {
        totalEldritchs = EnemyManager.Instance.GetNumberOfEldritchs();
    }

    // Update is called once per frame
    void Update()
    {
        eldritchsKilled = EnemyManager.Instance.GetNumberOfEldritchsKilled();
        if(eldritchsSpawned < EnemyManager.Instance.GetNumberOfEldritchsSpawned())
        {
            eldritchsSpawned = EnemyManager.Instance.GetNumberOfEldritchsSpawned();
            differenceInSpawn = eldritchsSpawned - previousEldritchsSpawned;
            totalEldritchs = totalEldritchs + differenceInSpawn;
            previousEldritchsSpawned = EnemyManager.Instance.GetNumberOfEldritchsSpawned();
        }

        eldritchBarSlider.value = 1- (totalEldritchs - eldritchsKilled) / totalEldritchs;
    }
}
