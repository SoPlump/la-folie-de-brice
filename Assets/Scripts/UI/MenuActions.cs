﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuActions : MonoBehaviour
{
    public GameObject optionsUI;

    public void Awake()
    {
        Time.timeScale = 0f;
    }

    public void NewGame()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Levels/Act1/Act1_Part0_IndustrialDistrict");
    }

    public void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            GameMode.Instance.StartLoadingGame();
        }
        else
        {
            Debug.Log("no game saved!");
        }
    }

    public void OpenOptions()
    {
        optionsUI.SetActive(true);
    }

    public void Quit()
    {
        SceneManager.LoadScene("Levels/L_Outro");
    }
}
