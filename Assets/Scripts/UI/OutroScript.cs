﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutroScript : MonoBehaviour
{
    public void OpenURL()
    {
        Application.OpenURL("https://docs.google.com/document/d/1YDx9ywgP7QUngPRAcUH_AEjXjSa3FsyFoRfD64iMFs0/edit?usp=sharing");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
