﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class PeopleBehaviourManager : MonoBehaviour
{
    Animator anim;
    PlayableDirector playableDirector;
    public bool isDrinking;
    public bool isTalking;
    public bool isSitting;

    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        gameObject.TryGetComponent<PlayableDirector>(out playableDirector);

        if (isDrinking)
            StartDrinking();
        if (isTalking)
            StartTalking();
        if (isSitting)
            StartSitting();
    }

    public void StartTalking(bool usePlayableDirector = true)
    {
        if (!isSitting)
            anim.SetBool("isTalking", true);

        if ((usePlayableDirector)&&(playableDirector))
        {
            GameManager.Instance.enableInput = false;
            playableDirector.Play();
        }
    }

    public void StopTalking()
    {
        anim.SetBool("isTalking", false);
        if (playableDirector)
        {
            GameManager.Instance.enableInput = true;
            playableDirector.Stop();
        }
    }

    public void StartDrinking()
    {
        anim.SetBool("isDrinking", true);
    }
    public void StopDrinking()
    {
        anim.SetBool("isDrinking", false);
    }

    public void StartSitting()
    {
        anim.SetBool("isSitting", true);
    }
    public void StopSitting()
    {
        anim.SetBool("isSitting", false);
    }
}
