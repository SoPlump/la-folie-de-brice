﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;


public class ArtManager : MonoBehaviour
{

    public static ArtManager Instance;

    // Camera
    Camera camera;
    PostProcessVolume postProcessVolume;

    void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(gameObject);

            //Rest of your Awake code

        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        camera = FindObjectOfType<Camera>();
        postProcessVolume = camera.GetComponentInChildren<PostProcessVolume>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActivateEldritchWorldFilter()
    {
        postProcessVolume.enabled = true;
    }

    public void DeactivateEldritchWorldFilter()
    {
        postProcessVolume.enabled = false;
    }
}
