﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum QueuingMode
{
    QUEUING,
    FLUSH,
    QUEUE_IF_QUEUE_EMPTY,
}

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager Instance;

    void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    [System.Serializable]
    public struct QuestData
    {
        public int nbSentences;
        public string questName;
    }

    Queue<string> dialoguesToPlay = new Queue<string>();
    Dictionary<string, Dialogue> dialogues = new Dictionary<string, Dialogue>();
    Dictionary<string, GameObject> NPC = new Dictionary<string, GameObject>();

    AudioSource playingAudio = null;
    bool isPlaying = false;

    private void Start()
    {
        enabled = false;
    }

    // Scenario dialogues (would have been nice in json)

    public void LoadScenarioDialoguesFromLevel(ScenarioManager.Shortcuts shortcut)
    {
        NPC.Clear();
        FindNPCInLevel();

        dialogues.Clear();

        switch (shortcut)
        {
            case ScenarioManager.Shortcuts.ACT1_PART0:
                CreateScenarioDialogues_Act1_Part0();
                break;
            case ScenarioManager.Shortcuts.ACT1_TAVERN:
                CreateScenarioDialogues_Act1_Tavern();
                break;
            case ScenarioManager.Shortcuts.ACT1_PART1:
                CreateScenarioDialogues_Act1_Part1();
                break;
            case ScenarioManager.Shortcuts.ACT2_PART0:
                CreateScenarioDialogues_Act2_Part0();
                break;
            case ScenarioManager.Shortcuts.ACT2_TAVERN:
                CreateScenarioDialogues_Act2_Tavern();
                break;
            case ScenarioManager.Shortcuts.ACT2_PART1:
                //CreateScenarioDialogues_Act2_Part1();
                break;
            case ScenarioManager.Shortcuts.ACT3_PART0:
                CreateScenarioDialogues_Act3_Part0();
                break;
            case ScenarioManager.Shortcuts.ACT3_TEAROOM:
                CreateScenarioDialogues_Act3_TeaRoom();
                break;
            case ScenarioManager.Shortcuts.ACT3_INSANEPART:
                CreateScenarioDialogues_Act3_InsanePart();
                break;
            case ScenarioManager.Shortcuts.ACT4:
                CreateScenarioDialogues_Act4();
                break;
        }
    }

    private void FindNPCInLevel(string tag = "Talker")
    {
        List<GameObject> talkers = new List<GameObject>(GameObject.FindGameObjectsWithTag(tag));
        foreach (var talker in talkers)
        {
            if (!NPC.ContainsKey(talker.name))
            {
                NPC.Add(talker.name, talker);
            }
        }
    }

    private void CreateScenarioDialoguesTest()
    {
        AudioSource audio;
        Dialogue dialogue;
        if (NPC.ContainsKey("Diana"))
        {
            audio = NPC["Diana"].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/test_intro1Diana");
            if (audio.clip)
            {
                dialogue = new Dialogue("Diana", "Intro blabla", audio);
                dialogues.Add("intro1Diana", dialogue);
                audio.dopplerLevel = 0.0f;
            }

            audio = NPC["Diana"].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/test_intro2Diana");
            if (audio.clip)
            {
                dialogue = new Dialogue("Diana", "Intro blabla", audio);
                dialogues.Add("intro2Diana", dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }

        if (NPC.ContainsKey("Jean"))
        {
            audio = NPC["Jean"].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/test_intro0Jean");
            if (audio.clip)
            {
                dialogue = new Dialogue("Jean", "Intro blabla", audio);
                dialogues.Add("intro0Jean", dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }

    public void CreateScenarioDialogues_Act1_Part0()
    {
        AudioSource audio;
        Dialogue dialogue;
        string name, dialRef;

        name = "VoiceOverAudio";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act1Intro";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act1/Act1_Intro");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }

    public void CreateScenarioDialogues_Act1_Tavern()
    {
        AudioSource audio;
        Dialogue dialogue;
        string name, dialRef;

        name = "VoiceOverAudio";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act1TavernIntro";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act1/Act1_Tavern");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }

        name = "Barman";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act1Tavern";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act1/Act1_Tavern_UnlockDoor");
            if (audio.clip)
            {
                audio.spatialBlend = 1.0f;
                dialogue = new Dialogue(name, "Il y a une sale ambiance derrière la taverne... Je te laisse voir par toi-même.", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }

    public void CreateScenarioDialogues_Act1_Part1()
    {
        AudioSource audio;
        Dialogue dialogue;
        string name, dialRef;

        name = "VoiceOverAudio";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act1AfterBoss";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act1/Act1_AfterBossFight");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }

    public void CreateScenarioDialogues_Act2_Part0()
    {
        AudioSource audio;
        Dialogue dialogue;
        string name, dialRef;

        name = "VoiceOverAudio";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act2Intro";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act2/Act2_Intro");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act2Help";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act2/Act2_Help");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act2Next";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act2/Act2_Next");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }

    public void CreateScenarioDialogues_Act2_Tavern()
    {
        AudioSource audio;
        Dialogue dialogue;
        string name, dialRef;

        name = "VoiceOverAudio";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act2Tavern";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act2/Act2_Tavern");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }

    public void CreateScenarioDialogues_Act3_Part0()
    {
        AudioSource audio;
        Dialogue dialogue;
        string name, dialRef;

        name = "VoiceOverAudio";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act3GuardBefore";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_Part0_GuardBefore");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act3GuardFightAfter";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_Part0_GuardFightAfter");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }

        FindNPCInLevel("GuardEnemy");

        name = "Guard";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act3GuardPeace";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_Part0_GuardPeace");
            if (audio.clip)
            {
                audio.spatialBlend = 1.0f;
                dialogue = new Dialogue(name, "T'as vraiment une sale gueule.. Mais bon, si le roi te laisse passer ici j'ai rien à dire. Sinon je t'aurais déjà tué.", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act3GuardFight";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_Part0_GuardFight");
            if (audio.clip)
            {
                audio.spatialBlend = 1.0f;
                dialogue = new Dialogue(name, "Le roi n'a plus toute sa tête à faire venir un fou comme toi dans notre ville. Tu pouvais bien te mêler aux pauvres, mais ici, je ne te laisserai pas troubler les habitants.", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }

    public void CreateScenarioDialogues_Act3_TeaRoom()
    {
        AudioSource audio;
        Dialogue dialogue;
        string name, dialRef;

        name = "VoiceOverAudio";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act3TeaRoomIntro";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_GuardBefore");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act3BeforeQTE";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_TeaRoom_BeforeQTE");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "creepyVoices";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/CreepyVoices");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }

        name = "Bourgeois";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act3Hatred";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_TeaRoom_Hatred");
            if (audio.clip)
            {
                audio.spatialBlend = 1.0f;
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act3Insults";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_TeaRoom_Insults");
            if (audio.clip)
            {
                audio.spatialBlend = 1.0f;
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }

        name = "Guard";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act3AfterQTE";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_TeaRoom_AfterQTE");
            if (audio.clip)
            {
                audio.spatialBlend = 1.0f;
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }

    public void CreateScenarioDialogues_Act3_InsanePart()
    {
        AudioSource audio;
        Dialogue dialogue;
        string name, dialRef;

        name = "VoiceOverAudio";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act3AfterFight";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_InsanePart_AfterFight");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act3Help";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act3/Act3_InsanePart_Help");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }

    public void CreateScenarioDialogues_Act4()
    {
        AudioSource audio;
        Dialogue dialogue;
        string name, dialRef;

        name = "VoiceOverAudio";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act4Intro";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act4/Act4_Intro");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act4SpawnEldritchs";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act4/Act4_SpawnEldritchs");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
            
            dialRef = "act4Stuck";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act4/Act4_Stuck");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act4Action";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act4/Act4_Action");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
        FindNPCInLevel("EldritchBoss");

        name = "King";
        if (NPC.ContainsKey(name))
        {
            dialRef = "act4Betrayal";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act4/Act4_Betrayal");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }

            dialRef = "act4Disclosure";
            audio = NPC[name].AddComponent<AudioSource>();
            audio.clip = (AudioClip)Resources.Load("Dialogues/Act4/Act4_Disclosure");
            if (audio.clip)
            {
                dialogue = new Dialogue(name, "", audio);

                dialogues.Add(dialRef, dialogue);
                audio.dopplerLevel = 0.0f;
            }
        }
    }


    public void CreateQuestDialogues(QuestData[] data, string name)
    {
        AudioSource audio;
        Dialogue dialogue;
        string dialRef;

        if (NPC.ContainsKey(name))
        {
            for (int i = 0; i < data.Length; ++i)
            {
                for (int j = 0; j < data[i].nbSentences; ++j)
                {
                    dialRef = data[i].questName + j + name;
                    if (!dialogues.ContainsKey(dialRef))
                    {
                        audio = NPC[name].AddComponent<AudioSource>();
                        audio.clip = (AudioClip)Resources.Load("Dialogues/" + dialRef);
                        if (audio.clip)
                        {
                            audio.spatialBlend = 1.0f;
                            dialogue = new Dialogue(name, "Intro blabla", audio);
                            dialogues.Add(dialRef, dialogue);
                            audio.dopplerLevel = 0.0f;
                        }
                    }
                }
            }
        }
    }

    private void Update()
    {
        if (!isPlaying)
        {
            isPlaying = true;
            if (dialoguesToPlay.Count < 2)
            {
                enabled = false;
            }
            StartCoroutine(StartDialogue());
        }
    }

    public IEnumerator PlayDialogue(string dialogueRef, QueuingMode mode)
    {
        if (dialogues.ContainsKey(dialogueRef))
        {
            if (mode == QueuingMode.FLUSH)
            {
                dialoguesToPlay.Clear();
                dialoguesToPlay.Enqueue(dialogueRef);
                if (playingAudio)
                {
                    playingAudio.Stop();

                    if (playingAudio.GetComponentInParent<FlagComponent>())
                    {
                        playingAudio.GetComponentInParent<FlagComponent>().Trigger();
                    }

                    playingAudio = null;
                }

                isPlaying = true;
                yield return StartCoroutine(StartDialogue());
            }
            else if (mode == QueuingMode.QUEUING)
            {
                dialoguesToPlay.Enqueue(dialogueRef);
                enabled = true;

                yield return null;
            }
            else if (mode == QueuingMode.QUEUE_IF_QUEUE_EMPTY)
            {
                if (dialoguesToPlay.Count == 0)
                {
                    dialoguesToPlay.Enqueue(dialogueRef);
                    enabled = true;
                }

                yield return null;
            }
        }
    }

    private IEnumerator StartDialogue()
    {
        string dialogueRef = dialoguesToPlay.Dequeue();
        playingAudio = dialogues[dialogueRef]._audio;
        playingAudio.volume = AudioManager.Instance.dialogueVolume;
        playingAudio.Play();

        yield return new WaitForSeconds(playingAudio.clip.length);

        if (playingAudio)
        {
            if (playingAudio.gameObject.TryGetComponent<FlagComponent>(out var flag))
            {
                flag.Trigger();
            }
        }
        playingAudio = null;
        isPlaying = false;
    }

}
