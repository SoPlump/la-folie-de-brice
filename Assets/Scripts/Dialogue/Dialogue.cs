﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue
{

    public string _NPCName;
    public List<string> _sentences = new List<string>();
    public AudioSource _audio;

    public Dialogue(string NPCName, string sentences, AudioSource audio)
    {
        _NPCName = NPCName;
        _audio = audio;

        // for now
        _sentences.Add(sentences);
    }
}
