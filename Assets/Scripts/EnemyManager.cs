﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyManager : MonoBehaviour
{

    //SINGLETON
    public static EnemyManager Instance;

    private List<GameObject> eldritchs = new List<GameObject>();

    //ALPHA
    private float previous_alpha = 0.0f;
    private float current_alpha = 0.0f;
    private float target_alpha = 0.0f;
    private float transitionDuration = 5.0f;
    private float currentTransitionTime = 0.0f;

    private bool isAggressive = false;

    public GameObject meleeEldritch;
    public GameObject rangedEldritch;
    public GameObject persoEldritch;

    private GameObject bossOnScene;
    private EnemyBehaviour bossBehaviour;

    public int eldritchsKilled;
    public int numberOfEldritchs;
    public int eldritchsSpawned;

    //PERSO ELDRITCHS
    List<GameObject> persoEldritchsList = new List<GameObject>();
    public float minTimeBetweenGenerations = 0.0f;
    private float currentTimeBetweenGenerations;
    private bool generationReady = true;
    public float timeBetweenSpawns = 2.0f;
    public uint maxPersoEldritch = 5;
    public uint maxPersoEldritchPerGeneration = 3;

    //GARDES
    List<GameObject> guards = new List<GameObject>();

    //CLONES
    List<GameObject> clones = new List<GameObject>();



    void Awake()
    {
        if (Instance == null)
        {

            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }
    }
    
    void Start()
    {
        StartCoroutine(WaitForLevelLoading());
        var eldritchsOnScene = GameObject.FindGameObjectsWithTag("Eldritch");
        foreach(GameObject eldritch in eldritchsOnScene)
        {
            eldritchs.Add(eldritch);
        }

        numberOfEldritchs = eldritchs.Count;
        eldritchsKilled = 0;
        SetEldritchsAlpha();
        SetEldritchsColliders(isAggressive);

    }

    public void Clear()
    {
        persoEldritchsList.Clear();
        guards.Clear();
        eldritchs.Clear();
        clones.Clear();
        eldritchsKilled = 0;
        numberOfEldritchs = 0;
        eldritchsSpawned = 0;
        currentTransitionTime = 0;
        generationReady = true;
        isAggressive = false;
        currentTimeBetweenGenerations = 0.0f;
    }

    void Update()
    {
        if (isAggressive)
        {
            foreach (GameObject eldritch in eldritchs)
            {
                var behaviour = eldritch.GetComponent<EnemyBehaviour>();
                behaviour.RotateToFacePlayer();
                behaviour.Move();
                behaviour.FireAttack();
            }
        }

        if (current_alpha != target_alpha)
        {
            currentTransitionTime += Time.deltaTime;
            SetEldritchsAlpha();
        }

        if (!generationReady)
        {
            currentTimeBetweenGenerations += Time.deltaTime;
            if(currentTimeBetweenGenerations >= minTimeBetweenGenerations)
            {
                generationReady = true;
                currentTimeBetweenGenerations = 0.0f;

            }
        }

        if (bossBehaviour)
        {
            bossBehaviour.RotateToFacePlayer();
            bossBehaviour.Move();
            bossBehaviour.FireAttack();
        }

        foreach (GameObject guard in guards)
        {
            var behaviour = guard.GetComponent<EnemyBehaviour>();
            behaviour.RotateToFacePlayer();
            behaviour.Move();
            behaviour.FireAttack();
        }

        foreach (GameObject clone in clones)
        {
            var behaviour = clone.GetComponent<EnemyBehaviour>();
            behaviour.RotateToFacePlayer();
            behaviour.Move();
            behaviour.FireAttack();
        }
    }

    public void SpawnMeleeEldritch(GameObject spawnZone)
    {
        Bounds bounds = spawnZone.GetComponent<Collider>().bounds;
        float x = Random.Range(bounds.min.x, bounds.max.x);
        float z = Random.Range(bounds.min.z, bounds.max.z);

        var newEldritch = GameManager.Instance.Pool.GetObject("Melee Eldritch V2 Variant");
        newEldritch.transform.position = new Vector3(x, meleeEldritch.transform.position.y + spawnZone.transform.position.y, z);
        newEldritch.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        newEldritch.GetComponent<EnemyHealth>().Init();

        EldritchsSpawned(newEldritch);
    }

    public void SpawnRangedEldritch(GameObject spawnZone)
    {
        Bounds bounds = spawnZone.GetComponent<Collider>().bounds;
        float x = Random.Range(bounds.min.x, bounds.max.x);
        float z = Random.Range(bounds.min.z, bounds.max.z);

        //var newEldritch = Instantiate(rangedEldritch, new Vector3(x+position.x, rangedEldritch.transform.position.y, z+position.z), rangedEldritch.transform.rotation);
        var newEldritch = GameManager.Instance.Pool.GetObject("Projectile Eldritch V2 Variant");
        newEldritch.transform.position = new Vector3(x, meleeEldritch.transform.position.y + spawnZone.transform.position.y, z);
        newEldritch.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        newEldritch.GetComponent<EnemyHealth>().Init();

        EldritchsSpawned(newEldritch);
    }

    public void SpawnPersoEldritch(Vector3 position, float tailleX = 40.0f, float tailleZ = 40.0f)
    {
        Vector3 spawnPosition;
        var iterations = 5000;
        do {
            float x = Random.Range(-tailleX / 2.0f, tailleX / 2.0f) + position.x ;
            float z = Random.Range(-tailleZ / 2.0f, tailleZ / 2.0f) + position.z ;
            spawnPosition = new Vector3(x,/* persoEldritch.transform.position.y + */Player.Instance.transform.position.y + 0.5f, z);
            iterations--;
        } while (!(CheckPositionIsEmpty(spawnPosition) && PlayerIsVisible(spawnPosition)) && (iterations >= 0));
        if (iterations < 0) return;

        //var newEldritch = Instantiate(rangedEldritch, new Vector3(x+position.x, rangedEldritch.transform.position.y, z+position.z), rangedEldritch.transform.rotation);
        var newEldritch = GameManager.Instance.Pool.GetObject("Eldritch perso");
        //newEldritch.transform.position = new Vector3(x + position.x, persoEldritch.transform.position.y, z + position.z);
        newEldritch.transform.position = spawnPosition;
        newEldritch.GetComponent<EnemyHealth>().Init();
        newEldritch.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        EldritchsPersoSpawned(newEldritch);
    }

    public void GeneratePersoEldritchs()
    {
        if (generationReady)
        {
            StartCoroutine(GenerationPersoEldritchs());
            generationReady = false;
            currentTimeBetweenGenerations = 0.0f;
        }
    }

    IEnumerator GenerationPersoEldritchs()
    {
        var currentEldritchsNumber = CountActivePersoEldritchs();
        if(currentEldritchsNumber < maxPersoEldritch)
        {
            uint numberToGenerate = (uint)Random.Range(1, maxPersoEldritchPerGeneration);
            var playerPosition = Player.Instance.transform.position;
            numberToGenerate = (uint)Mathf.Clamp(numberToGenerate, 0, (maxPersoEldritch - currentEldritchsNumber));
            //Debug.Log("GenerationPersoEldritchs to generate " + numberToGenerate);
            for (uint i = 0; i < numberToGenerate; i++)
            {
                SpawnPersoEldritch(playerPosition);
                yield return new WaitForSeconds(timeBetweenSpawns);
            }
            yield return null;
        }
    }

    private uint CountActivePersoEldritchs()
    {
        uint res = 0;
        foreach(GameObject eldritch in persoEldritchsList)
        {
            if (eldritch.activeSelf) res++;
        }
        //Debug.Log("CountActivePersoEldritchs " + res);
        return res;
    }


    public void SetEldritchAlpha(GameObject eldritch)
    {
        float maximum;
        float minimum;
        float ratio;
        if(previous_alpha < target_alpha)
        {

            minimum = previous_alpha;
            maximum = target_alpha;
            ratio = currentTransitionTime / transitionDuration;

        }
        else
        {
            minimum = target_alpha;
            maximum = previous_alpha;
            ratio = (transitionDuration - currentTransitionTime) / transitionDuration;

        }
        current_alpha = Mathf.Lerp(minimum, maximum, ratio);
        var renderers = eldritch.GetComponentsInChildren<Renderer>();
        foreach (Renderer renderer in renderers)
        {
            var materials = renderer.materials;
            foreach (Material material in materials)
            {
                if (material.HasProperty("_Color"))
                {
                    var color = material.color;
                    color.a = current_alpha;
                    material.color = color;
                }

            }


        }
        var healthbar = eldritch.GetComponentInChildren<CanvasGroup>();
        if (healthbar)
        {
            healthbar.alpha = current_alpha;
        }

        if (current_alpha > maximum || current_alpha < minimum)
        {
            currentTransitionTime = transitionDuration;
            previous_alpha = target_alpha;
        }
    }


    public void SetEldritchsAlpha()
    {
        foreach (GameObject eldritch in eldritchs)
        {
            SetEldritchAlpha(eldritch);
        }
    }

    public void SetEldritchsColliders(bool isEnabled)
    {
        foreach (GameObject eldritch in eldritchs)
        {
            SetEldritchColliders(isEnabled, eldritch);
        }
    }

    public void SetEldritchColliders(bool isEnabled , GameObject eldritch)
    {
        var parentCollider = eldritch.GetComponent<BoxCollider>();
        if (parentCollider) parentCollider.enabled = isEnabled;
        var colliders = eldritch.GetComponentsInChildren<BoxCollider>();
        foreach (BoxCollider collider in colliders)
        {
            collider.enabled = isEnabled;
        }
    }

    public void ChangeTargetAlpha(float alpha)
    {
        target_alpha = alpha;
        previous_alpha = current_alpha;
        currentTransitionTime = 0.0f;
    }

    public void StartBossFight()
    {
        bossOnScene = GameObject.FindGameObjectWithTag("EldritchBoss");
        if (bossOnScene)
        {
            bossOnScene.GetComponent<EnemyBehaviour>().enabled = true;
            bossBehaviour = bossOnScene.GetComponent<EnemyBehaviour>();
            bossBehaviour.gameObject.GetComponent<EnemyHealth>().deathFlag.onTrigger.AddListener(BossDefeated);
        }
    }

    private void BossDefeated()
    {
        //yield return StartCoroutine(ScenarioManager.Instance.WaitUntilEvent(bossBehaviour.gameObject.GetComponent<EnemyHealth>().deathFlag.onTrigger));

        KillEldritchs();
        // TODO : Get the player to borderline sanity
    }

    public void EldritchKilled(GameObject eldritch)
    {
        eldritch.GetComponent<BoxCollider>().enabled = false;
        eldritchsKilled++;
        Player.Instance.PlayerMentalHealth.RefillPotion();
        FightManager.Instance.EldritchKilled();
        eldritchs.Remove(eldritch);
        eldritch.GetComponent<EnemyHealth>().StopAllCoroutines();
        eldritch.GetComponent<EnemyBehaviour>().Release();
    }

    public void CloneSpawned(GameObject clone)
    {
        clones.Add(clone);
    }

    public void CloneDespawned(GameObject clone)
    {
        clones.Remove(clone);
        clone.GetComponent<EnemyBehaviour>().Release();
    }

    public void EldritchsSpawned(GameObject newEldritch)
    {
        eldritchs.Add(newEldritch);
        FightManager.Instance.EldritchSpawned();
        SetEldritchColliders(isAggressive, newEldritch);
        //SetEldritchAlpha(newEldritch);
        //eldritchsSpawned++;
    }

    public void EldritchsPersoSpawned(GameObject newEldritch)
    {
        eldritchs.Add(newEldritch);
        persoEldritchsList.Add(newEldritch);
        SetEldritchColliders(isAggressive, newEldritch);
        FightManager.Instance.EldritchSpawned();
        //SetEldritchAlpha(newEldritch);
        //eldritchsSpawned++;
    }

    private void KillEldritchs()
    {
        foreach (GameObject go in eldritchs.ToArray())
        {
            go.GetComponent<EnemyHealth>().Execute();
        }
    }


    public int GetNumberOfEldritchs()
    {
        return numberOfEldritchs;
    }

    public int GetNumberOfEldritchsKilled()
    {
        return eldritchsKilled;
    }

    public int GetNumberOfEldritchsSpawned()
    {
        return eldritchsSpawned;
    }


    public float EldritchCount()
    {
        return eldritchs.Count;
    }

    public void setAggro(bool isAggro)
    {
        isAggressive = isAggro;
        SetEldritchsColliders(isAggro);
    }


    public GameObject FindClosestEnemyInExecuteRange(Vector3 playerPosition, Transform executeZone, Transform guardZone)
    {
        GameObject eldritchInRange = null;
        float maxDistance = 1000.0f;
        Vector3 execZoneSize = Vector3.Scale(executeZone.localScale, executeZone.GetComponent<Collider>().bounds.size);
        Vector3 execGuardZoneSize = Vector3.Scale(guardZone.localScale, guardZone.GetComponent<Collider>().bounds.size);

        Debug.Log("Player : " + playerPosition);
        Debug.Log("Zone : " + executeZone.transform.position);
        Debug.Log("Size : " + execZoneSize);


        Vector3 pos;
        float distance;
        foreach(GameObject go in eldritchs)
        {
            if (go.GetComponent<EnemyHealth>().IsInExecuteRange())
            {
                pos = go.transform.position;
                if (executeZone.GetComponent<Collider>().bounds.Contains(pos))
                {
                    distance = Vector3.Distance(pos, playerPosition);
                    if (distance < maxDistance)
                    {
                        eldritchInRange = go;
                        maxDistance = distance;
                    }
                }
            }
        }
        foreach (GameObject go in guards)
        {
            if (go.GetComponent<EnemyHealth>().IsInExecuteRange())
            {
                pos = go.transform.position;
                if (guardZone.GetComponent<Collider>().bounds.Contains(pos))
                {
                    distance = Vector3.Distance(pos, playerPosition);
                    if (distance < maxDistance)
                    {
                        eldritchInRange = go;
                        maxDistance = distance;
                    }
                }
            }
        }

        return eldritchInRange;
        
    }

    public void GuardSpawned(GameObject guard)
    {
        guards.Add(guard);
    }

    private bool CheckPositionIsEmpty(Vector3 pos, float radius = 0.5f)
    {
        Collider[] hitColliders = Physics.OverlapSphere(pos, radius);

        for (int j = 0; j < hitColliders.Length; j++)
        {
            if (hitColliders[j].gameObject.CompareTag("Building"))
            {
                return false;
            }
        }
        return true;
    }

    private bool PlayerIsVisible(Vector3 pos)
    {
        int layerMask = 1 << 0;
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer

        return !Physics.Raycast(pos, Player.Instance.transform.position - pos, out hit,
            Vector3.Distance(pos, Player.Instance.transform.position) - 1.0f,layerMask);

    }

    public void FindGuards()
    {
        GameObject[] guardsInScene = GameObject.FindGameObjectsWithTag("Guard");
        foreach(GameObject guard in guardsInScene)
        {
            guards.Add(guard.transform.parent.gameObject);
        }
    }

    public void StartGuardFight()
    {
        foreach(GameObject guard in guards)
        {
            guard.GetComponent<GuardBehaviour>().BecomeAggro();
        }
    }

    public int CountGuards()
    {
        return guards.Count;
    }

    public void DestroyGuards()
    {
        foreach (GameObject guard in guards.ToArray())
        {
            guards.Remove(guard);
            Destroy(guard);
        }
    }

    private IEnumerator WaitForLevelLoading()
    {
        int actualScene = SceneManager.GetActiveScene().buildIndex;

        yield return StartCoroutine(GameMode.Instance.WaitForSceneSwitchFromActualScene(actualScene));
        Clear();
        StartCoroutine(WaitForLevelLoading());
    }
}
