﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save
{
    public MentalHealth mentalHealth = null;
    public float hitPoints = 0.0f;
    public SerializableVector3 playerPosition = new SerializableVector3();


    public ScenarioManager.Shortcuts scenarioShortcut = ScenarioManager.Shortcuts.ACT1_PART0;
    public int scene = 0;
    public SerializableDictionary lastPositionOfPlayer = new SerializableDictionary();

    public bool hasUnlockedHeavyAttack = false;
    public bool hasUpgradedPotion = false;
    public bool hasUnlockedRoll = false;
    public bool hasUnlockedLifesteal = false;
    public bool hasUnlockedDodge = false;
    public bool hasUpgradedSpells = false;
}

[System.Serializable]
public struct SerializableVector3
{
    public float x;
    public float y;
    public float z;

    public SerializableVector3(float rX, float rY, float rZ)
    {
        x = rX;
        y = rY;
        z = rZ;
    }
    
    public static implicit operator Vector3(SerializableVector3 rValue)
    {
        return new Vector3(rValue.x, rValue.y, rValue.z);
    }

    public static implicit operator SerializableVector3(Vector3 rValue)
    {
        return new SerializableVector3(rValue.x, rValue.y, rValue.z);
    }
}

[System.Serializable]
public class SerializableDictionary
{
    [SerializeField] public List<int> keys = new List<int>();
    [SerializeField] public List<SerializableVector3> values = new List<SerializableVector3>();

    public SerializableDictionary() { }

    public SerializableDictionary(Dictionary<int, Vector3> dictionary)
    {
        keys.Clear();
        values.Clear();

        foreach (var kvp in dictionary)
        {
            keys.Add(kvp.Key);
            values.Add((SerializableVector3)kvp.Value);
        }
    }

    public Dictionary<int, Vector3> Deserialize()
    {
        Dictionary<int, Vector3> dictionary = new Dictionary<int, Vector3>();

        for (int i = 0; i != Math.Min(keys.Count, values.Count); i++)
            dictionary.Add(keys[i], (Vector3)values[i]);

        return dictionary;
    }
}